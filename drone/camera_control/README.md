# V1.0 #
ROS node that reads a S.Bus data stream and generates a S.Bus data stream to control BMMCC functions

* `camera_control` service changes camera parameters
* `camera_status` topic has info regarding the S.Bus channels values being generated, camera connection status and radio connection status.

The funtions values presented on `camera_status` are the currently written to the S.Bus interface in BMMCC units.

There is no camera feedback acknowledging any change of functions or giving the current funtions status.

## Interface Description ##
*The Futaba S.Bus protocol is a serial protocol to control servos.
Up to 16 proportional and two digital channels are available.
The protocol is derived from the very known RS232 protocol used everywhere.
**The signal must be first inverted**.
The frame is 8E2*
[source](https://os.mbed.com/users/Digixx/notebook/futaba-s-bus-controlled-by-mbed/)

Although S.Bus is a 11bit per channel protocol, BMMCC only uses the 8 most significant bits.
[source](https://forum.blackmagicdesign.com/viewtopic.php?f=2&t=67024)

The **input value needs to be between 44 and 212** in order to be interpreted by the camera. A value of **128 is considered to be the midpoint or neutral** point. 

There are 2 ways to map the commands to the controls

1. A value within some certain ranges will trigger a particular setting. This is the case when controlling settings of:
	* Iris
	* Focus
	* Audio
	* Frame Rate
	* Codec

2. A change from the neutral value, to above or bellow, and then back to neutral point. This is the case when incrementing/decrementing the settings of:
	* Zoom
	* Start/Stop Record
	* Auto Focus
	* ISO
	* Shutter Angle
	* White Balance

**Zoom is a particular case of the second type of controller since the user will zoom in/out for as long as the user sends a value out of the neutral. The zoom speed appears to be constant.**

## Hardware setup used ##

* Radio Transmitter: FrSky Taranis X9D Plus
* Radio Receiver: FrSky X8R
* USB to TTL RS232 converter: FTDI TTL-232R-3V3-WE
* \#2 NPN Transistors: 2N2222A
* \#2 Resistors: 4.7K
* \#2 Resistors: 10K
* \#1 Futaba J Female connector (to connect to X8R)
* \#1 Futaba J male connector (to connect to BMMCC)

With a simple transistor+resistors inverter circuit we invert the S.Bus output and input.
[It’s safe to generate a 5V S.Bus signal to control BMMCC](https://forum.blackmagicdesign.com/viewtopic.php?f=4&t=43703).
FTDI TTL-232R-3V3-WE provides 5V. 


## Definitions ##

### Camera Control default S.Bus camera function assignment ###

BMMCC control  | S.Bus Channel
------------- | -------------
Start Record   | 13
IRIS | 7
Manual Focus | 8
Auto Focus | 12
Zoom | 3
ISO | 9
Shutter Angle | 10
White Balance | 11
Audio Levels | 15
Frame Rate | 5
Codec | 6

This default selection can be changed via parameters server. Example on launch file.
### Camera Control default functions ###

BMMCC control  | S.Bus Channel
------------- | -------------
IRIS   | largest (f3.5 for the tested DW len @ 14mm)
Frame Rate | 30 FPS
Codec | RAW

This default selection can be changed via parameters server. Example on launch file.

## Manual Control ##

### FrSky Taranis X9D sources/outputs ###
In order for this transmitter to send the exact neutral point of 128 required by the BMMCC, a trim of 4 is required when setting up the neutral point of the sources.

Mixer Channel (S.Bus channel)  | Taranis X9D input source
------------- | -------------
1 | Thr
2 | Ail
3 | Ele
4 | Rud
5 | S1
6 | S2
7 | LS
8 | RS
9 | SA
10 | SB
11 | SC
12 | SD
13 | SE
14 | SF
15 | SG
16 | SH
