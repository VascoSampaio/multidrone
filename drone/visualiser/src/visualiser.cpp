#include "ros/ros.h"
#include <cstdlib>
#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include "visualanalysis_msgs/Verify.h"
#include "visualanalysis_msgs/Detect.h"
#include "visualanalysis_msgs/Track.h"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <string>
#include <fstream>
#include <math.h>
#include <fstream>

using namespace cv;
using namespace std;

visualanalysis_msgs::Track trk_srv;

char image_filename[30];
int im_counter = 0;
int counter1 = 0;

char buff[50];

#define debug_mode
#define vis_mode

bool start = false;

float search_variable = 1.2;

bool target_found = true;


//ofstream text;



ros::ServiceClient trk_client;
ros::ServiceClient ver_client;
ros::ServiceClient det_client;

//cv frame
cv::Mat current_img;


float trk_score_th = 0.4;
float ver_score_th = 2;

int minSize=32;
int minSize_cntr=0;

// convert cv Rect to visualanalysis_msgs::TargetPositionROI2D
visualanalysis_msgs::TargetPositionROI2D ConvertToROI(cv::Rect cvrect)
{
	visualanalysis_msgs::TargetPositionROI2D msgROI;
	msgROI.x = cvrect.x;
	msgROI.y = cvrect.y;
	msgROI.w = cvrect.width;
	msgROI.h = cvrect.height;
	msgROI.id = -1;
	return msgROI;
}

// convert visualanalysis_msgs::TargetPositionROI2D to cv Rect
cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg)
{
	cv::Rect cvrect;
	cvrect.x = msg.x;
	cvrect.y = msg.y;
	cvrect.width = msg.w;
	cvrect.height = msg.h;
	return cvrect;
}

float Area(cv::Rect A)
{
	return float(A.width * A.height);
}

bool empty_image(Mat img)
{
	if (img.empty())
	{
		return true;
	}
	else
	{
		return false;
	}
}


void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{


	current_img = cv_bridge::toCvShare(msg, "bgr8")->image;

	//char buff[30];
	//char buff2[30];
	//char buff3[20];

	//snprintf(buff, 10, "%s", "det area");
	//cv::putText(img, buff, Point(20,40), FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0),2);
	//snprintf(buff, 10, "%s", "tracking roi");
	//cv::putText(img, buff, Point(20,60), FONT_HERSHEY_PLAIN, 2, cv::Scalar(0,255,0),2);

	//snprintf(buff2, 10, "%s", "detected rois");
	//cv::putText(img, buff2, Point(20,80), FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,255,255),2);


	//snprintf(buff3, 10, "%s", "corrected & reinit roi");
	//cv::putText(img, buff3, Point(20,100), FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,255,0),2);

	ROS_INFO("frame %d", msg->header.seq);

    // cv::Mat img2 =current_img;

//    if (!current_img.empty())
//    {
//        imshow("image", current_img);
//        cv::waitKey(40);
//    }

}

cv::Rect CheckLimits(cv::Rect roi)
{

	if (0 <= roi.x && 0 <= roi.width && roi.x + roi.width <= current_img.cols && 0 <= roi.y && 0 <= roi.height && roi.y + roi.height <= current_img.rows)
	{
		ROS_INFO("ok");
		return roi;
	}
	else
	{
		roi.x = max(0, roi.x);
		roi.y = max(0, roi.y);
		roi.height = min(roi.height, current_img.rows - roi.y - 1);
		roi.width = min(roi.width, current_img.cols - roi.x - 1);
		ROS_INFO(" not ok");
		return roi;
	}
}



void drawRect(cv::Rect rect, cv::Scalar color, int size)
{
	
	rect = CheckLimits(rect);
        cout << rect.x << " " << rect.y << " " << rect.width << " " << rect.height << endl;

	if (!current_img.empty())
	{
	cv::rectangle(current_img, rect, color, size);
	}
}

void drawScore(float score,cv::Rect rect, int x,int y, cv::Scalar color)
{
	if (!current_img.empty())
	{
	char s[30];
	snprintf(s, 10, "%f", score);
	cv::putText(current_img, s, Point(rect.x-x,rect.y-y), FONT_HERSHEY_PLAIN, 2, color,2);
	}
}


void drawID(int score,cv::Rect rect, int x,int y, cv::Scalar color)
{
	if (!current_img.empty())
	{
	char s2[30];
	snprintf(s2, 10, "%d", score);
	cv::putText(current_img, s2, Point(rect.x-x,rect.y-y), FONT_HERSHEY_PLAIN, 2, color,2);
	}
}


void detection(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& msg)
{

	for (int i = 0; i < msg->targets.size(); i++)
	{	
	
		cv::Rect cv_roi = ConvertToCVRect(msg->targets[i]);
	       
		drawRect(cv_roi, cv::Scalar(255,255,0),2);

		drawID(msg->targets[i].id, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, -msg->targets[i].h-5, cv::Scalar(100,255,255));
		drawScore(msg->targets[i].det_score, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, 20, cv::Scalar(255,255,255));

	}

//	snprintf(image_filename, 30, "%08d.jpg", ++counter1);
//	string imgName = "/media/data/danai/temporary/" + string(image_filename);
//	imwrite(imgName, current_img);

	
	//if (!img.empty())
	//{
	//	imshow("image", img);
	//	cv::waitKey(40);
	//}


}






void tracker(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& msg)
{
	

	for (int i = 0; i < msg->targets.size(); i++)
	{	
	
		cv::Rect cv_roi = ConvertToCVRect(msg->targets[i]);
	        cout << cv_roi.x << " " << cv_roi.y << " " << cv_roi.width << " " << cv_roi.height<< endl;
		drawRect(cv_roi, cv::Scalar(0,255,0),2);
		drawID(msg->targets[i].id, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, -msg->targets[i].h-30, cv::Scalar(100,255,255));
		drawScore(msg->targets[i].trk_score, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, 20, cv::Scalar(255,255,255));



	//text  << msg->targets[i].frame << endl;
	//text << msg->targets[i].x << " " << msg->targets[i].y << " " << msg->targets[i].w << " " << msg->targets[i].h  <<endl;

	}

//	snprintf(image_filename, 30, "%08d.jpg", ++counter1);
//	string imgName = "/media/data/danai/temporary/" + string(image_filename);
//	imwrite(imgName, current_img);

    if (!current_img.empty())
    {
        imshow("image", current_img);
        cv::waitKey(40);
    }
}


void verify(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& msg)
{

	for (int i = 0; i < msg->targets.size(); i++)
	{	
	
		cv::Rect cv_roi = ConvertToCVRect(msg->targets[i]);
	        cout << cv_roi.x << " " << cv_roi.y << " " << cv_roi.width << " " << cv_roi.height<< endl;
		drawRect(cv_roi, cv::Scalar(0,0,255),2);
		drawID(msg->targets[i].id, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, -msg->targets[i].h-30, cv::Scalar(100,255,255));
		drawScore(msg->targets[i].trk_score, cv::Rect(msg->targets[i].x, msg->targets[i].y, msg->targets[i].w, msg->targets[i].h), 0, 20, cv::Scalar(255,255,255));

	}



}

void det_area(const visualanalysis_msgs::TargetPositionROI2D& msg)
{
	
		cv::Rect cv_roi = ConvertToCVRect(msg);	    
		drawRect(cv_roi, cv::Scalar(255,0,0),2);
		
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "visualiser");
	ros::NodeHandle nh;

	static int drone_id_;
	nh.param<int>("/drone_id", drone_id_, 1);
	ROS_INFO("Reading from drone %d", drone_id_);

	//text.open("/home/danai/ros_for_review/src/visualiser/src/video.txt");
	

	image_transport::ImageTransport it(nh);
	string image_topic;
	nh.param<string>("/visualiser/image_topic", image_topic, "/drone_1/shooting_camera");
	image_transport::Subscriber sub1 = it.subscribe(image_topic, 1,imageCallback);
	ROS_INFO("Reading from image topic %s", image_topic.c_str());

	//ros::Subscriber subscriber_trRois = n.subscribe("tracker_rois", 1, verification);


	ros::Subscriber sub2 = nh.subscribe("visual_analysis/target_position_roi_2d_array/", 1, tracker);
     
	ros::Subscriber sub3 = nh.subscribe("visual_analysis/verification_target_position_roi_2d_array/", 1, verify);

    //    ros::Subscriber sub4 = nh.subscribe("drone_"+std::to_string(drone_id_)+"/visual_analysis/detection_target_position_roi_2d_array/", 1, detection);
    ros::Subscriber sub4 = nh.subscribe("detection_rois", 1, detection);

	ros::Subscriber sub5 = nh.subscribe("det_Area", 1, det_area);

	

	ros::spin();
	return 0;
}
