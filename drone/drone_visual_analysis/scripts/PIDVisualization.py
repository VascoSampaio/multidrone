#!/usr/bin/env python

import os
from os.path import join

import rospy
from cv_bridge import CvBridge
from std_msgs.msg import String
from sensor_msgs.msg import Image
from std_srvs.srv import Empty, EmptyResponse
from sensor_msgs.msg import CompressedImage
from multidrone_msgs.msg import *
from multidrone_msgs.srv import *
from visualanalysis_msgs.msg import *
from visualanalysis_msgs.srv import *

import cv2
import numpy as np
import time


class PIDControllerNode:
    """
    This node simulates a gimbal/camera controller that works on a 2-D image
    """

    def __init__(self):
        # Controller simulator internal state (bbox center and zoom)
        self.center_x = 0.5
        self.center_y = 0.5
        self.zoom = 1.0
        self.base_zoom = 0.1
        self.zoom_limits = [0.3, 1. / self.base_zoom]
        # self.base_zoom = 0.3
        # self.zoom_limits = [0.3, 10]
        self.VideoFrameWidth = int(rospy.get_param('analyzed_video_frame_width_pixels'))
        self.VideoFrameHeight = int(rospy.get_param('analyzed_video_frame_height_pixels'))
        self.imageTopic = str(
            rospy.get_param('image_input_topic'))  # '/usb_cam/image_raw/compressed' for USB camera input
        self.image = np.zeros((self.VideoFrameWidth, self.VideoFrameHeight, 3))
        self.cv_bridge = CvBridge()
        self.first_im = True

        # Target values
        self.target_x = float(rospy.get_param('target_roi_center_x'))
        self.target_y = float(rospy.get_param('target_roi_center_y'))
        self.target_zoom = float(rospy.get_param('target_frame_coverage'))

        # Define the services for controlling the targets
        self.adjust_x_target_service = rospy.Service('set_control_x', ControllerTarget, self.set_control_x)
        self.adjust_y_target_service = rospy.Service('set_control_y', ControllerTarget, self.set_control_y)
        self.adjust_zoom_target_service = rospy.Service('set_control_zoom', ControllerTarget, self.set_control_zoom)

        # PID Controller state
        self.last_x_error = 0.
        self.last_y_error = 0.
        self.last_zoom_error = 0.

        self.x_integral = 0.
        self.y_integral = 0.
        self.zoom_integral = 0.

        self.x_derivative = 0.
        self.y_derivative = 0.
        self.zoom_derivative = 0.

        # PID Controller parameters
        # self.x_parameters = [0.1, 0.05, 0.0]
        # self.y_parameters = [0.1, 0.05, 0.0]
        # self.zoom_parameters = [0.9, 0.5, 0.0]
        self.x_parameters = [0.1, 0.05, 0.0]
        self.y_parameters = [0.1, 0.05, 0.0]
        self.zoom_parameters = [0.9, 0.5, 0.0]

    def reset_pid(self):
        self.last_x_error = 0.
        self.last_y_error = 0.
        self.last_zoom_error = 0.

        self.x_integral = 0.
        self.y_integral = 0.
        self.zoom_integral = 0.

    def listener(self):
        rospy.init_node('VSA_visualization_crop', anonymous=True)
        print('VSA_visualization_crop node started!')

        # Get the image to simulate the control process
        rospy.Subscriber(self.imageTopic, Image, self.camera_callback, queue_size=30)

        # Subscribe to detector topic to get the bboxes
        rospy.Subscriber('/drone_1/visual_analysis/target_position_roi_2d_array', TargetPositionROI2DArray,
                         self.control_callback)
        rospy.spin()

    def set_control_x(self, req):
        self.target_x = float(req.value)
        self.reset_pid()
        # return ControllerTargetResponse("OK")

    def set_control_y(self, req):
        self.target_y = float(req.value)
        self.reset_pid()
        # return ControllerTargetResponse("OK")

    def set_control_zoom(self, req):
        self.target_zoom = float(req.value)
        self.reset_pid()
        # return ControllerTargetResponse("OK")

    def camera_callback(self, data):
        """
        Reads the camera pixels to fake the control process
        :param data:
        :return:
        """
        rospy.loginfo("received frame")
        # Compressed image
        # np_arr = np.fromstring(data.data, np.uint8)
        # cv_image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        cv_image = self.cv_bridge.imgmsg_to_cv2(data, "bgr8")
        if self.first_im:
            self.VideoFrameHeight, self.VideoFrameWidth = cv_image.shape[0], cv_image.shape[1]
            self.first_im = False
        self.image = np.asarray(cv_image).copy()

    def control_callback(self, data_msg):
        ROIs = data_msg.targets
        BB = ROIs[0]
        data = [BB.x, BB.x + BB.w, BB.y, BB.y + BB.h]

        # Get the bbox raw data
        # data = str(data)[7:-1]
        # data = [float(x) for x in data.split(',')]
        # bbox_x_min, bbox_x_max, bbox_y_min, bbox_y_max = np.asarray(data)

        # bbox_x_min, bbox_x_max = int(self.image.shape[1] * data[0]), int(self.image.shape[1] * data[1])
        # bbox_y_min, bbox_y_max = int(self.image.shape[0] * data[2]), int(self.image.shape[0] * data[3])
        bbox_x_min, bbox_x_max = data[0], data[1]
        bbox_y_min, bbox_y_max = data[2], data[3]
        aspRatTarg = float(bbox_x_max - bbox_x_min + 1) / (bbox_y_max - bbox_y_min + 1)

        # Compute the camera view box
        height, width, _ = self.image.shape
        cam_x_min = max(int(width * (self.center_x - self.base_zoom / (self.zoom))), 0)
        cam_x_max = min(int(width * (self.center_x + self.base_zoom / (self.zoom))), width)
        cam_y_min = max(int(height * (self.center_y - self.base_zoom / (self.zoom))), 0)
        cam_y_max = min(int(height * (self.center_y + self.base_zoom / (self.zoom))), height)

        aspRatCam = float(cam_x_max - cam_x_min + 1) / (cam_y_max - cam_y_min + 1)
        aspRat16_9 = float(width / float(height))

        if aspRatCam < aspRat16_9:
            # increase numerator or decrease denominator
            X = aspRat16_9 * (cam_y_max - cam_y_min + 1)
            xx = round(X - (cam_x_max - cam_x_min + 1))

            Y = (cam_x_max - cam_x_min + 1) / aspRat16_9
            yy = round(Y - (cam_y_max - cam_y_min + 1))

            # increase numerator
            if abs(xx) < abs(yy):
                # the margin can be split equally to both sides
                if (round(cam_x_min - xx / 2) >= 0) & (round(cam_x_max + xx / 2) <= width):
                    cam_x_min = round(cam_x_min - xx / 2)
                    cam_x_max = round(cam_x_max + xx / 2)
                # the margin is far too big and there is not enough space
                elif (round(cam_x_min - xx / 2) < 0) & (round(cam_x_max + xx / 2) > width):
                    # decrease denominator
                    cam_y_min = round(cam_y_min - yy / 2)
                    cam_y_max = round(cam_y_max + yy / 2)
                # there is enough space only from the left side of the bb
                # increase numerator as much as possible (equally from both sides)
                # and subsequently decrease denominator accordingly
                elif round(cam_x_min - xx / 2) >= 0:
                    xx = width - cam_x_max
                    cam_x_max = width
                    cam_x_min = cam_x_min - xx

                    Y = (cam_x_max - cam_x_min + 1) / aspRat16_9
                    yy = round(Y - (cam_y_max - cam_y_min + 1))

                    cam_y_min = round(cam_y_min - yy / 2)
                    cam_y_max = round(cam_y_max + yy / 2)
                # there is enough space only from the right side of the bb
                # increase numerator as much as possible (equally from both sides)
                # and subsequently decrease denominator accordingly
                elif round(cam_x_max + xx / 2) <= width:
                    xx = cam_x_min
                    cam_x_min = 0
                    cam_x_max = cam_x_max + xx

                    Y = (cam_x_max - cam_x_min + 1) / aspRat16_9
                    yy = round(Y - (cam_y_max - cam_y_min + 1))

                    cam_y_min = round(cam_y_min - yy / 2)
                    cam_y_max = round(cam_y_max + yy / 2)
            # decrease denominator
            else:
                cam_y_min = round(cam_y_min - yy / 2)
                cam_y_max = round(cam_y_max + yy / 2)
        else:
            # increase denominator or decrease numerator
            X = aspRat16_9 * (cam_y_max - cam_y_min + 1)
            xx = round(X - (cam_x_max - cam_x_min + 1))

            Y = (cam_x_max - cam_x_min + 1) / aspRat16_9
            yy = round(Y - (cam_y_max - cam_y_min + 1))

            # increase denominator
            if abs(yy) < abs(xx):
                # the margin can be split equally to both sides
                if (round(cam_y_min - yy / 2) >= 0) & (round(cam_y_max + yy / 2) <= height):
                    cam_y_min = round(cam_y_min - yy / 2)
                    cam_y_max = round(cam_y_max + yy / 2)
                # the margin is far too big and there is not enough space
                elif (round(cam_y_min - yy / 2) < 0) & (round(cam_y_max + yy / 2) > height):
                    # decrease numerator
                    cam_x_min = round(cam_x_min - xx / 2)
                    cam_x_max = round(cam_x_max + xx / 2)
                # there is enough space only from the top side of the bb
                # increase denominator as much as possible (equally from both sides)
                # and subsequently decrease numerator accordingly
                elif round(cam_y_min - yy / 2) >= 0:
                    yy = height - cam_y_max
                    cam_y_max = height
                    cam_y_min = cam_y_min - yy

                    X = aspRat16_9 * (cam_y_max - cam_y_min + 1)
                    xx = round(X - (cam_x_max - cam_x_min + 1))

                    cam_x_min = round(cam_x_min - xx / 2)
                    cam_x_max = round(cam_x_max + xx / 2)
                # there is enough space only from the bottom side of the bb
                # increase denominator as much as possible (equally from both sides)
                # and subsequently decrease numerator accordingly
                elif round(cam_y_max + yy / 2) <= height:
                    yy = cam_y_min
                    cam_y_min = 0
                    cam_y_max = cam_y_max + yy

                    X = aspRat16_9 * (cam_y_max - cam_y_min + 1)
                    xx = round(X - (cam_x_max - cam_x_min + 1))

                    cam_x_min = round(cam_x_min - xx / 2)
                    cam_x_max = round(cam_x_max + xx / 2)
                # decrease numerator
            else:
                cam_x_min = round(cam_x_min - xx / 2)
                cam_x_max = round(cam_x_max + xx / 2)

        cam_x_min = int(cam_x_min)
        cam_x_max = int(cam_x_max)
        cam_y_min = int(cam_y_min)
        cam_y_max = int(cam_y_max)
        aspRatCamNew = float(cam_x_max - cam_x_min + 1) / (cam_y_max - cam_y_min + 1)

        # Compute the bbox overlapping
        # This is a bit artificial - but this is just a demonstration
        effective_xmin = max(cam_x_min, bbox_x_min)
        effective_xmax = min(cam_x_max, bbox_x_max)
        effective_ymin = max(cam_y_min, bbox_y_min)
        effective_ymax = min(cam_y_max, bbox_y_max)

        # Check the bounds
        # if (effective_xmin >= cam_x_max) or (effective_xmax <= cam_x_min) or (effective_ymin >= cam_y_max) or (effective_ymax <= cam_y_min):
        # 	effective_xmin, effective_ymin, effective_xmax, effective_ymax = -1, -1, -1, -1

        # Compute the center and the dimensions inside the camera view
        if effective_xmin > 0:
            cam_view_width, cam_view_height = cam_x_max - cam_x_min, cam_y_max - cam_y_min
            effective_width = (effective_xmax - effective_xmin) / float(cam_view_width)
            effective_height = (effective_ymax - effective_ymin) / float(cam_view_height)
            effective_area = ((effective_xmax - effective_xmin) * (effective_ymax - effective_ymin)) / float(
                cam_view_width * cam_view_height)
            cam_bbox_x_center = ((effective_xmin + effective_xmax - 2 * cam_x_min) / 2.0) / cam_view_width
            cam_bbox_y_center = ((effective_ymin + effective_ymax - 2 * cam_y_min) / 2.0) / cam_view_height
        else:
            # No bounding box detected inside the view
            cam_bbox_x_center, cam_bbox_y_center = -1, -1
        # print "BBox center in camera view: " + str(cam_bbox_x_center) + ", " + str(cam_bbox_y_center)

        ##############################################################################
        #                          -  Main control loop  -                           #
        #  Available info to the control loop: bbox center and its width and height  #
        ##############################################################################
        if cam_bbox_x_center == -1:
            x_error, y_error = 0., 0.
            zoom_error = 0.
            rospy.loginfo("cam_bbox_x_center == -1")
        else:
            x_error, y_error = cam_bbox_x_center - self.target_x, cam_bbox_y_center - self.target_y

            margin_x_1 = float(cam_bbox_x_center - effective_width) / 2.0
            margin_x_2 = 1 - (float(cam_bbox_x_center + effective_width) / 2.0)
            margin_y_1 = float(cam_bbox_y_center - effective_height) / 2.0
            margin_y_2 = 1 - (float(cam_bbox_y_center + effective_height) / 2.0)
            min_margin = min(margin_x_1, margin_x_2, margin_y_1, margin_y_2)
            # max_margin = max(margin_x_1, margin_x_2, margin_y_1, margin_y_2)

            zoom_error = self.target_zoom - effective_area
            # zoom_error = self.target_zoom - min_margin

            # print "Errors = " + str(x_error) + ", " + str(y_error) + ", " + str(zoom_error)

            # CODE FOR PID CAMERA CONTROL
            # TODO: In a production-grade system delta must be adjusted using the timestaps
            delta = 0.1
            self.x_integral += delta * x_error
            self.y_integral += delta * y_error
            self.zoom_integral += delta * zoom_error

            # Safe guard
            if np.abs(self.x_integral) > 0.5:
                self.x_integral = 0
            if np.abs(self.y_integral) > 0.5:
                self.x_integral = 0
            if np.abs(self.zoom_integral) > 0.5:
                self.zoom_integral = 0

            self.x_derivative = (x_error - self.last_x_error) / delta
            self.y_derivative = (y_error - self.last_y_error) / delta
            self.zoom_derivative = (zoom_error - self.last_zoom_error) / delta

            x = self.x_parameters[0] * x_error + self.x_parameters[1] * self.x_integral + \
                self.x_parameters[2] * self.x_derivative
            y = self.y_parameters[0] * y_error + self.y_parameters[1] * self.y_integral + \
                self.y_parameters[2] * self.y_derivative
            z = self.zoom_parameters[0] * zoom_error + self.zoom_parameters[1] * self.zoom_integral + \
                self.zoom_parameters[2] * self.zoom_derivative

            # Limit the maximum movements
            x = max(min(x, 0.2), -0.2)
            y = max(min(y, 0.2), -0.2)
            z = max(min(z, 0.2), -0.2)

            # If no object in view, zoom out
            if cam_bbox_x_center == -1:
                z = -0.02

            # Simulate the control
            self.center_x = max(min(self.center_x + x, 1), 0)
            self.center_y = max(min(self.center_y + y, 1), 0)
            self.zoom = max(min(self.zoom + z, self.zoom_limits[1]), self.zoom_limits[0])
            # print "Control = " + str(self.center_x) +", " + str(self.center_y) + ", " + str(self.zoom)

            # Save the state
            self.last_x_error = x_error
            self.last_y_error = y_error
            self.last_zoom_error = zoom_error

            # Show the output
            to_draw = self.image

            if bbox_x_min > 0:
                cv2.rectangle(to_draw, (bbox_x_min, bbox_y_min), (bbox_x_max, bbox_y_max), (255, 0, 0), 2)
            cv2.rectangle(to_draw, (cam_x_min, cam_y_min), (cam_x_max, cam_y_max), (0, 255, 0), 2)

            if effective_xmin > 0:
                cv2.rectangle(to_draw, (effective_xmin, effective_ymin), (effective_xmax, effective_ymax),
                              (0, 255, 255), 2)

            # cv2.imshow("Control View", to_draw)

            camera_view_img = self.image[cam_y_min:cam_y_max, cam_x_min:cam_x_max, :]
            camera_view_img = cv2.resize(camera_view_img, ((int)(width / 1.5), (int)(height / 1.5)))
            cv2.imshow('Cropped Camera View', camera_view_img)
            cv2.waitKey(1)


if __name__ == '__main__':
    node = PIDControllerNode()
    node.listener()
