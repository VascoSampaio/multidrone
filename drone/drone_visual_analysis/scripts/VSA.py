#!/usr/bin/env python

import rospy
import rospkg
from std_msgs.msg import Header
from multidrone_msgs.srv import *
from visualanalysis_msgs.msg import *
from geometry_msgs.msg import Vector3


class VSA:
    def __init__(self):
        rospack = rospkg.RosPack()
        self.UAV_ID = rospy.get_param('~drone_id', 1)
        self.mode_test = int(rospy.get_param('~mode_test', False))
        self.path = rospack.get_path('drone_visual_analysis')
        self.VideoFrameWidth = 640
        self.VideoFrameHeight = 480
        self.trk_threshold = rospy.get_param("trk_score_th", -1.0)
        self.pub_VCE = rospy.Publisher('/drone_{}/visual_analysis/visual_control_error'.format(self.UAV_ID),
                                       VisualControlError, queue_size=10)
        self.pub_VCE_target = rospy.Publisher('/drone_{}/visual_analysis/visual_control_target'.format(self.UAV_ID),
                                              Vector3, queue_size=10)
        self.pub_VCE_goal = rospy.Publisher('/drone_{}/visual_analysis/visual_control_goal'.format(self.UAV_ID),
                                            Vector3, queue_size=10)
        self.targetFrameCoverage = 0.2
        self.targetROICenterX = self.VideoFrameWidth / 2
        self.targetROICenterY = self.VideoFrameHeight / 2

        self.timer = rospy.Timer(rospy.Duration(1 / 25.), self.timed_callback)
        self.BB = None

    def handle_set_framing_type(self, req):
        self.targetROICenterX = req.target_roi_center_x
        self.targetROICenterY = req.target_roi_center_y
        rospy.loginfo("Target x, y: %.2f %.2f", self.targetROICenterX, self.targetROICenterY)
        framingTypeMsg = req.target_framing_type
        framingTypeVal = framingTypeMsg.type
        # ELS framing shot type
        if framingTypeVal == 0:
            self.targetFrameCoverage = 0.03
        # VLS framing shot type
        elif framingTypeVal == 1:
            self.targetFrameCoverage = 0.13
        # LS framing shot type
        elif framingTypeVal == 2:
            self.targetFrameCoverage = 0.30
        # MS framing shot type
        elif framingTypeVal == 3:
            self.targetFrameCoverage = 0.50
        # MCU framing shot type
        elif framingTypeVal == 4:
            self.targetFrameCoverage = 0.67
        # CU framing shot type
        elif framingTypeVal == 5:
            self.targetFrameCoverage = 0.80
        else:
            self.targetFrameCoverage = 0.2
        return SetFramingTypeResponse()

    def listener(self):
        rospy.loginfo('VisualShotAnalysis node started!')
        setFramingType = rospy.Service('/drone_{}/set_framing_type'.format(self.UAV_ID),
                                       SetFramingType, self.handle_set_framing_type)

        # Subscribe to 2D ROI from visual detection/tracking
        roi_topic = '/drone_{}/visual_analysis/target_position_roi_2d_array'.format(self.UAV_ID)
        rospy.Subscriber(roi_topic,
                         TargetPositionROI2DArray, self.compute_callback)
        rospy.loginfo('Reading ROI from {}'.format(roi_topic))
        rospy.spin()

    def timed_callback(self, timer):
        if self.BB is not None:
            # check time
            header = Header()
            header.stamp = timer.current_real
            target = Vector3(self.BB.x + self.BB.w / 2, self.BB.y + self.BB.h / 2, 0)
            self.pub_VCE_target.publish(target)

            goal = Vector3(self.targetROICenterX, self.targetROICenterY, 0)
            self.pub_VCE_goal.publish(goal)

            if timer.current_real - self.BB_time > rospy.Duration(0.5):
                rospy.logwarn("ROI is older than 0.5s, publishing zeros")
                visualCE = VisualControlError(header, 0., 0., 0.)
            elif out_of_limits(self.BB, self.VideoFrameHeight, self.VideoFrameWidth):
                rospy.logwarn("ROI is out of frame, publishing zeros")
                visualCE = VisualControlError(header, 0., 0., 0.)
            else:
                xError = (self.BB.x + self.BB.w / 2) - self.targetROICenterX
                yError = (self.BB.y + self.BB.h / 2) - self.targetROICenterY
                widthFrameCoverage = float(self.BB.w) / self.VideoFrameWidth
                heightFrameCoverage = float(self.BB.h) / self.VideoFrameHeight
                BBFrameCoverage = max(widthFrameCoverage, heightFrameCoverage)
                zoomError = BBFrameCoverage - self.targetFrameCoverage
                visualCE = VisualControlError(header, float(xError), float(yError), float(zoomError))
            self.pub_VCE.publish(visualCE)

    def compute_callback(self, dataMsg):
        self.BB = dataMsg.targets[0]
        self.BB_time = rospy.Time.now()
        rospy.loginfo("Got ROI {},{},{}x{}".format(self.BB.x, self.BB.y, self.BB.w, self.BB.h))


def out_of_limits(roi, h, w):
    if roi.x + roi.w < 0 or roi.x > w:
        return True
    if roi.y + roi.h < 0 or roi.h > h:
        return True
    return False


if __name__ == '__main__':
    rospy.init_node('VisualShotAnalysis', anonymous=True)
    node = VSA()
    node.listener()
