#!/usr/bin/env python
# modified from https://github.com/tensorflow/models

import roslib
import rospy
import rospkg
from sensor_msgs.msg import Image
from visualanalysis_msgs.msg import TargetPositionROI2D, TargetPositionROI2DArray
from visualanalysis_msgs.srv import *

from cv_bridge import CvBridge, CvBridgeError

import numpy as np
import os
import sys
import tensorflow as tf


class ObjectDetector:
    def __init__(self, base_path, model_name, labelmap_name):
        self.bridge = CvBridge()
        self.img = None
        self.frame_count = 0

        # DETECTOR INITIALIZATION
        self.MODEL_NAME = model_name
        self.trt = False

        self.PATH_TO_CKPT = os.path.join(base_path, 'models', 'detectors', 'ssd', self.MODEL_NAME,
                                         'frozen_inference_graph.pb')
        print self.PATH_TO_CKPT
        if not os.path.exists(self.PATH_TO_CKPT):
            print('ERROR! Checkpoint path does not exist, exiting')
            exit(0)
        # List of the strings that is used to add correct label for each box
        self.LABELMAP_NAME = labelmap_name  # 'faces_label_map.pbtxt'
        self.PATH_TO_LABELS = os.path.join(base_path, 'data', self.LABELMAP_NAME)
        self.NUM_CLASSES = 1

        # Load a (frozen) Tensorflow model into memory
        config = tf.ConfigProto()

        config.gpu_options.allow_growth = True
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                if self.trt:
                    import tensorflow.contrib.tensorrt as trt
                    trt_graph = trt.create_inference_graph(
                        input_graph_def=od_graph_def,
                        outputs=['detection_boxes', 'detection_scores', 'detection_classes', 'num_detections'],
                        max_batch_size=1,
                        max_workspace_size_bytes=1 << 25,
                        precision_mode="FP16")
                    tf.import_graph_def(
                        trt_graph,
                        return_elements=['detection_boxes', 'detection_scores', 'detection_classes', 'num_detections'],
                        name='')
                else:
                    tf.import_graph_def(od_graph_def, name='')
                self.sess = tf.Session(graph=self.detection_graph, config=config)

        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

        self.init_w_img = rospy.get_param("/init_tracker_mode", 0)

    def setup_comm(self, drone_id):
        self.image_topic = rospy.get_param('/drone_{}//drone_visual_analysis/camera_topic'.format(drone_id),
                                           '/drone_{}/shooting_camera'.format(drone_id))
        print('Reading images from {}'.format(self.image_topic))
        self.image_sub = rospy.Subscriber(self.image_topic, Image, self.image_callback,
                                          queue_size=1,
                                          buff_size=2 ** 24)
        self.detection_srv = rospy.Service("detection", Detect, self.detect_single)
        self.detection_pub = rospy.Publisher("detection_rois", TargetPositionROI2DArray, queue_size=1)
        self.verification_pub = rospy.Publisher("verification_rois", TargetPositionROI2DArray, queue_size=1)

    def dummy_detections(self):
        image_np_expanded = np.zeros((1, 300, 300, 3))
        for _ in range(3):
            (boxes, scores, classes, num) = self.sess.run(
                [self.detection_boxes, self.detection_scores,
                 self.detection_classes, self.num_detections],
                feed_dict={self.image_tensor: image_np_expanded})

    def detect_single(self, req):
        rospy.loginfo("DETECTION REQUEST")
        if self.img is None:
            return False
        detarea = req.Area
        RoiArray = TargetPositionROI2DArray()
        res = DetectResponse()

        if self.init_w_img == 1:
            self.img = self.bridge.imgmsg_to_cv2(req.img, "bgr8")
            self.init_w_img = 0
        if self.img is not None and self.img.size > 0:
            with self.detection_graph.as_default():
                img_np = self.img[detarea.y:detarea.y + detarea.h,
                         detarea.x:detarea.x + detarea.w]
                if img_np.size < 10:
                    return res
                image_np_expanded = np.expand_dims(img_np, axis=0)
                (boxes, scores, classes, num) = self.sess.run(
                    [self.detection_boxes, self.detection_scores,
                     self.detection_classes, self.num_detections],
                    feed_dict={self.image_tensor: image_np_expanded})

                boxes_ = np.squeeze(boxes)
                scores_ = np.squeeze(scores)
                classes_ = np.squeeze(classes)
                for id, box in enumerate(boxes_):
                    if scores_[id] > 0.1:
                        temp = TargetPositionROI2D()
                        temp.x = box[1] * img_np.shape[1] + detarea.x
                        temp.y = box[0] * img_np.shape[0] + detarea.y
                        temp.w = (box[3] - box[1]) * img_np.shape[1]
                        temp.h = (box[2] - box[0]) * img_np.shape[0]
                        temp.id = id
                        temp.det_score = scores_[id]
                        RoiArray.targets.append(temp)
                        res.ArrayROI.append(temp)
                if req.TargetType == 1:
                    self.verification_pub.publish(RoiArray)
                else:
                    self.detection_pub.publish(RoiArray)
        rospy.loginfo("Returning %d detections", len(res.ArrayROI))
        return res

    def verify_single(self, req):
        print "VERIFICATION REQUEST"
        ver_msg = req.ROI
        ver_roi = [ver_msg.x, ver_msg.y, ver_msg.w, ver_msg.h]
        RoiArray = TargetPositionROI2DArray()
        res = VerifyResponse()

        ver_roi = self.check_limits_context(ver_roi)
        if self.img is not None:
            with self.detection_graph.as_default():
                img_np = self.img[ver_roi[1]:ver_roi[1] + ver_roi[3], ver_roi[0]:ver_roi[0] + ver_roi[2]]
                print 'cropped img : ', img_np.shape
                image_np_expanded = np.expand_dims(img_np, axis=0)
                (boxes, scores, classes, num) = self.sess.run(
                    [self.detection_boxes, self.detection_scores,
                     self.detection_classes, self.num_detections],
                    feed_dict={self.image_tensor: image_np_expanded})
                try:
                    boxes_ = np.squeeze(boxes)
                    scores_ = np.squeeze(scores)
                    for id, box in enumerate(boxes_):
                        if scores_[id] > 0.5:
                            print 'box ', id, ':', box, scores_[id]
                            temp = TargetPositionROI2D()
                            temp.x, temp.y = box[0], box[1]
                            temp.w, temp.h = box[2], box[3]
                            temp.id = id
                            temp.det_score = scores_[id]
                            RoiArray.targets.append(temp)
                    if scores_[np.argmax(scores_)] > 0.5:
                        if num == 1:
                            res.ver_score = scores_[0]
                        if num > 1:
                            res.ver_score = scores_[0]
                        if num == 0:
                            res.ver_score = .0
                    self.verification_pub.publish(RoiArray)
                except CvBridgeError as e:
                    print(e)
        return res

    def check_limits(self, roi):
        if self.img is None:
            return roi
        roi.x = max(0, roi.x)
        roi.y = max(0, roi.y)
        roi.h = min(roi.h, self.img.shape[0] - roi.y - 1)
        roi.h = min(roi.w, self.img.shape[1] - roi.x - 1)
        return roi

    def check_limits_context(self, box):
        pad = 1.5 * np.sqrt(box[2] * box[3])
        box[0] = max(0, box[0] - pad)
        box[1] = max(0, box[1] - pad)
        box[3] = min(box[3] + 2 * pad, self.img.shape[0] - box[3] + 2 * pad - 1)
        box[2] = min(box[2] + 2 * pad, self.img.shape[1] - box[2] + 1 * pad - 1)
        return [int(x) for x in box]

    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "rgb8")
            self.img = cv_image
            self.frame_count += 1
        except CvBridgeError as e:
            print 'frame conversion failed'
            print e

        return


def main(args):
    rospy.init_node('ObjectDetector', anonymous=True, log_level=rospy.DEBUG)
    rospack = rospkg.RosPack()
    drone_id = rospy.get_param("~drone_id", 1)

    base_path = rospack.get_path("drone_visual_analysis")
    model_name = rospy.get_param("/drone_{}/model_name".format(drone_id), "ssd_mobilenet_300x300_faces")
    labelmap_name = rospy.get_param("/drone_{}/labelmap_name".format(drone_id), "faces_labelmap.pbtxt")

    det = ObjectDetector(base_path, model_name, labelmap_name)
    det.setup_comm(drone_id=drone_id)

    det.dummy_detections()
    rospy.loginfo('Reading images from {}'.format(det.image_topic))

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")


if __name__ == '__main__':
    main(sys.argv)
