#!/usr/bin/env python

import rospy
import rospkg
from geometry_msgs.msg import Vector3
import random


class GPSSim(object):
    def __init__(self):
        rospack = rospkg.RosPack()
        self.drone_id = rospy.get_param('/drone_id', 1)
        self.path = rospack.get_path('drone_visual_analysis')
        self.image_width = int(rospy.get_param('/drone_{}/analyzed_video_frame_width_pixels'.format(self.drone_id)))
        self.image_height = int(rospy.get_param('/drone_{}/analyzed_video_frame_height_pixels'.format(self.drone_id)))

        self.pub = rospy.Publisher('/drone_{}/pixel_position'.format(self.drone_id),
                                   Vector3, queue_size=10)
        self.timer = rospy.Timer(rospy.Duration(0.04), self.timed_callback)
        self.msg = Vector3()
        self.msg.x = 0.5 * self.image_width
        self.msg.y = 0.5 * self.image_height
        self.msg.z = 1.

    def timed_callback(self, timer):
        self.msg.x += random.randint(-3, 3)
        self.msg.y += random.randint(-3, 3)
        # self.msg.x = min(self.msg.x, self.image_width)
        # self.msg.y = min(self.msg.y, self.image_height)
        rospy.loginfo("Publishing message {},{},{}".format(self.msg.x, self.msg.y, self.msg.z))
        self.pub.publish(self.msg)


if __name__ == '__main__':
    rospy.init_node('GPSSimulator', anonymous=True)
    node = GPSSim()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
