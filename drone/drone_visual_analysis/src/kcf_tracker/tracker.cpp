#include <ros/ros.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include "kcftracker.hpp"

#include <visualanalysis_msgs/Track.h>
#include <vector>
#include <chrono>

#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <drone_visual_analysis/Reidentify.h>

#include <multidrone_msgs/GimbalStatus.h>
#include <multidrone_msgs/CameraStatus.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include <interface_test/interface_test.h>


using namespace std;


multidrone_msgs::CameraStatus camera_status;
bool it_17_21 = false;
multidrone_msgs::GimbalStatus gimbal_status;
bool it_16_21 = false;
geometry_msgs::PoseStamped drone_pose;
bool it_18_21 = false;
geometry_msgs::TwistStamped drone_velocity;

vector<KCFTracker *> kcfTrackers;
bool hog_;
int init_w_img;
bool fixed_window_;
bool multiscale_;
bool lab_;
ofstream logfile;

ros::Publisher publisher;
bool init_tracking = false;
cv::Mat current_img;
float total_time = 0;
double rv = 0.0;

visualanalysis_msgs::TargetPositionROI2D ConvertToROI(cv::Rect cvrect) {
    visualanalysis_msgs::TargetPositionROI2D msgROI;
    msgROI.x = cvrect.x;
    msgROI.y = cvrect.y;
    msgROI.w = cvrect.width;
    msgROI.h = cvrect.height;
    msgROI.id = -1;
    return msgROI;
}

// convert visualanalysis_msgs::TargetPositionROI2D to cv Rect
cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg) {
    cv::Rect cvrect;
    cvrect.x = msg.x;
    cvrect.y = msg.y;
    cvrect.width = msg.w;
    cvrect.height = msg.h;
    return cvrect;
}

cv::Rect CheckLimits(cv::Rect roi, cv::Mat img) {
    cv::Rect _roi = roi & cv::Rect(0, 0, img.cols, img.rows);
    return _roi;
}

void disposeTrackers() {
    for (int i = 0; i < kcfTrackers.size(); i++) {
        delete kcfTrackers[i];
    }
    kcfTrackers.clear();
}

// IT 17-21
void cameraStatusCallback(const multidrone_msgs::CameraStatus &msg) {
    camera_status = msg;
    if (it_17_21) {
        run_test(string("it17_21"), msg.header.stamp, it_17_21);
    }
}

// IT 16-21
void gimbalStatusCallback(const multidrone_msgs::GimbalStatus &msg) {
    gimbal_status = msg;
    if (it_16_21) {
        run_test(string("it16_21"), msg.header.stamp, it_17_21);
    }
}

// IT 18-21
void dronePoseCallback(const geometry_msgs::PoseStamped &msg) {
    drone_pose = msg;
    if (it_18_21) {
        run_test(string("it18_21a"), msg.header.stamp, it_18_21);
    }
}

// IT 18-21
void droneVelocityCallback(const geometry_msgs::TwistStamped &msg) {
    drone_velocity = msg;
    if (it_18_21) {
        run_test(string("it18_21b"), msg.header.stamp, it_18_21);
    }
}


void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
    try {
        // convert to OpenCV
        current_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
        return;
    }

    for (int i = 0; i < kcfTrackers.size(); i++) {
        if (!kcfTrackers[i]->initialized) {
            ROS_INFO("initializing tracking...");
            kcfTrackers[i]->init(kcfTrackers[i]->next_rect, current_img);
            kcfTrackers[i]->initialized = true;
        }
    }
    if (!init_tracking) {
        init_tracking = true;
        return;
    }

    if (!kcfTrackers.empty()) {
        if (!current_img.empty()) {
            visualanalysis_msgs::TargetPositionROI2DArray RoiArray;
            // update all initialized trackers with new img, save to RoiArray for publishing
            for (int i = 0; i < kcfTrackers.size(); i++) {
                if (kcfTrackers[i]->initialized) {
                    cv::Rect roi_result = kcfTrackers[i]->update(current_img);
                    // copy most recent gimbal status, camera status, drone pose (and drone velocity?)
                    visualanalysis_msgs::TargetPositionROI2D msgROI = ConvertToROI(roi_result);
                    msgROI.gimbal_status = gimbal_status;
                    msgROI.camera_status = camera_status;
                    msgROI.drone_pose = drone_pose;
                    msgROI.id = kcfTrackers[i]->id;
                    msgROI.trk_score = kcfTrackers[i]->peak;
                    RoiArray.targets.push_back(msgROI);
                    ROS_INFO("ID  %d  [%d] [%d] [%d] [%d]", msgROI.id, msgROI.x, msgROI.y, msgROI.w, msgROI.h);
                }
            }
            RoiArray.seq = msg->header.seq;
            RoiArray.camera = camera_status;
            RoiArray.gimbal = gimbal_status;
            RoiArray.drone_pose = drone_pose;
            publisher.publish(RoiArray);
        }
        rv++;
    }
}

void DeleteTracker(int trackerId) {
    vector<KCFTracker *>::iterator it;
    for (it = kcfTrackers.begin(); it != kcfTrackers.end(); it++) {
        if ((*it)->id == trackerId)
            break;
    }
    kcfTrackers.erase(it);
}

int GetTracker(int trackerId) {
    int i;
    for (i = 0; i < kcfTrackers.size(); i++) {
        if (kcfTrackers[i]->id == trackerId)
            break;
    }
    return i;
}


void ReinitializeTracker(visualanalysis_msgs::TargetPositionROI2D roi) {
    ROS_INFO("Re-initialization ROI %d: [%d %d %d %d]", roi.id, roi.x, roi.y, roi.w, roi.h);
    cv::Rect r(roi.x, roi.y, roi.w, roi.h);
    int i = GetTracker(roi.id);
    kcfTrackers[i]->next_rect = r;
    kcfTrackers[i]->initialized = false;
}

bool ReidentifyTarget(drone_visual_analysis::Reidentify::Request &req, drone_visual_analysis::Reidentify::Response &resp) {
    cv::Mat tracked_img = current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
    cv::Rect current_rect = CheckLimits(ConvertToCVRect(req.target), tracked_img);
    ROS_INFO("--Re-identification for ROI %d: [%d %d %d %d]", req.target.id, req.target.x, req.target.y, req.target.w,
             req.target.h);

    float peak = kcfTrackers[GetTracker(req.target.id)]->get_response(tracked_img, current_rect);
    ROS_INFO("---- score: %.2f", peak);
    resp.trk_score = peak;

    return true;
}

bool controlTracker(visualanalysis_msgs::Track::Request &req, visualanalysis_msgs::Track::Response &resp) {
    if (req.cmd == 0) {
        // delete any old trackers and initialize new ones with req.targets
        disposeTrackers();
        ROS_INFO("Initializing tracker(s)...");

        for (int i = 0; i < req.targets.size(); i++) {
            cv::Rect rect1(req.targets[i].x, req.targets[i].y, req.targets[i].w, req.targets[i].h);
            KCFTracker *kcf = new KCFTracker(hog_, fixed_window_, multiscale_, lab_);
            kcf->next_rect = rect1;
            kcf->id = req.targets[i].id;
            ROS_INFO("--Created tracker with id: %d", req.targets[i].id);
            kcfTrackers.push_back(kcf);

            if (init_w_img == 1) {
                ROS_INFO("initializing tracking...");
                if (init_w_img == 1) {
                    current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
                }
                kcf->init(kcf->next_rect, current_img);
                kcf->initialized = true;
            }
        }

        init_tracking = true;
        resp.result = 0;
    }
    if (req.cmd == 1) {
        ROS_INFO("Re-initializing tracker with id %d", req.targets[0].id);
        ReinitializeTracker(req.targets[0]);
    }
    if (req.cmd == -1) {
        disposeTrackers();
        resp.result = -1;
    }
    return true;
}


int main(int argc, char **argv) {
    ROS_INFO("Initiating tracker node.");
    static int drone_id_;

    ros::init(argc, argv, "tracker");
    ros::NodeHandle nh("~");

    string path_to_node = ros::package::getPath("drone_visual_analysis") + string("/");
    nh.param<int>("drone_id", drone_id_, 1);

    image_transport::ImageTransport it(nh);
    string image_topic;
    if (!nh.hasParam("/drone_visual_analysis/camera_topic")) {
        image_topic = string("drone_") + to_string(drone_id_) + string("/shooting_camera");
    } else {
        nh.getParam("/drone_visual_analysis/camera_topic", image_topic);
    }

    image_transport::Subscriber sub1 = it.subscribe(image_topic, 1, imageCallback);

    // Camera, Gimbal & Drone status subscribers
    string camera_status_topic = string("drone_") + to_string(drone_id_) + string("/camera/status");
    ROS_INFO("Camera status topic: %s", camera_status_topic.c_str());
    ros::Subscriber cs_sub = nh.subscribe(camera_status_topic, 1, cameraStatusCallback);
    nh.param<bool>("it_17_21", it_17_21, false);
    if (it_17_21)
        test_setup("it17_21");

    string gimbal_status_topic = string("drone_") + to_string(drone_id_) + string("/gimbal/status");
    ROS_INFO("Gimbal status topic: %s", gimbal_status_topic.c_str());
    ros::Subscriber gs_sub = nh.subscribe(gimbal_status_topic, 1, gimbalStatusCallback);
    nh.param<bool>("it_16_21", it_16_21, false);
    if (it_16_21)
        test_setup("it16_21");

    string drone_pose_topic = string("drone_") + to_string(drone_id_) + string("/ual/pose");
    ROS_INFO("Drone pose topic: %s", drone_pose_topic.c_str());
    ros::Subscriber dp_sub = nh.subscribe(drone_pose_topic, 1, dronePoseCallback);

    string drone_velocity_topic = string("drone_") + to_string(drone_id_) + string("/ual/velocity");
    ROS_INFO("Drone velocity topic: %s", drone_velocity_topic.c_str());
    ros::Subscriber dv_sub = nh.subscribe(drone_velocity_topic, 1, droneVelocityCallback);
    nh.param<bool>("it_18_21", it_18_21, false);
    if (it_18_21)
        test_setup("it18_21a");
    test_setup("it18_21b");

    // publish tracker rois
    publisher = nh.advertise<visualanalysis_msgs::TargetPositionROI2DArray>(
            "drone_" + std::to_string(drone_id_) + "/visual_analysis/target_position_roi_2d_array/", 1);

    nh.param<bool>("hog", hog_, true);
    nh.param<bool>("fixed_window", fixed_window_, true);
    nh.param<bool>("multiscale", multiscale_, true);
    nh.param<bool>("lab", lab_, false);
    nh.getParam("/init_tracker_mode", init_w_img);

    // control tracker service
    ros::ServiceServer service = nh.advertiseService("drone_" + std::to_string(drone_id_) + "/tracking",
                                                     controlTracker);
    ros::ServiceServer reidentification_srv = nh.advertiseService(
            "drone_" + std::to_string(drone_id_) + "/reidentification", ReidentifyTarget);

    logfile.open(path_to_node + std::string("logfile_kcf.txt"));

    ros::spin();

    disposeTrackers();
    service.shutdown();
}





