// ros node that performs verification every $freq frames through a cnn and published the verification result
// if msg.id =0 then verification failed and the ROI is changed
// if msg.id =1 then verification passed and the ROI is not changed
// Danai Triantafyllidou
#include <ros/ros.h>
#include <ros/package.h>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <caffe/caffe.hpp>
#include "caffe/common.hpp"
#include <memory>
#include <chrono>
#include <visualanalysis_msgs/Verify.h>
#include <visualanalysis_msgs/Detect.h>
#include <vector>
#include <string>
#include "PyramidDetector.h"
#include <fstream>

using namespace caffe;

#define debug_mode
#define vis_mode

ros::Publisher ver_rois_pub;

string log_filename;
ofstream f;


int search_variable = 2;
int search_cnt = 0;
std::chrono::duration<double, std::ratio<1, 1000>> d1, d2;
std::chrono::time_point<std::chrono::high_resolution_clock> p1, p2;
// Frequency of detection variables
long freq = 4;

char s[30];
char s1[30];

int rv = 0;
float total_time = 0;
int im_counter = 0;
// img is the original frame
cv::Mat current_img;

// caffe model
string model_bike_;
string weights_bike_;

string model_football_;
string weights_football_;

string model_boat_;
string weights_boat_;

int init_w_img = 0;
bool load_model = false;

int minSize_ = 32;

int stride_;
int scanSize_;
int octaveSteps_;
double threshold_py_;
double groupEps_;
bool border_ = "true";
//int minSize = 32;
int groupCount_;


// Pyramid detection objects
std::shared_ptr<Net<float> > net;
std::shared_ptr<PyramidDetector<float>> pydetect;


void print_info(int stride, int scanSize, int octaveSteps, double threshold_py, double groupEps, double groupCount,
                int task) {

    cout << "Initializing detector/verifier node with parameters: " << endl;

    cout << "stride: " << stride_ << endl;
    cout << "scanSize: " << scanSize_ << endl;
    cout << "octaveSteps: " << octaveSteps_ << endl;
    cout << "threshold_py: " << threshold_py_ << endl;
    cout << "groupEps: " << groupEps_ << endl;
    cout << "groupCount: " << groupCount_ << endl;

}


// convert cv::rect to mva_msgs
visualanalysis_msgs::TargetPositionROI2D ConvertToROI(cv::Rect cvrect) {
    visualanalysis_msgs::TargetPositionROI2D msgROI;
    msgROI.x = cvrect.x;
    msgROI.y = cvrect.y;
    msgROI.w = cvrect.width;
    msgROI.h = cvrect.height;
    //msgROI.id = 0;
    return msgROI;
}

// convert pyrect to mva_msgs
visualanalysis_msgs::TargetPositionROI2D PyRectToROI(PyRect pyrect) {

    visualanalysis_msgs::TargetPositionROI2D msgROI;
    msgROI.x = pyrect.x1;
    msgROI.y = pyrect.y1;
    msgROI.w = pyrect.x2 - pyrect.x1;
    msgROI.h = pyrect.y2 - pyrect.y1;
    msgROI.det_score = pyrect.weight;
    msgROI.id = 0;
    return msgROI;
}


cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg) {
    return cv::Rect(msg.x, msg.y, msg.w, msg.h);

}

//covert pyrect to cvrect
cv::Rect ToCvRect(PyRect roi) {
    return cv::Rect(roi.x1, roi.y1, roi.x2 - roi.x1, roi.y2 - roi.y1);
}


float IouRatio(cv::Rect A, cv::Rect B) {
    float ratio = (A & B).area() / float((A | B).area());
    return ratio;
}

cv::Rect CheckLimits(cv::Rect roi) {
    cv::Rect _roi = roi & cv::Rect(0, 0, current_img.cols, current_img.rows);
    return _roi;

}


vector<cv::Rect> generate_rects(cv::Rect ver_roi) {

    visualanalysis_msgs::TargetPositionROI2DArray RoiArray;

    vector<cv::Rect> ver_rois;

    ver_rois.push_back(cv::Rect(ver_roi.x + 2, ver_roi.y + 2, ver_roi.width, ver_roi.height));
    ver_rois.push_back(cv::Rect(ver_roi.x - 2, ver_roi.y - 2, ver_roi.width, ver_roi.height));
    ver_rois.push_back(cv::Rect(ver_roi.x + 3, ver_roi.y + 3, ver_roi.width, ver_roi.height));
    ver_rois.push_back(cv::Rect(ver_roi.x - 3, ver_roi.y - 3, ver_roi.width, ver_roi.height));

    return ver_rois;
    for (int i = 0; i < ver_rois.size(); i++) {
        visualanalysis_msgs::TargetPositionROI2D temp = ConvertToROI(ver_rois[i]);
        RoiArray.targets.push_back(temp);
    }
    // detections_pub.publish(RoiArray);

}

bool VerificationRequest(visualanalysis_msgs::Verify::Request &req, visualanalysis_msgs::Verify::Response &res) {
    visualanalysis_msgs::TargetPositionROI2D ver_msg;
    ver_msg = req.ROI;
    cv::Rect ver_roi = ConvertToCVRect(ver_msg);
    cv::Mat img_ver32;


    ver_roi = CheckLimits(ver_roi);

    ROS_INFO("Verifying ROI with id %d %d %d %d %d", req.ROI.id, req.ROI.x, req.ROI.y, req.ROI.w, req.ROI.h);

    if (!current_img.empty()) {
        cv::Mat img_ver = current_img(ver_roi);
        resize(img_ver, img_ver32, Size(32, 32));

        if (img_ver32.cols == 32 && img_ver32.rows == 32) {
            // Verifying
            Blob<float> *b = net->input_blobs()[0];
            vector<int> shape(4);
            shape[0] = 1;
            shape[1] = 3;
            shape[2] = 32;
            shape[3] = 32;
            b->Reshape(shape);
            net->Reshape();
            pydetect->MatToBlob(img_ver32, b);
            vector<Blob<float> *> result = net->ForwardPrefilled();
            float r = result[0]->data_at(0, 1, 0, 0);

            ROS_INFO("Verifying ROI with id %d, score: %f", ver_msg.id, r);
            res.ver_score = r;
            return true;

        }


    }

}


void load_caffe_model(int type) {
    std::string path = ros::package::getPath("drone_visual_analysis") + string("/");
    switch (type) {
        case 1 :
            ROS_INFO("Loading bikes model from %s", (path + string(model_bike_)).c_str());
            ROS_INFO("Loading bikes weights from %s", (path + string(weights_bike_)).c_str());
            net.reset(new Net<float>(path + model_bike_, TEST));
            net->CopyTrainedLayersFrom(path + weights_bike_);
            load_model = true;
            break;
        case 2 :
            ROS_INFO("Loading football model from %s", (path + model_football_).c_str());
            ROS_INFO("Loading football weights from %s", (path + weights_football_).c_str());
            net.reset(new Net<float>(path + model_football_, TEST));
            net->CopyTrainedLayersFrom(path + weights_football_);
            load_model = true;
            break;
        case 3 :
            ROS_INFO("Loading boats model from %s", (path + model_boat_).c_str());
            ROS_INFO("Loading boats weights from %s", (path + weights_boat_).c_str());
            net.reset(new Net<float>(path + model_boat_, TEST));
            net->CopyTrainedLayersFrom(path + weights_boat_);
            load_model = true;
            break;
    }

}

bool DetectionRequest(visualanalysis_msgs::Detect::Request &req, visualanalysis_msgs::Detect::Response &res) {

    if (!load_model) {

        load_caffe_model(req.TargetType);
        cout << " CNN initialization " << endl;

    }
    visualanalysis_msgs::TargetPositionROI2D detarea = req.Area;

    vector<PyRect> py_detections;

    // message that rois are going to be published (for visualisation purposes)
    visualanalysis_msgs::TargetPositionROI2DArray RoiArray;

    minSize_ = 32;

    // Init PyramidDetector
    pydetect.reset(new PyramidDetector<float>(octaveSteps_, scanSize_, stride_, minSize_, threshold_py_, groupEps_,
                                              groupCount_, border_));

    cv::Rect cv_detarea = CheckLimits(ConvertToCVRect(detarea));


    ROS_INFO("Detection around ROI: %d %d %d %d", cv_detarea.x, cv_detarea.y, cv_detarea.width, cv_detarea.height);

    if (init_w_img == 1) {
        current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
        //init_w_img = 0;
    }

    if (cv_detarea.width > 32 && cv_detarea.height > 32) {
        if (!current_img.empty() && cv_detarea.width != 0) {
            cv::Mat img_detarea = current_img(cv_detarea);
            ROS_INFO("ok img area: %d %d ", img_detarea.cols, img_detarea.rows);

            p1 = std::chrono::high_resolution_clock::now();
            py_detections = pydetect->MultiScaleScan(img_detarea, *net.get());
            d1 = std::chrono::high_resolution_clock::now() - p1;

            ROS_INFO("*************  PY_DETECTIONS =  %d", (int)py_detections.size());

            for (int i = 0; i < py_detections.size(); i++) {

                visualanalysis_msgs::TargetPositionROI2D temp = PyRectToROI(py_detections[i]);
                temp.x = temp.x + cv_detarea.x;
                temp.y = temp.y + cv_detarea.y;
                res.ArrayROI.push_back(temp);
                RoiArray.targets.push_back(temp);
            }

            total_time = total_time + d1.count() / 1000;

            ROS_INFO("detection lasted %f seconds", d1.count() / 1000);
            ROS_INFO("total time in %f seconds", total_time);
            ROS_INFO("Frames per second:  %6.2f", (rv / total_time));

            //ver_rois_pub.publish(RoiArray);

            rv++;
            return true;
        }


    }

    return false;
}


void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
    current_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    ROS_INFO("frame %d", msg->header.seq);
}


int main(int argc, char **argv) {

    ros::init(argc, argv, "detector_verifier");

    // Caffe Initialization

#ifdef CPU_ONLY
    Caffe::set_mode(Caffe::CPU);
#else
    Caffe::set_mode(Caffe::GPU);
#endif

    ros::NodeHandle nh("~");

    //static int drone_id_;
    //ph.param<int>("drone_id", drone_id_, 1);
    string path_to_node = ros::package::getPath("drone_visual_analysis") + string("/");

    static int drone_id_;

    nh.param<int>("drone_id", drone_id_, 1);

    nh.param("log_filename", log_filename, string("detector_verifier.log"));

//    nh.param("weights_bike", weights_bike_, string("MODELS/bike/weights_bike.caffemodel"));
//    nh.param("model_bike", model_bike_, string("MODELS/bike/model_bike.prototxt"));
    if (!load_model && nh.getParam("weights_bike", weights_bike_)) {
        nh.getParam("model_bike", model_bike_);
        load_caffe_model(1);
        cout << " CNN initialization " << endl;
    }
    else if (!load_model && nh.getParam("weights_football", weights_football_)) {
        nh.getParam("model_football", model_football_);
        load_caffe_model(2);
        cout << " CNN initialization " << endl;
    }
    else if (!load_model && nh.getParam("weights_boat", weights_football_)) {
        nh.param("model_boat", model_boat_);
        load_caffe_model(3);
        cout << " CNN initialization " << endl;
    }

    nh.param("stride", stride_, 2);
    nh.param("scanSize", scanSize_, 32);
    nh.param("octaveSteps", octaveSteps_, 4);
    nh.param("threshold_py", threshold_py_, 3.);
    nh.param("groupEps", groupEps_, 0.17);
    nh.param("border", border_, true);
    nh.param("groupCount", groupCount_, 1);

    nh.getParam("/init_tracker_mode", init_w_img);

    // Subscribe to image frame

    image_transport::ImageTransport it(nh);
    string image_topic;
    if (!nh.hasParam("/drone_visual_analysis/camera_topic"))
    {
        image_topic = string("drone_") + to_string(drone_id_) + string("/shooting_camera");
    }
    else {
        nh.getParam("/drone_visual_analysis/camera_topic", image_topic);
    }
    image_transport::Subscriber subscriber_image = it.subscribe(image_topic, 1, imageCallback);

    ros::ServiceServer service = nh.advertiseService("detection", DetectionRequest);


    ros::spin();

}
