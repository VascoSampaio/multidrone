#include <ros/ros.h>
#include <ros/package.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <std_msgs/Header.h>
#include <visualanalysis_msgs/Track.h>
#include <vector>
#include <chrono>

#include <visualanalysis_msgs/TargetPositionROI2D.h>
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <drone_visual_analysis/Reidentify.h>

#include <multidrone_msgs/GimbalStatus.h>
#include <multidrone_msgs/CameraStatus.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include "staple_tracker.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

multidrone_msgs::CameraStatus camera_status;
multidrone_msgs::GimbalStatus gimbal_status;
geometry_msgs::PoseStamped drone_pose;
geometry_msgs::TwistStamped drone_velocity;

vector<STAPLE_TRACKER *> STAPLE_Trackers;

ros::Publisher publisher;
bool init_tracking = false;
int init_w_img;
cv::Mat current_img;
int last_seq = 0;

visualanalysis_msgs::TargetPositionROI2D ConvertToROI(cv::Rect cvrect) {
    visualanalysis_msgs::TargetPositionROI2D msgROI;
    msgROI.x = cvrect.x;
    msgROI.y = cvrect.y;
    msgROI.w = cvrect.width;
    msgROI.h = cvrect.height;
    msgROI.id = -1;
    return msgROI;
}

// convert visualanalysis_msgs::TargetPositionROI2D to cv Rect
cv::Rect ConvertToCVRect(visualanalysis_msgs::TargetPositionROI2D msg) {
    cv::Rect cvrect;
    cvrect.x = msg.x;
    cvrect.y = msg.y;
    cvrect.width = msg.w;
    cvrect.height = msg.h;
    return cvrect;
}

cv::Rect CheckLimits(cv::Rect roi, cv::Mat img) {
    cv::Rect _roi = roi & cv::Rect(0, 0, img.cols, img.rows);
    return _roi;
}

void disposeTrackers() {
    for (int i = 0; i < STAPLE_Trackers.size(); i++) {
        delete STAPLE_Trackers[i];
    }
    STAPLE_Trackers.clear();
}

// IT 17-21
void cameraStatusCallback(const multidrone_msgs::CameraStatus &msg) {
    camera_status = msg;
}

// IT 16-21
void gimbalStatusCallback(const multidrone_msgs::GimbalStatus &msg) {
    gimbal_status = msg;
}

// IT 18-21
void dronePoseCallback(const geometry_msgs::PoseStamped &msg) {
    drone_pose = msg;
}

// IT 18-21
void droneVelocityCallback(const geometry_msgs::TwistStamped &msg) {
    drone_velocity = msg;
}

void imageCallback(const sensor_msgs::ImageConstPtr &msg) {
    try {
        // convert to OpenCV
        current_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
        return;
    }
    ROS_INFO("Frame: %d", msg->header.seq);

    for (int i = 0; i < STAPLE_Trackers.size(); i++) {
        if (!STAPLE_Trackers[i]->initialized) {
            ROS_INFO("initializing tracking...");
            STAPLE_Trackers[i]->tracker_staple_initialize(current_img, STAPLE_Trackers[i]->next_rect);
            STAPLE_Trackers[i]->initialized = true;
            STAPLE_Trackers[i]->tracker_staple_train(current_img, true);
        }
    }
    if (!init_tracking) {
        init_tracking = true;
        return;
    }

    if (!STAPLE_Trackers.empty()) {
        if (!current_img.empty()) {
            //roi_results.clear();
            visualanalysis_msgs::TargetPositionROI2DArray RoiArray;
            // update all initialized trackers with new img, save to RoiArray for publishing
            for (int i = 0; i < STAPLE_Trackers.size(); i++) {
                if (STAPLE_Trackers[i]->initialized) {
                    cv::Rect roi_result = STAPLE_Trackers[i]->tracker_staple_update(current_img);
                    STAPLE_Trackers[i]->tracker_staple_train(current_img, false);

                    // copy most recent gimbal status, camera status, drone pose (and drone velocity?)
                    visualanalysis_msgs::TargetPositionROI2D msgROI = ConvertToROI(
                            CheckLimits(roi_result, current_img));
                    msgROI.gimbal_status = gimbal_status;
                    msgROI.camera_status = camera_status;
                    msgROI.drone_pose = drone_pose;
                    msgROI.id = STAPLE_Trackers[i]->id;
                    msgROI.trk_score = STAPLE_Trackers[i]->peak;
                    RoiArray.targets.push_back(msgROI);
                    ROS_INFO("[IMG: %d]  [%d] [%d] [%d] [%d]", msg->header.seq, msgROI.x, msgROI.y, msgROI.w, msgROI.h);
                }
            }
            RoiArray.camera = camera_status;
            RoiArray.gimbal = gimbal_status;
            RoiArray.drone_pose = drone_pose;
            if (msg->header.seq != last_seq)
                publisher.publish(RoiArray);
        }
    }
    last_seq = msg->header.seq;
}

void DeleteTracker(int trackerId) {
    vector<STAPLE_TRACKER *>::iterator it;
    for (it = STAPLE_Trackers.begin(); it != STAPLE_Trackers.end(); it++) {
        if ((*it)->id == trackerId)
            break;
    }
    STAPLE_Trackers.erase(it);
}

int GetTracker(int trackerId) {
    int i;
    for (i = 0; i < STAPLE_Trackers.size(); i++) {
        if (STAPLE_Trackers[i]->id == trackerId)
            break;
    }
    return i;
}


void ReinitializeTracker(visualanalysis_msgs::TargetPositionROI2D roi) {
    ROS_INFO("Re-initialization ROI %d: [%d %d %d %d]", roi.id, roi.x, roi.y, roi.w, roi.h);
    cv::Rect r(roi.x, roi.y, roi.w, roi.h);
    int i = GetTracker(roi.id);
    STAPLE_Trackers[i]->next_rect = r;
    STAPLE_Trackers[i]->initialized = false;
}

bool ReidentifyTarget(drone_visual_analysis::Reidentify::Request &req, drone_visual_analysis::Reidentify::Response &resp) {
    //cv::Mat tracked_img = current_img.clone();
    cv::Mat tracked_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
    cv::Rect current_rect = CheckLimits(ConvertToCVRect(req.target), tracked_img);
    ROS_INFO("--Re-identification for ROI %d: [%d %d %d %d]", req.target.id, req.target.x, req.target.y, req.target.w,
             req.target.h);

    float peak = STAPLE_Trackers[GetTracker(req.target.id)]->get_response(tracked_img, current_rect);
    ROS_INFO("---- score: %.2f", peak);
    resp.trk_score = peak;

    return true;
}

bool controlTracker(visualanalysis_msgs::Track::Request &req, visualanalysis_msgs::Track::Response &resp) {
    if (req.cmd == 0) {
        // delete any old trackers and initialize new ones with req.targets
        disposeTrackers();
        ROS_INFO("Initializing tracker(s)...");

        for (int i = 0; i < req.targets.size(); i++) {
            cv::Rect rect1(req.targets[i].x, req.targets[i].y, req.targets[i].w, req.targets[i].h);
            STAPLE_TRACKER *staple = new STAPLE_TRACKER();
            staple->next_rect = rect1;
            staple->id = req.targets[i].id;
            ROS_INFO("--Created tracker with id: %d", req.targets[i].id);
            STAPLE_Trackers.push_back(staple);

            if (init_w_img == 1) {
                ROS_INFO("initializing tracking...");
                if (init_w_img == 1) {
                    current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
                }
                staple->tracker_staple_initialize(current_img, staple->next_rect);
                staple->tracker_staple_train(current_img, true);
                staple->initialized = true;
            } else {
                ROS_INFO("init_w_img = 0");
            }
        }

        init_tracking = true;
        resp.result = 0;
    }
    if (req.cmd == 1) {
        ROS_INFO("Re-initializing tracker with id %d", req.targets[0].id);
        ReinitializeTracker(req.targets[0]);
    }
    if (req.cmd == 2) {     // MANUAL TRACKER UPDATE
        if (init_w_img == 1) {
            current_img = cv_bridge::toCvCopy(req.img, "bgr8")->image;
        }
        visualanalysis_msgs::TargetPositionROI2DArray RoiArray;
        // update all initialized trackers with new img, save to RoiArray for publishing
        for (int i = 0; i < STAPLE_Trackers.size(); i++) {
            if (STAPLE_Trackers[i]->initialized) {
                cv::Rect roi_result = STAPLE_Trackers[i]->tracker_staple_update(current_img);
                STAPLE_Trackers[i]->tracker_staple_train(current_img, false);

                // copy most recent gimbal status, camera status, drone pose (and drone velocity?)
                visualanalysis_msgs::TargetPositionROI2D msgROI = ConvertToROI(CheckLimits(roi_result, current_img));
                msgROI.gimbal_status = gimbal_status;
                msgROI.camera_status = camera_status;
                msgROI.drone_pose = drone_pose;
                msgROI.id = STAPLE_Trackers[i]->id;
                msgROI.trk_score = STAPLE_Trackers[i]->peak;
                ROS_INFO("[%d] %d,%d,%d,%d", req.img.header.seq, msgROI.x, msgROI.y, msgROI.w, msgROI.h);
            }
        }
    }
    if (req.cmd == -1) {
        disposeTrackers();
        resp.result = -1;
    }
    return true;
}

int main(int argc, char **argv) {
    ROS_INFO("Initiating tracker node.");
    static int drone_id_;

    ros::init(argc, argv, "tracker");
    ros::NodeHandle nh;

    string path_to_node = ros::package::getPath("drone_visual_analysis") + string("/");
    ros::NodeHandle nh_pr("~");
    nh_pr.param<int>("drone_id", drone_id_, 1);
    ROS_INFO("TRACKER DRONE ID: %d", drone_id_);


    image_transport::ImageTransport it(nh);
    string image_topic;
    if (!nh.hasParam(string("/drone_") + to_string(drone_id_) + "/drone_visual_analysis/camera_topic")) {
        image_topic = string("/drone_") + to_string(drone_id_) + string("/shooting_camera");
    } else {
        nh.getParam(string("/drone_") + to_string(drone_id_) + "/drone_visual_analysis/camera_topic", image_topic);
    }

    image_transport::Subscriber sub1 = it.subscribe(image_topic, 1, imageCallback);
    ROS_INFO("Reading images from: %s", image_topic.c_str());

    // Camera, Gimbal & Drone status subscribers
    string camera_status_topic = "camera/status";
    ROS_INFO("Camera status topic: %s", camera_status_topic.c_str());
    ros::Subscriber cs_sub = nh.subscribe(camera_status_topic, 1, cameraStatusCallback);

    string gimbal_status_topic = "gimbal/status";
    ROS_INFO("Gimbal status topic: %s", gimbal_status_topic.c_str());
    ros::Subscriber gs_sub = nh.subscribe(gimbal_status_topic, 1, gimbalStatusCallback);

    string drone_pose_topic = "ual/pose";
    ROS_INFO("Drone pose topic: %s", drone_pose_topic.c_str());
    ros::Subscriber dp_sub = nh.subscribe(drone_pose_topic, 1, dronePoseCallback);

    string drone_velocity_topic = "ual/velocity";
    ROS_INFO("Drone velocity topic: %s", drone_velocity_topic.c_str());
    ros::Subscriber dv_sub = nh.subscribe(drone_velocity_topic, 1, droneVelocityCallback);
    // publish tracker rois
    publisher = nh.advertise<visualanalysis_msgs::TargetPositionROI2DArray>(
            "visual_analysis/target_position_roi_2d_array/", 1);

    nh.param("init_tracker_mode", init_w_img, 1);

    // control tracker service
    ros::ServiceServer service = nh.advertiseService("tracking", controlTracker);
    ros::ServiceServer reidentification_srv = nh.advertiseService("reidentification", ReidentifyTarget);

    ros::spin();

    disposeTrackers();
    service.shutdown();
}
