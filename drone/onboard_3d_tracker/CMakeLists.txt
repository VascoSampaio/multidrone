cmake_minimum_required(VERSION 2.8.3)
project(onboard_3d_tracker)

## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  multidrone_msgs
  roscpp
  rospy
  tf
  visualanalysis_msgs
  std_msgs
  message_generation
  nav_msgs
  pcl_ros
  pcl_conversions
)

find_package(octomap REQUIRED)

# Generate messages in the 'msg' folder
 add_message_files(
   FILES
   EIFasoc_P2P.msg
   Contribution.msg
   MeasurementInfo.msg
   Point2D.msg
   Targets3DArray_P2P.msg
 )

## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
   nav_msgs
 )

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES DistanceAsociationEIF_library
  CATKIN_DEPENDS multidrone_msgs roscpp rospy std_msgs message_runtime tf visualanalysis_msgs
)

include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_library(DistanceAsociationEIF_library src/DistanceAsociationEIF.cpp src/Track.cpp)
add_dependencies(DistanceAsociationEIF_library ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

add_executable(onboard_3d_tracker src/onboard_3d_tracker_node.cpp)
target_link_libraries(onboard_3d_tracker DistanceAsociationEIF_library ${catkin_LIBRARIES})
add_dependencies(onboard_3d_tracker ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
