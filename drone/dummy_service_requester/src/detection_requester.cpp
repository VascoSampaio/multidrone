#include <ros/ros.h>
#include <ros/package.h>
#include <visualanalysis_msgs/Detect.h>
#include <multidrone_msgs/TargetType.h>

#include <fstream>

int main(int argc, char **argv) {
    int count = 0;
    ros::init(argc, argv, "detection_requester");

    ros::NodeHandle nh;

    //std::ofstream logfile;
    //std::string logfilename = nh.param<std::string>("logfilename", "log/detection_request.log");
    std::string path_to_node = ros::package::getPath("dummy_service_requester") + std::string("/");
    //ROS_INFO("Logging to %s", (path_to_node + logfilename).c_str());
    //logfile.open(path_to_node + logfilename);

    static int id = nh.param<int>("id", 0);
    int target_type = nh.param<int>("target_type", 1);

    ros::ServiceClient service_client = nh.serviceClient<visualanalysis_msgs::Detect>("/Detection");
    ROS_INFO("Requesting from service %s", service_client.getService().c_str());
    visualanalysis_msgs::Detect req;
    req.request.id = id;
    req.request.Area = visualanalysis_msgs::TargetPositionROI2D();
    req.request.Area.x = 0;
    req.request.Area.y = 0;
    req.request.Area.w = -1;
    req.request.Area.h = -1;
    req.request.TargetType = (uint64_t)target_type;

    ros::Rate loop_rate(20);
    while (ros::ok()) {
        if (service_client.call(req)) {
            ROS_INFO("Received positive request response, retrying at 1fps.");
            loop_rate = ros::Rate(1);
            ros::spinOnce();
            loop_rate.sleep();
        }
        else {
            ROS_INFO("Received negative request response, retrying...");
            ros::spinOnce();
            loop_rate.sleep();
            ++count;
        }
    }

    //logfile.close();
    return 0;
}
