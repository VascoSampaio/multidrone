#include <ros/ros.h>
#include <ros/timer.h>
#include <ros/console.h>

#include <sensor_msgs/Imu.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Vector3.h>
#include <multidrone_msgs/GimbalStatus.h>

#include <cmath>

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "tf_conversions/tf_eigen.h"
#include <Eigen/Dense>

#define SERIAL_PORT_READ_BUF_SIZE 256
#define SBGC_CMD_MAX_BYTES 255
#define SBGC_CMD_NON_PAYLOAD_BYTES 5
#define SBGC_RC_NUM_CHANNELS 6
#define SBGC_CMD_DATA_SIZE (SBGC_CMD_MAX_BYTES - SBGC_CMD_NON_PAYLOAD_BYTES)
#define SBGC_IMU_TO_DEG 0.02197265625
#define SBGC_IMU_TO_RAD (SBGC_IMU_TO_DEG/180.0*M_PI)
#define SBGC_CONTROL_MODE_NO_CONTROL 0
#define SBGC_CONTROL_MODE_SPEED 1
#define SBGC_CONTROL_MODE_ANGLE 2
#define SBGC_SPEED_SCALE  (1.0f/0.1220740379f)
#define SBGC_ANGLE_FULL_TURN 16384
#define SBGC_DEGREE_ANGLE_SCALE ((float)SBGC_ANGLE_FULL_TURN/360.0f)
#define SBGC_ANGLE_DEGREE_SCALE (360.0f/(float)SBGC_ANGLE_FULL_TURN)
#define SBGC_ANGLE_SCALE  (1.0f/SBGC_IMU_TO_DEG)
// Conversions for angle in degrees to angle in SBGC 14bit representation, and back
#define SBGC_DEGREE_TO_ANGLE(val) ((val)*SBGC_DEGREE_ANGLE_SCALE)
#define SBGC_ANGLE_TO_DEGREE(val) ((val)*SBGC_ANGLE_DEGREE_SCALE)

#define CMD_REALTIME_DATA_3       23
#define CMD_REALTIME_DATA_4       25
#define CMD_CONTROL		            67
#define CMD_REALTIME_DATA         68
#define CMD_DATA_STREAM_INTERVAL  85
#define CMD_REALTIME_DATA_CUSTOM  88

#define PUB_FREQ  30.0  // Hz

enum STATE
{
  STATE_WAIT, STATE_GOT_MARKER, STATE_GOT_ID, STATE_GOT_LEN, STATE_GOT_HEADER, STATE_GOT_DATA
};

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;

// CMD_CONTROL
typedef struct __attribute__((packed)) {
  // uint8_t mode;     // legacy format: mode is common for all axes
  uint8_t mode[3];	// in extended format (firmware ver. 2.55b5+): mode is set independently for each axes
  int16_t speedROLL;
  int16_t angleROLL;
  int16_t speedPITCH;
  int16_t anglePITCH;
  int16_t speedYAW;
  int16_t angleYAW;
} SBGC_cmd_control_t;

// CMD_REALTIME_DATA_3, CMD_REALTIME_DATA_4
typedef struct __attribute__((aligned)) {
  struct {
    int16_t acc_data;
    int16_t gyro_data;
  } sensor_data[3];  // ACC and Gyro sensor data (with calibration) for current IMU (see cur_imu field)
  uint16_t serial_error_cnt; // counter for communication errors
  uint16_t system_error; // system error flags, defined in SBGC_SYS_ERR_XX
  uint8_t reserved1[4];
  int16_t rc_raw_data[SBGC_RC_NUM_CHANNELS]; // RC signal in 1000..2000 range for ROLL, PITCH, YAW, CMD, EXT_ROLL, EXT_PITCH channels
  int16_t imu_angle[3]; // ROLL, PITCH, YAW Euler angles of a camera, 16384/360 degrees
  int16_t frame_imu_angle[3]; // ROLL, PITCH, YAW Euler angles of a frame, if known
  int16_t target_angle[3]; // ROLL, PITCH, YAW target angle
  uint16_t cycle_time_us; // cycle time in us. Normally should be 800us
  uint16_t i2c_error_count; // I2C errors counter
  uint8_t reserved2;
  uint16_t battery_voltage; // units 0.01 V
  uint8_t state_flags1; // bit0: motor ON/OFF state;  bits1..7: reserved
  uint8_t cur_imu; // actually selecteted IMU for monitoring. 1: main IMU, 2: frame IMU
  uint8_t cur_profile; // active profile number starting from 0
  uint8_t motor_power[3]; // actual motor power for ROLL, PITCH, YAW axis, 0..255

  // Fields below are filled only for CMD_REALTIME_DATA_4 command
  int16_t rotor_angle[3]; // relative angle of each motor, 16384/360 degrees
  uint8_t reserved3;
  int16_t balance_error[3]; // error in balance. Ranges from -512 to 512,  0 means perfect balance.
  uint16_t current; // Current that gimbal takes, in mA.
  int16_t magnetometer_data[3]; // magnetometer sensor data (with calibration)
  int8_t  imu_temp_celcius;  // temperature measured by the main IMU sensor, in Celsius
  int8_t  frame_imu_temp_celcius;  // temperature measured by the frame IMU sensor, in Celsius
  uint8_t reserved4[38];
} SBGC_cmd_realtime_data_t;

class SerialCommand {
  public:
    uint8_t pos;
    uint8_t id;
    uint8_t data[SBGC_CMD_DATA_SIZE];
    uint8_t len;

    void init(uint8_t _id) {
      id = _id;
      len = 0;
      pos = 0;
    }
};

class GimbalInterface
{
private:

  ros::Publisher status_pub;
  ros::Publisher euler_pub;

  boost::asio::io_service io_service_;
  serial_port_ptr port_;
  boost::mutex mutex_;

  std::string portName_;
  int baud_;

  char read_buf_raw_[SERIAL_PORT_READ_BUF_SIZE];
  std::string read_buf_str_;

  char end_of_line_char_ = '>';

  SerialCommand cmd_in, cmd_out;
  int len;
  uint8_t checksum = 0;
  enum STATE state = STATE_WAIT;

  int drone_id_;

  ros::Timer timer;
  ros::Timer timer_pub;

  Eigen::Quaterniond gimbal_quat;
  geometry_msgs::Vector3 gimbal_euler;
  geometry_msgs::Vector3 gimbal_ang_vel;
  bool has_gimbal_status_;

  void start();
  void async_read_some_();
  void on_receive_(const boost::system::error_code& ec, size_t bytes_transferred);
  void parseData(const std::string &data);
  void SBGC_cmd_control_pack(SBGC_cmd_control_t &p, SerialCommand &cmd);
  uint8_t SBGC_cmd_realtime_data_unpack(SBGC_cmd_realtime_data_t &p, SerialCommand &cmd);
  int write_some(const std::string &buf);
  int write_some(const char *buf, const int &size);
  void stop();
  void send_cmd();
  void timerCallback(const ros::TimerEvent&);
  void timerCallbackPub(const ros::TimerEvent&);
  void cmd_callback(const geometry_msgs::Vector3::ConstPtr& msg);

public:

  // Constructor
  GimbalInterface(int _argc, char** _argv)
  {
    ros::init(_argc, _argv, "Gimbal_Control_");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    pnh.param<int>("drone_id", drone_id_, 1);
    pnh.param<std::string>("portName", portName_, std::string("/dev/ttyUSB0"));
    pnh.param<int>("baud", baud_, 115200);

    ROS_INFO("Gimbal Controller [%d] initialized!", drone_id_);

    // Publisher
    status_pub   = nh.advertise<multidrone_msgs::GimbalStatus>("drone_"+std::to_string(drone_id_)+"/gimbal/status", 1);
    euler_pub    = nh.advertise<geometry_msgs::Vector3>("drone_"+std::to_string(drone_id_)+"/gimbal/euler", 1);
    // Subscribers
    ros::Subscriber cmd_sub      = nh.subscribe("drone_"+std::to_string(drone_id_)+"/gimbal/cmd",1,&GimbalInterface::cmd_callback, this);


    //open serial
    if (port_) {
      std::cout << "error : port is already opened..." << std::endl;
      ros::shutdown();
    }

    start();

    timer = nh.createTimer(ros::Duration(0.01), &GimbalInterface::timerCallback, this);
    timer_pub = nh.createTimer(ros::Duration(1/PUB_FREQ), &GimbalInterface::timerCallbackPub, this);

    ros::MultiThreadedSpinner spinner(4);
    spinner.spin();

    stop();

    return;
  }

  // Destructor
  ~GimbalInterface(){};
};

void GimbalInterface::start()
{
  boost::system::error_code ec;

  port_ = serial_port_ptr(new boost::asio::serial_port(io_service_));
  port_->open(portName_, ec);

  while (ec) {
    std::cout << "Waiting for UART connection on port : "
      << portName_ << " ( " << ec.message().c_str() << ")" << std::endl;
      port_->open(portName_, ec);
      ros::Duration(1).sleep();
  }

  std::cout << "UART connected on port : "
    << portName_ << " ( " << ec.message().c_str() << ")" << std::endl;

  // option settings...
  port_->set_option(boost::asio::serial_port_base::baud_rate(baud_));
  port_->set_option(boost::asio::serial_port_base::character_size(8));
  port_->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
  port_->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
  port_->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

  async_read_some_();

  // boost::asio::service should be launched after async_read_some so it has some work when launched preventing to return
  boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service_));
}

void GimbalInterface::async_read_some_()
{
  if (port_.get() == NULL || !port_->is_open()) return;

  port_->async_read_some(
    boost::asio::buffer(read_buf_raw_, SERIAL_PORT_READ_BUF_SIZE),
    boost::bind(
      &GimbalInterface::on_receive_,
      this, boost::asio::placeholders::error,
      boost::asio::placeholders::bytes_transferred));
}

/** \brief read loop on serial port
 *  \details this function read data on serial port and send every good frame to ds301 stack
 *  Warning : - this is a blocking function
 *            - init must be call before calling this function
 */
void GimbalInterface::on_receive_(const boost::system::error_code& ec, size_t bytes_transferred)
{
  boost::mutex::scoped_lock lock(mutex_);
  if (port_.get() == NULL || !port_->is_open()) return;
  if (ec) {
    async_read_some_();
    return;
  }

  for (unsigned int i = 0; i < bytes_transferred; ++i) {
    char c = read_buf_raw_[i];
    if (c == end_of_line_char_) {
      parseData(read_buf_str_);
      read_buf_str_.clear();
    }
    read_buf_str_ += c;
  }

  async_read_some_();
}

void GimbalInterface::parseData(const std::string &data)
{

  if(data.size() == 0)
      return;

  uint8_t c;
  for(int i = 0; i < data.size(); i++){
    c = data.at(i);
    switch(state)
    {
      case STATE_WAIT:
        if(c == '>'){
          state = STATE_GOT_MARKER;
          ROS_DEBUG_STREAM("GOT_MARKER" << std::endl);
        }
        break;

      case STATE_GOT_MARKER:
        cmd_in.init(c);
        state = STATE_GOT_ID;
        ROS_DEBUG_STREAM("GOT_ID" << std::endl);
        break;

      case STATE_GOT_ID:
        len = c;
        state = STATE_GOT_LEN;
        ROS_DEBUG_STREAM("GOT_LEN" << std::endl);
        break;

      case STATE_GOT_LEN:
        if(c == (uint8_t)(cmd_in.id+len) && len <= sizeof(cmd_in.data)){
          checksum = 0;
          state = (len == 0) ? STATE_GOT_DATA : STATE_GOT_HEADER;
          if(len == 0)
            ROS_DEBUG_STREAM("GOT_DATA" << std::endl);
          else
            ROS_DEBUG_STREAM("GOT_HEADER" << std::endl);
        }else{
          state = STATE_WAIT;
          ROS_DEBUG_STREAM("WRONG HEADER CHECKSUM" << std::endl);
        }

        break;

      case STATE_GOT_HEADER:
        cmd_in.data[cmd_in.len++] = c;
        checksum+=c;
        if(cmd_in.len == len){
            state = STATE_GOT_DATA;
            ROS_DEBUG_STREAM("GOT_DATA" << std::endl);
          }
        break;

      case STATE_GOT_DATA:
        state = STATE_WAIT;
        if(c != checksum){
          ROS_DEBUG_STREAM("WRONG DATA CHECKSUM" << std::endl);
          return;
        }else
          ROS_DEBUG_STREAM("DATA CHECKSUM VERIFIED" << std::endl);
        break;
    }
  }

  if(state == STATE_WAIT){

	  if(cmd_in.id == CMD_REALTIME_DATA_3 || cmd_in.id == CMD_REALTIME_DATA_4){

      SBGC_cmd_realtime_data_t rt_data;
      SBGC_cmd_realtime_data_unpack(rt_data, cmd_in);


      gimbal_quat = Eigen::AngleAxisd(M_PI, Eigen::Vector3d::UnitX()) * \
                    Eigen::AngleAxisd(M_PI/2, Eigen::Vector3d::UnitZ()) * \
                    Eigen::AngleAxisd(rt_data.imu_angle[2]*SBGC_IMU_TO_RAD, Eigen::Vector3d::UnitZ()) * \
                    Eigen::AngleAxisd(rt_data.imu_angle[1]*SBGC_IMU_TO_RAD, Eigen::Vector3d::UnitX()) * \
                    Eigen::AngleAxisd(rt_data.imu_angle[0]*SBGC_IMU_TO_RAD, Eigen::Vector3d::UnitY());

      /*msg_status.linear_acceleration.x = -rt_data.sensor_data[0].acc_data/512.0*9.81;
      msg_status.linear_acceleration.y = -rt_data.sensor_data[1].acc_data/512.0*9.81;
      msg_status.linear_acceleration.z =  rt_data.sensor_data[2].acc_data/512.0*9.81;*/

      gimbal_ang_vel.x = -rt_data.sensor_data[0].gyro_data/32767.0*2000.0*M_PI/180.0;
      gimbal_ang_vel.y = -rt_data.sensor_data[1].gyro_data/32767.0*2000.0*M_PI/180.0;
      gimbal_ang_vel.z =  rt_data.sensor_data[2].gyro_data/32767.0*2000.0*M_PI/180.0;

      gimbal_euler.x = rt_data.imu_angle[1]*SBGC_IMU_TO_DEG;
      gimbal_euler.y = rt_data.imu_angle[0]*SBGC_IMU_TO_DEG;
      gimbal_euler.z = rt_data.imu_angle[2]*SBGC_IMU_TO_DEG;

      has_gimbal_status_ = true;

    } else{
		    ROS_DEBUG("> %d\n", cmd_in.id);
    }
  }
}

/* Packs command structure to SerialCommand object */
void GimbalInterface::SBGC_cmd_control_pack(SBGC_cmd_control_t &p, SerialCommand &cmd)
{
	cmd.init(CMD_CONTROL);
	memcpy(cmd.data, &p, sizeof(p));
	uint8_t tmp = sizeof(p);
	cmd.len = tmp;
}

  /*
* Unpacks SerialCommand object to command structure.
* Returns 0 on success, PARSER_ERROR_XX code on fail.
*/
uint8_t GimbalInterface::SBGC_cmd_realtime_data_unpack(SBGC_cmd_realtime_data_t &p, SerialCommand &cmd)
{
    if(cmd.len <= sizeof(p)) {
      memcpy(&p, cmd.data, cmd.len);
      return 0;
    } else
      return 1;
}

int GimbalInterface::write_some(const std::string &buf)
{
  return write_some(buf.c_str(), buf.size());
}

int GimbalInterface::write_some(const char *buf, const int &size)
{
  boost::system::error_code ec;

  if (!port_) return -1;
  if (size == 0) return 0;

  return port_->write_some(boost::asio::buffer(buf, size), ec);
}

void GimbalInterface::stop()
{
  boost::mutex::scoped_lock lock(mutex_);

  if (port_) {
    port_->cancel();
    port_->close();
    port_.reset();
  }
  io_service_.stop();
  io_service_.reset();
}

void GimbalInterface::send_cmd()
{

  std::string data_str(">");
  data_str.push_back(cmd_out.id);
  data_str.push_back(cmd_out.len);
  data_str.push_back(cmd_out.id+cmd_out.len);

  uint8_t checksum = 0;
  for(int i = 0; i < cmd_out.len; i++){
    data_str.push_back(cmd_out.data[i]);
    checksum+=cmd_out.data[i];
  }
  data_str.push_back(checksum);

  int rep = write_some(data_str);
  if (!rep) {
    ROS_WARN("ERROR: No serial output");
  }

}

void GimbalInterface::timerCallback(const ros::TimerEvent&)
{
  char data_[] = {'>', CMD_REALTIME_DATA_3, 0, CMD_REALTIME_DATA_3, 0, 0};
  write_some((char*)data_, 6);
}

void GimbalInterface::timerCallbackPub(const ros::TimerEvent&)
{
  if (has_gimbal_status_) {
    multidrone_msgs::GimbalStatus msg_status;
    geometry_msgs::Vector3 msg_euler;

    msg_status.header.stamp = ros::Time::now();
    msg_status.orientation.x = gimbal_quat.x();
    msg_status.orientation.y = gimbal_quat.y();
    msg_status.orientation.z = gimbal_quat.z();
    msg_status.orientation.w = gimbal_quat.w();
    msg_status.angular_velocity = gimbal_ang_vel;

    msg_euler = gimbal_euler;

    status_pub.publish(msg_status);
    euler_pub.publish(msg_euler);
  }
  has_gimbal_status_ = false;
}

void GimbalInterface::cmd_callback(const geometry_msgs::Vector3::ConstPtr& msg)
{
  int16_t speedROLL_   = msg->x;
	int16_t speedPITCH_  = msg->y;
	int16_t speedYAW_    = msg->z;

	SBGC_cmd_control_t c = {0,0,0,0,0,0,0,0,0};

  c.mode[0]    = SBGC_CONTROL_MODE_SPEED;
  c.mode[1]    = SBGC_CONTROL_MODE_SPEED;
  c.mode[2]    = SBGC_CONTROL_MODE_SPEED;
  c.speedROLL  = speedROLL_*SBGC_SPEED_SCALE;
  c.speedPITCH = -speedPITCH_*SBGC_SPEED_SCALE;
  c.speedYAW   = speedYAW_*SBGC_SPEED_SCALE;
  c.angleROLL  = 0;
  c.anglePITCH = 0;
  c.angleYAW   = 0;

  SBGC_cmd_control_pack(c, cmd_out);
  send_cmd();

}

int main(int _argc, char **_argv)
{
  ROS_INFO("Setting up Gimbal Control node");
  sleep(2);
  GimbalInterface gimbalInterface(_argc,_argv);
  return 0;
}
