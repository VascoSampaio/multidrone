#!/bin/bash
multidrone_msgs_dir=$( rospack find multidrone_msgs )
msgs_subdir="/common/multidrone_msgs"
multidrone_repo=${multidrone_msgs_dir/%$msgs_subdir}
touch "${multidrone_repo}/ground/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/darknet_ros/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/drone_visual_analysis/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/dummy_service_requester/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/dummy_service_requester_2/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/video_streamer/CATKIN_IGNORE"
