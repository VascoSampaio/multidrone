#!/bin/bash
multidrone_msgs_dir=$( rospack find multidrone_msgs )
msgs_subdir="/common/multidrone_msgs"
multidrone_repo=${multidrone_msgs_dir/%$msgs_subdir}
touch "${multidrone_repo}/common/multidrone_kml_parser/CATKIN_IGNORE"
touch "${multidrone_repo}/ground/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/action_executer/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/camera_control/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/dummy_service_requester/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/dummy_service_requester_2/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/gimbal_interface/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/gimbal_interface_small/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/onboard_3d_tracker/CATKIN_IGNORE"
touch "${multidrone_repo}/drone/onboard_scheduler/CATKIN_IGNORE"
