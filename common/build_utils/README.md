# build_utils

This folder contains a few scripts and instructions for building some parts of the MULTIDRONE repository.

* build_vision_instructions.md -> Instructions for building the dependencies of the vision packages
* ignore_vision.bash -> Puts CATKIN_IGNORE files in the vision packages
* ignore_drone.bash -> Puts CATKIN_IGNORE file in the drone folder
* NUC_config.bash -> Puts CATKIN_IGNORE files in the packages that don't need to compile in the NUC
* TX2_config.bash -> Puts CATKIN_IGNORE files in the packages that don't need to compile in the TX2
* remove_ignore_files.bash -> Removes every CATKIN_IGNORE file in the repo