# Instructions for running multimaster

Each machine should have its own configuration file "mm_machinename.launch".

Launch the corresponding file on each machine to run the nodes "master_discovery" and "master_sync".

Enjoy the ROS multimaster connection!