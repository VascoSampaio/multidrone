#ifndef INTERFACE_TEST_H
#define INTERFACE_TEST_H

//Test parameters
#include <ros/ros.h>
#include <stdlib.h>
#include <fstream>


void test_setup(std::string test_id);
void run_test(std::string test_id, ros::Time timestamp, bool &interface_test_flag);
void run_test(std::string test_id, bool &interface_test_flag);
std::map<std::string, double> first_received;
std::map<std::string, double> last_received;
std::map<std::string, int> count;
std::map<std::string, double> freq_sum;
std::map<std::string, double> freq_min;
std::map<std::string, double> freq_max;
std::map<std::string, double> latency_max;
std::map<std::string, double> latency_sum;

std::map<std::string, double> desired_freq;
std::map<std::string, double> max_allowed_freq_shift;
std::map<std::string, double> test_nr;
std::map<std::string, double> max_allowed_time;
std::map<std::string, double> max_latency_time;

bool interface_test_aeual = false;
bool interface_test_aeual_2 = false;
bool interface_test_aegi = false;
bool interface_test_aeci = false;
bool interface_test_aevsa = false;
bool interface_test_aeos = false;
bool interface_test_aeot = false;
bool interface_test_aecc = false;
bool interface_test_giae = false;
bool interface_test_rtkti = false;

#endif // INTERFACE_TEST_H