#xterm -e roslaunch ae-vsa-topics.launch &

#xterm -e roslaunch video_stream_opencv bmmcc.launch &

#cd
#cd rosdemo/launchers/camera_face/
#./ssd_create_and_launch.sh &

#cd ../camera_face_VSA
#./VSA.sh


sleep 2
tmux split-window -v -p 50 "roslaunch ae-vsa-topics.launch"
#tmux split-window -v -p 50
sleep 2
tmux split-window -h -p 10 "roslaunch video_stream_opencv bmmcc.launch"
sleep 2
tmux split-window -h -p 10 "cd ~/rosdemo/launchers/camera_face && . ssd_create_and_launch.sh"
sleep 1
tmux split-window -h -p 10 "cd ~/rosdemo/launchers/camera_face_VSA && . VSA.sh"
tmux split-window -h -p 10 "rostopic echo /visual_shot_analysis"
#tmux select-layout even-horizontal