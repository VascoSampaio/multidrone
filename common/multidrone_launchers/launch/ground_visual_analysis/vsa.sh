#!/bin/bash
source ../../../../../../devel/setup.bash
chmod +x $(rospack find ground_visual_analysis)/scripts/crowd_node.py
# $CFG_FILE="$(rospack find ground_visual_analysis)/scripts/crowd_detector_test.cfg"
printf "{\"emulation\": \"airsim\" \n\
,\"emulation_image_topic\": \"drone_%%d/image_raw/compressed\"\n\
,\"emulation_debug_folder\": \"/home/ekakalet/Documents/AUTHCrowdData/HD\"\n\
,\"dji_demo_data\":\"/home/ekakalet/Documents/AUTHCrowdData/HD\"\n\
,\"airsim_demo_data\": \"/home/ekakalet/Documents/crowd2/BP_FlyingPawn\"\n\
,\"drone_ID\":\"1\"\n\
,\"demo_frame_delay\": 1\n\
,\"visualization_number_of_drones\": 1\n\
,\"visualization_show_all\": 0 \n\
,\"visualization_is_blending\": 0 \n\
,\"visualization_cmap\": \"seismic\" \n\
,\"visualization_vmax\": 0.85 \n\
,\"visualization_blending_amount\": 0.25 \n\
,\"visualization_blending_color_map\": \"PuRd\" \n\
,\"visualization_tight_layout\": 1 \n\
,\"_visualization_figure_size\": [5,5] \n\
,\"is_saving_heatmap\": 1\n\
,\"is_saving_input_image\": 1\n\
,\"is_thresholding\": 0\n\
,\"is_logging_timings\": 0\n\
,\"is_visualizing_input\": 0\n\
,\"is_visualizing_output\": 0\n\
,\"is_debugging\": 0\n\
,\"nn_input_size\": 640 \n\
,\"nn_output_scale\": 1 \n\
,\"nn_debug_level\": 1 \n\
,\"ros_queue_size\": 1 \n\
,\"number_of_drones\": 3 \n\
,\"model_name\": \"baseline\" \n\
,\"is_showing_heatmap\": 1 \n}" > $(rospack find ground_visual_analysis)/scripts/crowd_detector.cfg

#xterm -e roslaunch gscam drone1.launch &
#xterm -e roslaunch gscam drone2.launch &
#xterm -e roslaunch gscam drone3.launch &
xterm -hold -e  rosrun ground_visual_analysis crowd_node.py    

