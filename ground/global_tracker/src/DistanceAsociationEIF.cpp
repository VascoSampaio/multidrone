#include <DistanceAsociationEIF.h>

// Constructor
DistanceAsociationEIF::DistanceAsociationEIF(){
    initialize();
}
// Destructor
DistanceAsociationEIF::~DistanceAsociationEIF(){}

// Guarda la última tabla con datos de asoación y EIF
// También llama a push_targets3Dtable para guardar la parte correspondiente
// de los datos en la tabla de targets de la ventana de tiempo
void DistanceAsociationEIF::push_EIFasocTable(std::vector <global_tracker::EIFasoc_P2P> newTable){
    EIFasocTable = newTable;
    global_tracker::Targets3DArray_P2P targetList;
    std::vector <global_tracker::Targets3DArray_P2P> targetTable;
    for(unsigned int i = 0; i < newTable.size(); i++){
        targetList.group_id = newTable[i].group_id;
        targetList.destination_id = newTable[i].destination_id;
        targetList.source_id = newTable[i].source_id;
        targetList.type = newTable[i].type;
        targetList.nseq = newTable[i].nseq;
        targetList.targets3D = newTable[i].targets3D;
        targetList.targetsID = newTable[i].targetsID;
        targetTable.push_back(targetList);
    }
    push_targets3Dtable(targetTable);
}

void DistanceAsociationEIF::push_EIFasocTableWithGPSdata(std::vector <global_tracker::EIFasoc_P2P> newTable){
    if(lastGPSdata.targets3D.size() > 0){
        newTable.push_back(lastGPSdata);
        //clearGPSdataVector();	// Se borran las medidas de gps guardadas desde la última iteración del EIF, ya han sido utilizadas
    }

    push_EIFasocTable(newTable);
}

// Introduce una nueva tabla de targets 3D para ser asociada
void DistanceAsociationEIF::push_targets3Dtable(std::vector <global_tracker::Targets3DArray_P2P> newTable){
    // Se añade al inicio del vector la nueva tabla
    targets3Dtables.push_front(newTable);
    // Si se ha superado el tamaño máximo para la ventana de tiempo se borra el último elemento
     if(targets3Dtables.size() > asociation_window){
         targets3Dtables.pop_back();
     }
}

// Actualiza los emparejamientos de la track list usando la distancia de cada target
// de la nueva tabla de medidas con los estados de cada track
// Llamar en la fase de actualización tras añadir la nueva tabla de medidas
// con la función push_targets3Dtable
// Tras la llamada de esta función debe ser llamada la función updateTrackListPairingsTimeWindow
// para emparejar los targets que hayan sido marcados
void DistanceAsociationEIF::updateTrackListPairingsByState(std::vector <Track> &tl, std::vector <std::vector <bool>> &markedTargets){
    // Se va a recorrer cada target de cada device de la última tabla añadida. La distancia de 
    // este con cada track (que debe haber sido anteriormente actualizado) será calculada.
    // El número de tracks cercanos (dado un umbral de cercanía) indicará el emparejamiento
    // de dicho target o si el target debe ser marcado para ser tratado por el emparejamiento
    // por ventana de tiempo

    // Con las medidas de GPS se actúa de distinta forma
    // 1. Los track de gps llevan la id de las propias medidas del gps. Por tanto, si una medida tiene
    //    el mismo id que un track de gps se trata de una medida de ese track directamente y solo es 
    //    necesario actualizar el tiempo desde el último emparejameinto del track
    // 2. Si no existía ningún track con la id de la medida del gps, se busca si algún track visual
    //    está cerca de la medida de gps. En caso de que se parezca a 1 sólo track, se empareja con ese
    //    track y se cambia la id común del track por la de la medida del gps (reordenar trackList). Dicho de otra forma,
    //    el track visual es convertido a track gps. En caso no estar cerca de ningún track visual
    //    o cerca de varios, se envía a la asociación por proyección.
    
    // Si no hay tracks no tiene sentido asociar usando los estados de los tracks
    if(tl.size() > 0){
        double targetdist;
        unsigned int ndist;
        std::vector <unsigned int> indexList;   // Índices de los tracks con los que el target se encuentra cerca
        target_local_id auxLocalTarget;
        double eudist;
        int track_index;
        bool gps_measure;
        int firstCompareTrackIndex;
        int firstVisualTrackIndex = findFirstVisualTrackIndex();    // Índice del primer track visual
        if(firstVisualTrackIndex == -1){    // No hay tracks visuales
            // No se comparán medidas de gps con la lista de tracks (porque o no hay tracks o todos son de gps)
            firstVisualTrackIndex = tl.size();
        }
        
        for(unsigned int i = 0; i < targets3Dtables[0].size(); i++){ // Bucle que recorre los distintos devices (drones) de la última tabla            
            for(unsigned int j = 0; j < targets3Dtables[0][i].targets3D.size(); j++){ // Bucle que recorre los targets de dicho device de la última tabla
                if(targets3Dtables[0][i].source_id == GPS_VIRTUAL_DEVICE_ID){   // Device virtual con las medidas de los sensores
                    track_index = findGPSTrack(targets3Dtables[0][i].targetsID[j]);
                    firstCompareTrackIndex = firstVisualTrackIndex; // Sólo se compara con tracks visuales
                    gps_measure = true;
                }else{  // medida visual, seguir procedimiento habitual
                    track_index = -1;
                    firstCompareTrackIndex = 0;  // Comparar todos los tracks
                    gps_measure = false;                    
                }
                if(track_index == -1){  // No existe un track con la id del gps o es una medida visual
                    indexList.clear();
                    // Si se trata de una medida gps sólo se compara con los tracks visuales
                    // no tiene sentido comparar con los tracks gps
                    //printf("\nTarget %d,%d. firstCompareTrackIndex: %d",i,j,firstCompareTrackIndex);   // DEBUG
                    for(unsigned int k = firstCompareTrackIndex; k < tl.size(); k++){    // Bucle que recorre los distintos tracks
                        // Se calcula la distancia en esta última tabla (última medida) entre el target y el track
                        targetdist = euclideanDistance(targets3Dtables[0][i].targets3D[j],tl[k].state);
                        ndist = 1;
                        // Se suman las distancias entre mismo target y track en instante anteriores
                        // (siguientes tablas y elementos de ambas ventanas de tiempo)
                        // (siempre que el target y el track tienen medidas y estimación en dichos instantes de tiempos)
                        // También se controla el número de distancias sumadas para calcular la media
                        for(int t = 1; t < targets3Dtables.size(); t++){
                            // Se busca en la tabla t, el track y el target del drone comparado justo arriba en la tabla 0
                            auxLocalTarget.device_id = targets3Dtables[0][i].source_id;
                            auxLocalTarget.target_local_id = targets3Dtables[0][i].targetsID[j];
                            if(findTrackTargets3Ddistance(t, k, auxLocalTarget, eudist) != -1){
                                targetdist += eudist;
                                ndist++;                                
                                //std::cout << "pre-targetdist: " << targetdist << " ndist: " << ndist << std::endl;    // DEBUG
                            }else{
                                //std::cout << "Comparación imposible en anterior tabla " << t << std::endl;  // DEBUG
                            }
                        }

                        // Se calcula la distancia media media
                        targetdist /= ndist;
                        
                        //std::cout << "\n--targetdist media: " << targetdist << " ndist: " << ndist;    // DEBUG

                        if(targetdist <= track_asociation_th){
                            indexList.push_back(k);
                        }
                    }
                    //printf("\nindexList.size(): %u", (unsigned int)indexList.size());   // DEBUG
                    // En este punto es conocido el número de tracks que están cerca del target analizado
                    // y los índices de dichos tracks
                    // Se actua en función del número de tracks cercanos al target
                    auxLocalTarget.device_id = targets3Dtables[0][i].source_id;
                    auxLocalTarget.target_local_id = targets3Dtables[0][i].targetsID[j];
                    if(indexList.size() == 1){
                        if(!gps_measure){   // Medida visual
                            // Se comprueba si el target ya está emparejado con ese track
                            // en ese caso no se hace nada, xk el emparejameinto es ya correcto
                            // En caso contrario se añade el target a la lista de targets emparejados del track
                            // Si el track ya estaba emparejado al track no se tiene que buscar si el target
                            // está emparejado a otros tracks para borrarlos porque se supone que si todo funciona
                            // correctamente el target sólo aparece una vez en toda la track list, en el caso 
                            // mencionado, en el track dónde debe estar.
                            if(findTargetLocalPairingList(tl[indexList[0]].pairings,auxLocalTarget) == -1){ // El target no estaba emparejado en ese track
                                // Primero se busca y elimina el target si estuviera emparejado a otros tracks                    
                                eraseTargetFromTrackVector(tl,auxLocalTarget);
                                // Una vez asegurada la unicidad del target se añade al track
                                tl[indexList[0]].pairings.targets.push_back(auxLocalTarget);
                                //printf("\nAñadiendo target %d, del drone %d al track %d",auxLocalTarget.target_local_id,auxLocalTarget.device_id,tl[indexList[0]].pairings.common_id);
                            }                            
                        }else{  // Medida gps      
                            // Se añade el emparejamiento de la medida
                            tl[indexList[0]].pairings.targets.push_back(auxLocalTarget);
                            // Se cambia la id común del track para convertir el track a tipo track gps
                            //tl[indexList[0]].pairings.common_id = targets3Dtables[0][i].targetsID[j];
                            // Se reordena la trackList
                            changeTrackID(indexList[0],targets3Dtables[0][i].targetsID[j]);
                        }
                        // En cualquiera de los casos se refresca el tiempo sin emparejamiento del track
                        tl[indexList[0]].time_since_last_pairing = 0;
                        // También se desmarca el target para no ser asociado por proyección
                        markedTargets[i][j] = false;
                        //printf("\nTarget %d,%d desmarcado\n",i,j);   // DEBUG
                    }else{  // 0 o > 1 tracks cercanos al target, target enviado a asociación por proyección
                        // Si el target había sido emparejado anteriormente con algún track debe ser
                        // borrado el emparejamiento
                        if(!gps_measure){   // Con total seguridad la medida de gps no estrá emparejada
                            eraseTargetFromTrackVector(tl,auxLocalTarget);
                        }
                        //printf("\nTarget %d,%d. %u tracks cercanos. Se asocia por proyección",i,j,(unsigned int)indexList.size());   // DEBUG
                    }
                }else{  // El track gps había sido creado anteriormente
                    // Se resetea el tiempo desde útlimo emparejamiento
                    trackList[track_index].time_since_last_pairing = 0;
                    // También se desmarca el target para no ser asociado por proyección
                    markedTargets[i][j] = false;
                    //printf("\nTarget gps %d,%d, desmarcado (ya existia su track)\n",i,j);   // DEBUG
                }
            }
        }
    }// Si no hay tracks se pasan todos los targets para ser asociados mediante proyección
}

// Ordena la trackList por la id de los tracks
void DistanceAsociationEIF::sortTrackList(){
    // Deben existir al menos dos tracks
    if(trackList.size() > 1){
        Track auxTrack;
        Eigen::MatrixXd aux_sigmat1;
        Eigen::MatrixXd aux_psigmat;
        Eigen::MatrixXd aux_sigmat;

        Eigen::MatrixXd aux_ept1;
        Eigen::MatrixXd aux_pept;
        Eigen::MatrixXd aux_ept;

        Eigen::MatrixXd aux_mut1;
        Eigen::MatrixXd aux_pmut;
    std::vector<Eigen::MatrixXd> covariance1;
        for(unsigned int i = 1; i < trackList.size(); i++){
            if(trackList[i-1].pairings.common_id > trackList[i].pairings.common_id){
                auxTrack = trackList[i];
                aux_sigmat1 = sigmat1[i];
                aux_psigmat = psigmat[i];
                aux_sigmat = sigmat[i];
                aux_ept1 = ept1[i];
                aux_pept = pept[i];
                aux_ept = ept[i];
                aux_mut1 = mut1[i];
                aux_pmut = pmut[i];

                trackList[i] = trackList[i-1];
                sigmat1[i] = sigmat1[i-1];
                psigmat[i] = psigmat[i-1];
                sigmat[i] = sigmat[i-1];
                ept1[i] = ept1[i-1];
                pept[i] = pept[i-1];
                ept[i] = ept[i-1];
                mut1[i] = mut1[i-1];
                pmut[i] = pmut[i-1];

                trackList[i-1] = auxTrack;
                sigmat1[i-1] = aux_sigmat1;
                psigmat[i-1] = aux_psigmat;
                sigmat[i-1] = aux_sigmat;
                ept1[i-1] = aux_ept1;
                pept[i-1] = aux_pept;
                ept[i-1] = aux_ept;
                mut1[i-1] = aux_mut1;
                pmut[i-1] = aux_pmut;
            }
        }
    }
}

// Cambia la id de un track y reordena la trackList
void DistanceAsociationEIF::changeTrackID(unsigned int track_index, unsigned int newID){
    printf("\n\nCambiando ID del track %d (index: %d), nueva ID: %d\n\n", trackList[track_index].pairings.common_id,track_index, newID);  // DEBUG

    // Se cambia la id del track indicado
    trackList[track_index].pairings.common_id = newID;

    // Se reordenar la trackList
    sortTrackList();    
}

// Esta función sumará las contribuciones a cada track
// Para ello recorrerá la lista de emparejamientos de cada track y buscará si han llegado 
// medidas del correspondiente target del drone emparejado
// Debe haberse realizado la asociación antes de este paso para la última tabla de medidas
void DistanceAsociationEIF::addContributions(){
    int index_device, index_target;
    Eigen::MatrixXd sigmat_contrib(ORDER,ORDER);
    Eigen::MatrixXd ept_contrib(ORDER,1);
    Eigen::MatrixXd T(4,4);
    Eigen::MatrixXd z_visual(2,1);
    Eigen::MatrixXd z_gps(3,1);
    Eigen::MatrixXd Qgps_k(3,3);    // Matriz de covarianza del gps

    if(EIFasocTable.size() > 0){
        //int contributionNumber; // DEBUG
        for(unsigned int i = 0; i < trackList.size(); i++){
            //printf("\nTrack %d. Sumando contribuciones.",i);    // DEBUG
            //contributionNumber = 0; // DEBUG
            // Se recorren los emparejamientos del track para buscar si se han recibido medida de ellos esta iteración
            for(unsigned int j = 0; j < trackList[i].pairings.targets.size(); j++){            
                // Un target de un drone emparejado podría no haber enviado medidas esta iteración
                // por ello debe comprobarse si se han recibido medidas del target esta iteración y buscar sus índices
                // Primero se busca el índice del drone (device) en la tabla de targets3D (los índices serán iguales que en 
                // la tabla con los datos de EIF "EIFasocTable"). La tabla EIFasocTable es igual al la última tabla de la 
                // ventana de tiempo, sólo que contiene los datos de las medidas para el EIF
                index_device = findDeviceTableIndex(0,trackList[i].pairings.targets[j].device_id);
                if(index_device >= 0){  // Device encontrado en la última tabla
                    // Se busca el índice del target
                    index_target = findIndexUint16(trackList[i].pairings.targets[j].target_local_id, targets3Dtables[0][index_device].targetsID);
                    if(index_target >= 0){  // Target encontrado
                        // Se calcula la contribución y se añade al cálculo de la estimación del filtro EIF
                        // Cada tipo de medida tiene su modelo de observación
                        if(trackList[i].pairings.targets[j].device_id == GPS_VIRTUAL_DEVICE_ID){    // target gps
                            z_gps(0) = EIFasocTable[index_device].targets3D[index_target].x;
                            z_gps(1) = EIFasocTable[index_device].targets3D[index_target].y;
                            z_gps(2) = EIFasocTable[index_device].targets3D[index_target].z;
                            
                            if(fixed_sigmaGPS){ // Matriz de covarianza de medidas gps fija
                                calculateContribution(z_gps, Hgps*pmut[i], Hgps, pmut[i], Qgps, ept_contrib, sigmat_contrib);
                            }else{  // Matriz de covarianza de medidas gps variable (viene con cada medida)
                                // Se recupera la covarianza del GPS para el cálculo de la contribución                            
                                for(int h = 0; h < 3; h++){
                                    for(int k = 0; k < 3; k++){
                                        Qgps_k(h,k) = lastGPSdata.contrib[index_target].covariance[h*6 + k];
                                    }
                                }
                                calculateContribution(z_gps, Hgps*pmut[i], Hgps, pmut[i], Qgps_k, ept_contrib, sigmat_contrib);
                            }
                            //printf("\nSumada contribución de target (%d,%d). Index (%d,%d). Tipo gps.",trackList[i].pairings.targets[j].device_id,trackList[i].pairings.targets[j].target_local_id, index_device, index_target);   // DEBUG
                        }else{  // target visual                        
                            z_visual(0) = EIFasocTable[index_device].measurements[index_target].z.x;
                            z_visual(1) = EIFasocTable[index_device].measurements[index_target].z.y;
                            // Se recupera (por filas) la matriz de transformación
                            for(int h = 0; h < 4; h++){
                                for(int k = 0; k < 4; k++){
                                    T(h,k) = EIFasocTable[index_device].measurements[index_target].T[h*4 + k];
                                }
                            }
                            calculateVisualContribution(z_visual, T, pmut[i], Qvisual, ept_contrib, sigmat_contrib);
                            //printf("\nSumada contribución de target (%d,%d). Index (%d,%d). Tipo visual.",trackList[i].pairings.targets[j].device_id,trackList[i].pairings.targets[j].target_local_id, index_device, index_target);   // DEBUG
                            //ept[i] += ept_contrib;  // DEBUG
                            //sigmat[i] += sigmat_contrib;        // DEBUG
                            // DEBUG
                            /*printf("\nSumando:\n");
                            std::cout << ept_contrib << std::endl;
                            printf("\n");
                            std::cout << sigmat_contrib << std::endl;*/
                            // DEBUG
                        }
                        // Se añade a la estimación del correspondiente track
                        ept[i] += ept_contrib;
                        sigmat[i] += sigmat_contrib;

                        //contributionNumber++;   // DEBUG
                    }
                }
            }
            // DEBUG
            //printf("\ntrack %d, contributionNumber: %d\n",i,contributionNumber);
            // DEBUG
        }

        // Los datos de EIFasocTable deben ser borrados en este punto para evitar que vuelvan a ser utilizados
        EIFasocTable.clear();
    }
}

// Función que guarda el vector y matriz de información para la siguiente iteración
// Es el último paso en la iteración dle filtro EIF
void DistanceAsociationEIF::saveEstimation(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat1[i] = sigmat[i];
	    ept1[i] = ept[i];
    }
}

// Actualiza el estado de los track con el último calculo realizado por el filtro EIF
// Tambien guarda el resultado del filtro en la forma canónica del filtro de kalman
// (mut1 y covariance1)
// Además suma uno a los tiempos de vida del track (se suponque esta función es llamada
// al final de cada iteración)
// Si un target lleva demasiado tiempo vivo sin el refresco o añadido de un pairing
// se elimina el track por deshuso
// También actualiza el último guardado en la ventana de tiempo de estados
void DistanceAsociationEIF::updateTrackListStates(){
    std::vector <unsigned int> indexList;  // Lista con los índices de los tracks por borrar 
    for(unsigned int i = 0; i < trackList.size(); i++){
        covariance1[i] = sigmat[i].inverse();
        mut1[i] = covariance1[i]*ept[i];

        trackList[i].state.x = mut1[i](0);
        trackList[i].state.y = mut1[i](1);
        trackList[i].state.z = mut1[i](2);

        trackList[i].stateTW[0].x = mut1[i](0);
        trackList[i].stateTW[0].y = mut1[i](1);
        trackList[i].stateTW[0].z = mut1[i](2);

        // Update de los tiempos de vida
        trackList[i].life_time++;
        trackList[i].time_since_last_pairing++;
        // Se guardan los tracks para ser borrados por deshuso
        if(trackList[i].time_since_last_pairing >= max_time_without_pairing_refresh){
            indexList.push_back(i);
        }
    }

    // Se borran los tracks marcados
    if(indexList.size() > 0){
        for(unsigned int i = 0; i < indexList.size(); i++){
            eraseTrack(indexList[i] - i);
        }
    }
}

// Suma la predicción a la estimacion final del EIF para cada track
void DistanceAsociationEIF::addPrediction(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat[i] += psigmat[i];	
		ept[i] += pept[i];
    }
}

// Actualiza la lista de tracks con un modelo de predicción
// Recordar llamar a setPredictionModel al menos una vez antes de llamar esta función
// sin el modelo la predicción no tiene sentido
// También es necesario haber indicado el ruido del modelo y las medidas
// lamando a setPredictionModelTrust y setMeasureTrust
void DistanceAsociationEIF::updateTrackListStatePredictionModel(std::vector <Track> &tl){
    // Se ejecuta el modelo de predicción y se actualiza el estado de cada track
    // (el estado del track es parte del vector de estados del filtro EIF)
    geometry_msgs::Point trackPos;
    for(unsigned int i = 0; i < trackList.size(); i++){
        // Prediction (Se realizan los cálculos)
        mut1[i] = (sigmat1[i].inverse())*ept1[i];   // NOTA. CREO QUE ESTA OPERACIÓN ES REDUNDANTE XK YA SE HACE AL FINAL DEL ALGORITMO AL INTENTAR OBTENER EL VECTOR DE ESTADO EN LA OTRA FORMA DUAL
        psigmat[i] = (G*sigmat1[i].inverse()*G.transpose() + R).inverse();
        pept[i] = psigmat[i]*G*mut1[i];

        // Se prepara la media estimada para ser enviada al resto de nodos
        pmut[i] = G*mut1[i];	// g = G

        // Se actualiza el estado de los tracks
        trackList[i].state.x = pmut[i](0);
        trackList[i].state.y = pmut[i](1);
        trackList[i].state.z = pmut[i](2);

        // Este nuevo estado es añadido a la ventana de tiempo de estados
        trackPos.x = pmut[i](0);
        trackPos.y = pmut[i](1);
        trackPos.z = pmut[i](2);
        trackList[i].pushStateTimeWindow(trackPos,asociation_window);
    }    
}

// Actualiza la lista de tracks con un modelo de predicción
// Llamar en la fase de predicción del filtro bayesiano
void DistanceAsociationEIF::updateTrackListStatePredictionModel(){
    updateTrackListStatePredictionModel(trackList);
}

// Inicializa las variables para la nueva iteración del EIF
void DistanceAsociationEIF::initilizeEIFiteration(){
    for(unsigned int i = 0; i < trackList.size(); i++){
        sigmat[i] = Eigen::MatrixXd::Zero(ORDER,ORDER);
	    ept[i] = Eigen::MatrixXd::Zero(ORDER,1);
    }    
}

// Actualiza el vector de emparejamientos con la tabla de targets3D
// Todos los targets de la última tabla son añadidos al proceso
void DistanceAsociationEIF::updateTrackListPairingsTimeWindow(){
    // Se borra la lista de targets marcados para ser
    // asociados mediante la ventana de tiempo
    // Se borra vector a vector
    for(int i = 0; i < markedTargets.size(); i++){
        markedTargets[i].clear();
    }
    // Se borra el vector de vectores
    markedTargets.clear();

    // Se prepara la nueva lista de targets marcados para la nueva tabla recibida
    // En este caso todos los targets serán añadidos al proceso de ventana de tiempo
    if(targets3Dtables.size() > 0){
        for(int i = 0; i < targets3Dtables[0].size(); i++){
            if(targets3Dtables[0][i].targets3D.size() > 0){
                std::vector <bool> auxVector;
                auxVector.assign(targets3Dtables[0][i].targets3D.size(),true);
                markedTargets.push_back(auxVector);
            }
        }
    }

    // Se llama a la asociación por ventana de tiempo
    updateTrackListPairingsTimeWindow(markedTargets);
}

// Actualiza el vector de emparejamientos con la tabla de targets3D
// NOTA: ¿QUE OCURRE CON LOS TARGETS NO EMPAREJADOS?
// Debe crearse un track solo para ellos. Esto es así porque es posible que un target sea sólo
// visto por un sólo drone. Este target no es emparejado pero debe tener su propio track
// Los target de gps se emparejan de la misma forma que los target por proyección normales
// Sin embargo, los emparejamientos con targets de gps interaccionan con los tracks de forma distinta
// 1. El target pareja del gps no está asociado: Se crea un track gps con el par (reordenar trackList)
// 2. El target pareja del gps está asociado a un track: Se asocia el target gps al track
//    y se le da la id del target gps al track, convirtiéndolo así a track gps (reordenar trackList)
// 3. El target gps ha quedado solo. Se crea con el un track gps (reordenar tracklist)
void DistanceAsociationEIF::updateTrackListPairingsTimeWindow(std::vector <std::vector <bool>> markedTargets){
    std::vector <std::vector <bool>> aloneTargets; // Lista de targets no emparejados al final de la ventana de tiempo (ni anteriormente)
    aloneTargets = markedTargets;    
    // Se van a calcular las distancias (en la ventana de tiempo) comparando dos a dos entre los targets3D dados
    // por cada drone (en el instante actual)
    if(targets3Dtables.size() > 0){
        if(targets3Dtables[0].size() > 1){ // Debe haber al menos 2 drones en la primera tabla para emparejar
            target_local_id auxLocalTarget1;
            target_local_id auxLocalTarget2;
            double pairdist;
            unsigned int ndist;
            double eudist;
            std::vector <pairingCouple> cPairs; // Vector auxiliar utilizado para almacenar la distancia de los emparejamientos de la última comparación entre la lista de targets de dos drones
            std::vector <pairingCoupleIndex> cPairsIndex; // Guarda los índices de los targets de los emparejamientos propuestos
            for(unsigned int h = 0; h < (targets3Dtables[0].size() - 1); h++){
                // Se compara cada tabla con todas las siguientes
                // De esta forma se comparan todas las tablas con todas
                for(unsigned int k = h+1; k < targets3Dtables[0].size(); k++){
                    cPairs.clear();
                    cPairsIndex.clear();
                    for(unsigned int i = 0; i < targets3Dtables[0][h].targets3D.size(); i++){
                        // Hay que comprobar que el target ha sido marcado para ser asociado por la ventana de tiempo
                        if(markedTargets[h][i]){
                            for(unsigned int j = 0; j < targets3Dtables[0][k].targets3D.size(); j++){
                                // Hay que comprobar que el target ha sido marcado para ser asociado por la ventana de tiempo
                                if(markedTargets[k][j]){
                                    // Se calcula la distancia en esta última tabla (última medida) entre ambos targets
                                    pairdist = euclideanDistance(targets3Dtables[0][h].targets3D[i], targets3Dtables[0][k].targets3D[j]);
                                    ndist = 1;
                                    // DEBUG
                                    //printf("COMPARANDO (%d,%d) con (%d,%d)\n",targets3Dtables[0][h].source_id,targets3Dtables[0][h].targetsID[i],targets3Dtables[0][k].source_id,targets3Dtables[0][k].targetsID[j]);
                                    //std::cout << "pre-pairdist: " << pairdist << " ndist: " << ndist << std::endl;
                                    // DEBUG

                                    // Se suman las distancias de los mismos targets en instante anteriores (siguientes tablas)
                                    // (siempre que dichos targets aparecían en la lista de targets de dichos drones)
                                    // También se controla el número de distancias sumadas para calcular la media
                                    for(int t = 1; t < targets3Dtables.size(); t++){
                                        // Se busca en la tabla t, los targets de los drones comparados justo arriba en la tabla 0
                                        auxLocalTarget1.device_id = targets3Dtables[0][h].source_id;
                                        auxLocalTarget1.target_local_id = targets3Dtables[0][h].targetsID[i];
                                        auxLocalTarget2.device_id = targets3Dtables[0][k].source_id;
                                        auxLocalTarget2.target_local_id = targets3Dtables[0][k].targetsID[j];
                                        if(findTargets3Ddistance(t, auxLocalTarget1, auxLocalTarget2, eudist) != -1){
                                            pairdist += eudist;
                                            ndist++;                                
                                            //std::cout << "pre-pairdist: " << pairdist << " ndist: " << ndist << std::endl;    // DEBUG
                                        }else{
                                            //std::cout << "Comparación imposible en anterior tabla " << t << std::endl;  // DEBUG
                                        }
                                    }

                                    // Se calcula la distancia media media
                                    pairdist /= ndist;
                                    
                                    //std::cout << "--pairdist media: " << pairdist << " ndist: " << ndist << std::endl;    // DEBUG

                                    // Ahora se va a comprobar si se acepta el emparejamiento
                                    // Para ello la media debe ser menor que un umbral
                                    if(pairdist <= asociation_th){
                                        //std::cout << "Nuevo emparejamiento ha superado el umbral" << std::endl; // DEBUG

                                        // Se añade a la lista de emparejamientos propuestos
                                        // sólo si es el mejor emparejamiento propuesto para el target del drone 2
                                        // Explicación: puede ocurrir que se propongan varios emparejamientos para un
                                        // mismo target del drone dos que hayan pasado el umbral. Por ello, si ya 
                                        // existía un emparejamiento propuesto para el target del drone 2 (drone k), se utilizará
                                        // el que tenga menor distancia.
                                        // MEJORA: ¿PUNTUAR POSITIVAMENTE QUE SE HAYAN USADO MÁS MEDIDAS DE LA VENTANA
                                        // DE TIEMPO PARA CALCULAR LA DISTANCIA MEDIA?
                                        pairingCouple auxPair;
                                        auxPair.target1.id.device_id = targets3Dtables[0][h].source_id;
                                        auxPair.target1.id.target_local_id = targets3Dtables[0][h].targetsID[i];
                                        auxPair.target1.state = targets3Dtables[0][h].targets3D[i];
                                        auxPair.target2.id.device_id = targets3Dtables[0][k].source_id;
                                        auxPair.target2.id.target_local_id = targets3Dtables[0][k].targetsID[j];
                                        auxPair.target2.state = targets3Dtables[0][k].targets3D[j];
                                        auxPair.d = pairdist;
                                        pairingCoupleIndex auxPairIndex;                                        
                                        auxPairIndex.target1.device_index = h;
                                        auxPairIndex.target1.target_local_index = i;
                                        auxPairIndex.target2.device_index = k;
                                        auxPairIndex.target2.target_local_index = j;
                                        if(cPairs.empty()){ // Ningún emparejamiento, se añade directamente el emparejamiento
                                            cPairs.push_back(auxPair);
                                            cPairsIndex.push_back(auxPairIndex);
                                        }else{
                                            // Búsqueda de emparejamiento de con mismo id del drone k
                                            // (sólo se pueden repetir los targets del drone recorrido por el bucle interno)
                                            unsigned int ii = 0;
                                            int index = -1;
                                            while(ii < cPairs.size() && index == -1){
                                                if(cPairs[ii].target2.id.target_local_id == targets3Dtables[0][k].targetsID[j]){
                                                    index = ii;
                                                }
                                                ii++;
                                            }
                                            
                                            // Se actua en función de si existía o no otro emparejamuiento similar
                                            // y de si este es mejor o peor
                                            if(index == -1){    // No existe esa id del drone 2 ya emparejada, se añade el nuevo emparejamiento a la lista
                                                cPairs.push_back(auxPair);
                                                cPairsIndex.push_back(auxPairIndex);
                                            }else{  // Si existe
                                                if(pairdist < cPairs[index].d){ // Nuevo emparejamiento con menor distancia
                                                    // Se borra el antiguo y se añade el nuevo
                                                    cPairs.erase(cPairs.begin() + index);
                                                    cPairs.push_back(auxPair);
                                                    cPairsIndex.push_back(auxPairIndex);
                                                }   // Si no, se deja el emparejamiento anteriormente calculado
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // En este punto ha sido construída una lista de de emparejamientos para dos drones
                    // La lista de tracks debe ser actualizada con esta lista
                    // Se espera hasta llegar a este punto para actualizar la lista de tracks y no se hace 
                    // individualmente con cada propuesta de emparejamiento porque es necesario encontrar
                    // un solo emparejamiento para un target, el de menor distancia, por atnto es necesario
                    // recorrer todas las posibilidades y quedarse con la mejor antes de actualizar la lista
                    // de tracks

                    // DEBUG
                    //std::cout << "\nEmparejamientos propuestos entre " << targets3Dtables[0][h].source_id << " y " << targets3Dtables[0][k].source_id << ": " << cPairs.size() << std::endl;
                    //printPairingCoupleVector(cPairs);
                    // DEBUG

                    // Inicialmente es la propia lista de tracks (que puede ser actualizada con un filtro Bayesiano)
                    // la utilizada para continuar con el emparejamiento de targets o para borrar tracks
                    // Sin embargo, sólo el proceso de la ventana de tiempo implementada por esta función peude
                    // crear nuevos tracks. Por otro lado, si un target ha sido marcado para ser emparejado mediante 
                    // la ventana de tiempo, es porque su emparejamiento mediante la lista de tarcks no es de 
                    // confianza. Por tanto, se da por hecho que cuando un target es añadido al proceso de ventana de
                    // tiempo, se ha borrado su aparición en la lista de tracks en ese momento.

                    // Se recorre la lista de emparejamientos propuestos para el par h,k
                    for(unsigned int i = 0; i < cPairs.size(); i++){
                        //printf("\nTratando pair %d\n", i);    // DEBUG
                        addPairingCouple2TrackList(cPairs[i]);

                        // Todos los targets de la lista serán emparejados de alguna forma
                        // Por tanto son desmarcados en la lista auxiliar
                        //printf("\nPuesto a false (%d,%d)\n", cPairsIndex[i].target1.device_index,cPairsIndex[i].target1.target_local_index);    // DEBUG
                        //printf("\nPuesto a false (%d,%d)\n", cPairsIndex[i].target2.device_index,cPairsIndex[i].target2.target_local_index);    // DEBUG
                        aloneTargets[cPairsIndex[i].target1.device_index][cPairsIndex[i].target1.target_local_index] = false;
                        aloneTargets[cPairsIndex[i].target2.device_index][cPairsIndex[i].target2.target_local_index] = false;                        
                    }
                }
            }
        }

        // Independientemente del número de devices (siempre que haya al menos uno)
        // se deben crear tracks para los targets que hayan quedado sin emparejar
        //drawMarkedTargets(aloneTargets);    // DEBUG
        if(targets3Dtables[0].size() > 0){
            target_local targ;
            target_local_id targ_id;
            int aux_index;
            for(unsigned int i = 0; i < aloneTargets.size(); i++){  // Se recorre cada device
                // Según el tipo de target se creará un tipo de track
                if(targets3Dtables[0][i].source_id == GPS_VIRTUAL_DEVICE_ID){   // targets tipo gps
                    for(unsigned int j = 0; j < aloneTargets[i].size(); j++){   // cada target de cada device
                        // Si el target sigue marcado es que no ha sido emparejado de ninguna forma
                        if(aloneTargets[i][j]){
                            //printf("\n\nAñadiendo NUEVO track GPS (con un solo target no emparejado)\n");    // DEBUG
                            addGPSTrack(targets3Dtables[0][i].targetsID[j],targets3Dtables[0][i].targets3D[j]);
                        }
                    }
                }else{  // targets visuales
                    for(unsigned int j = 0; j < aloneTargets[i].size(); j++){   // cada target de cada device
                        // Si el target sigue marcado es que no ha sido emparejado de ninguna forma
                        if(aloneTargets[i][j]){
                            targ_id.device_id = targets3Dtables[0][i].source_id;                        
                            targ_id.target_local_id = targets3Dtables[0][i].targetsID[j];
                            // Se añade un nuevo track con dicho target
                            targ.id = targ_id;
                            targ.state = targets3Dtables[0][i].targets3D[j];
                            //printf("\n\nAñadiendo NUEVO track visual (con un solo target no emparejado)\n");    // DEBUG
                            addVisualTrack(targ);

                            /*** NOTA: Los targets que llegan aquí (ventana de tiempo por proyección)
                                    han sido borrados de la track list y por tanto este código de abajo
                                    (que comprueba si el target estaba anteriormente emparejado) es inútil
                                    Crear directamente el nuevo track (código de arriba no comentado) ***/
                            // Si ya estaba emparejado no se toca el emparejamiento
                            /*targ_id.device_id = targets3Dtables[0][i].source_id;
                            targ_id.target_local_id = targets3Dtables[0][i].targetsID[j];
                            aux_index = findTargetLocalTrackList(trackList, targ_id);
                            if(aux_index == -1){    // Target no emparejado con ningún track anteriormente
                                // Se añade un nuevo track con dicho target
                                targ.id = targ_id;
                                targ.state = targets3Dtables[0][i].targets3D[j];
                                addVisualTrack(targ);
                            }else{  // Target si emparejado a un track anteriormente
                                // Se refresca el tiempo sin emparejameinto del track (el track en sí se deja como está, con dicho target emparejado a él)
                                trackList[aux_index].time_since_last_pairing = 0;
                            }*/
                            /*******/                        
                        }
                    }
                }
            }
        }
    }
}

// Añade un emparejamiento propuesto a la lista de tracks
// Se da por hecho que el target no ha sido asociado de forma confiable
// por los track predichos por el EIF y por tanto los targets del emparejamiento
// no aparecen en la lista de Tracks, y por tanto se debe crear un track. La única 
// excepción es que en esta misma iteración el target haya sido emparejado en otra 
// comparación de targets de drones y entonces ya si haya un track con alguno de los 
// targets a añadir del emparejamiento. En este caso se añade el target que falta al
// track, no se crea uno nuevo
int DistanceAsociationEIF::addPairingCouple2TrackList(pairingCouple pair){
    // Se recorren todos los targets de todos los tracks de la track list para ambos targets locales
    // 1) Si no se encuentra ningun track (los índices quedan en -1) se crea un nuevo track
    // 2) Si uno de los target ya se encuentra en un track, el otro target se añade a ese track
    // 3) Si ambos target ya están asignados a un track, se descarta este emparejamiento

    // El funcionamiento es distinto si una las medidas provienen del device virtual de medidas GPS
    if(pair.target1.id.device_id == GPS_VIRTUAL_DEVICE_ID){  // target 1 tipo gps
        addGPStargetPair2TrackList(pair.target1, pair.target2);
    }else if(pair.target2.id.device_id == GPS_VIRTUAL_DEVICE_ID){   // target 2 tipo gps
        addGPStargetPair2TrackList(pair.target2, pair.target1);
    }else{  // ambos targets tipo visual
        int target1_track_index = findTargetLocalTrackList(trackList, pair.target1.id);
        int target2_track_index = findTargetLocalTrackList(trackList, pair.target2.id);

        if(target1_track_index == -1 && target2_track_index == -1){ // Targets no asignados, crear nuevo track
            //printf("\n\nAñadiendo NUEVO track visual\n");    // DEBUG
            addVisualTrack(pair);
        }else if(target1_track_index == -1 && target2_track_index > -1){    // Sólo target 2 asignado, se añade el target 1 al track dónde está el target 2
            trackList[target2_track_index].pairings.targets.push_back(pair.target1.id);
            // Se refresca el tiempo sin emparejamiento del track
            trackList[target2_track_index].time_since_last_pairing = 0;
            //printf("\nAñadido target a track\n");    // DEBUG
        }else if(target1_track_index > -1 && target2_track_index == -1){    // Sólo target 1 asignado, se añade el target 2 al track dónde está el target 1
            trackList[target1_track_index].pairings.targets.push_back(pair.target2.id);
            // Se refresca el tiempo sin emparejamiento del track
            trackList[target2_track_index].time_since_last_pairing = 0;
            //printf("\nAñadido target a track\n");    // DEBUG
        }
    }
}

// Añade dos targets, el primero de ellos tipo track gps, a la trackList
// 0. Durante el propio proceso de asociación ya se ha creado un track con el gps pasado como argumento.
//    Se debe comprobar esto. En caso afirmativo se añade el target visual al track del gps
// 1. El target pareja del gps ni el target gps no están asociados: Se crea un track gps con el par (reordenar trackList)
// 2. El target pareja del gps está asociado a un track: Se asocia el target gps al track
//    y se le da la id del target gps al track, convirtiéndolo así a track gps (reordenar trackList)
void DistanceAsociationEIF::addGPStargetPair2TrackList(target_local GPStarget, target_local visualTarget){
    // Se comprueba si el target visual ya estaba asociado
    int visualTarget_track_index = findTargetLocalTrackList(trackList, visualTarget.id);
    int GPStarget_track_index = findGPSTrack(GPStarget.id.target_local_id);

    if(GPStarget_track_index == -1 && visualTarget_track_index == -1){  // target visual ni GPS no asignados, crear nuevo track
        //printf("\n\nAñadiendo NUEVO track GPS\n");    // DEBUG
        // Se usa la id local del target gps para la id común del nuevo track
        // Se usa la posición del target gps para inicializar el nuevo track GPS
        addGPSTrack(GPStarget.id.device_id,GPStarget.state,visualTarget.id);
    }else if(GPStarget_track_index > -1 && visualTarget_track_index == -1){ // target gps asignado, se añade el target visual al track del gps
        trackList[GPStarget_track_index].pairings.targets.push_back(visualTarget.id);
        // Se refresca el tiempo sin emparejamiento del track
        trackList[GPStarget_track_index].time_since_last_pairing = 0;
    }else if(GPStarget_track_index == -1 && visualTarget_track_index > -1){    // target visual asignado, se añade el target gps al track dónde está el target visual
        // Y se cambia el id del track al del target gps, convirtiéndolo en track gps
        trackList[visualTarget_track_index].pairings.targets.push_back(GPStarget.id);
        // Se refresca el tiempo sin emparejamiento del track
        trackList[visualTarget_track_index].time_since_last_pairing = 0;
        //printf("\nAñadido target a track\n");    // DEBUG
        // Se cambia la id del track y se reordena la trackList
        changeTrackID(visualTarget_track_index, GPStarget.id.target_local_id);
    }
}

// Busca un target en la trackList, en caso de encontrarlo devuelve el índice 
// del track dónde se encuentra. En caso de no encontrarlo devuelve -1
int DistanceAsociationEIF::findTargetLocalTrackList(std::vector <Track> &tl, target_local_id target){
    int index = -1;
    int i = 0;
    while(i < tl.size() && index == -1){
        if(findTargetLocalPairingList(tl[i].pairings, target) != -1){
            index = i;
        }
        i++;
    }

    return index;
}

// Busca un target en una lista de emparejamientos, en caso de encontrarlo devuelve el índice 
// del target en la lista. En caso de no encontrarlo devuelve -1
int DistanceAsociationEIF::findTargetLocalPairingList(pairing p, target_local_id target){
    int index = -1;
    int j = 0;
    while(j < p.targets.size() && index == -1){
        if(p.targets[j].device_id == target.device_id && p.targets[j].target_local_id == target.target_local_id){
            index = j;                
        }
        j++;
    }

    return index;
}

void DistanceAsociationEIF::manageGPSdata(multidrone_msgs::TargetStateArray gpsdata){
    // Save the gps info into a EIFasoc_P2P msg
    // Se van a recorrer todos los targets enviados (medidas de GPS)
    // y se va a guardar esta información en un mensaje tipo EIFasoc_P2P
    // si ya había una medida de un GPS, se actualiza con la nueva medida
    // Así se garantiza que cuando el algoritmo EIF ejecute su siguiente iteración
    // este utilice la medida recibida más reciente de cada GPS
    // Por tanto, el algoritmo EIF durante su iteración tendrá que hacer un push de este 
    // device "virtual" a su tabla más reciente, cómo si se tratara de medidas visuales de un drone
    // y tras acabar su uso, debe ser borrada, para dar paso a las nuevas medidas de GPS
    // El push a la tabla de targets tiene como objetivo asociar medidas de targets visuales 
    // a tracks de GPS.
    global_tracker::Contribution contri;
    int target_index;
    for(unsigned int i = 0; i < gpsdata.targets.size(); i++){
        // Se comprueba si dicho gps ya ha sido añadido        
        target_index = findIndexUint16(gpsdata.targets[i].target_id, lastGPSdata.targetsID);
        if(target_index == -1){ // No hay datos de gps con esa id
            // Se añade un nuevo target con la id del gps
            lastGPSdata.targetsID.push_back(gpsdata.targets[i].target_id);  // Se añade la id
            lastGPSdata.targets3D.push_back(gpsdata.targets[i].pose.pose.position); // Se añade el target(gps)

            // también se guarda la covarianza de la medida del gps
            for(unsigned int j = 0; j < ORDER*ORDER; j++){
                contri.covariance[j] = gpsdata.targets[i].pose.covariance[j];
            }
            lastGPSdata.contrib.push_back(contri);
        }else{  // El gps ya fue añadido anteriormente, se sobrescribe la medida con la nueva recibida
            lastGPSdata.targets3D[target_index] = gpsdata.targets[i].pose.pose.position;

            // también se sobreescribe la covarianza
            for(unsigned int j = 0; j < ORDER*ORDER; j++){
                lastGPSdata.contrib[target_index].covariance[j] = gpsdata.targets[i].pose.covariance[j];
            }
        }
    }
}

// Borrar las medidas GPS guardadas para que no sean añadidas e integradas en el filtro
// en la proxima iteración
void DistanceAsociationEIF::clearGPSdataVector(){
    lastGPSdata.targets3D.clear();
    lastGPSdata.targetsID.clear();
    lastGPSdata.contrib.clear();    
}

// Añade un track a la lista de tracks con los emparejamientos pasados como argumento con el estado pasado como argumento
void DistanceAsociationEIF::addTrack(pairing pairings, geometry_msgs::Point trackPos){
    trackList.push_back({pairings,trackPos,0,0});

    // Inicialización de la información EIF del track
    Eigen::VectorXd initialState(ORDER,1);	// Estado inicial
    initialState << trackPos.x, trackPos.y, trackPos.z, 0, 0, 0;
    Eigen::MatrixXd ept0(ORDER,1);
    ept0 = sigmat1_initial*initialState;

    sigmat1.push_back(sigmat1_initial);
    psigmat.push_back(Eigen::MatrixXd::Zero(ORDER,ORDER));
    sigmat.push_back(sigmat1_initial);

    ept1.push_back(ept0);
    pept.push_back(Eigen::MatrixXd::Zero(ORDER,1));
    ept.push_back(ept0);

    mut1.push_back(Eigen::MatrixXd::Zero(ORDER,1));
    pmut.push_back(initialState); // Muy importante inicializar la media predicha para el cálculo de las contribuciones
    covariance1.push_back(Eigen::MatrixXd::Zero(ORDER,ORDER));
}

// Añade un track a la lista de tracks con un par emparejados
// Se devuelve el id común asignado a los emparejamientos del track
// Se elige el primer id libre en la lista
// El track se crea con un primer pairingCouple
// Se supone que no existe ningún otro track con el par de targets añadidos
int DistanceAsociationEIF::addVisualTrack(pairingCouple pair){
    // Se prepara el estado que será guardado en el track como inicialización, este será la posición media
    // de ambos target locales (en teoría serán posiciones muy parecidas porque son el mismo track)
    geometry_msgs::Point trackPos;
    trackPos.x = (pair.target1.state.x + pair.target2.state.x)/2;
    trackPos.y = (pair.target1.state.y + pair.target2.state.y)/2;
    trackPos.z = (pair.target1.state.z + pair.target2.state.z)/2;
    //printf("\nState: %f, %f, %f\n", trackPos.x, trackPos.y, trackPos.z);  // DEBUG

    // Inicilización del track
    pairing pairings;
    pairings.common_id = nextFreeIDVisualTrackList();
    pairings.targets.push_back(pair.target1.id);
    pairings.targets.push_back(pair.target2.id);

    // Se añade el track
    addTrack(pairings, trackPos);

    // Se reordena la trackList por ids (probablemente ha sido desornada al añadir el último track)
    sortTrackList();

    return pairings.common_id;
}

// Añade un track a la lista de tracks con un solo target
// Se devuelve el id común asignado al track
// Se elige el primer id libre en la lista
// El track se crea con un primer target_local
// Se supone que no existe ningún otro track con el target añadido
int DistanceAsociationEIF::addVisualTrack(target_local t){
    // Se prepara el estado que será guardado en el track como inicialización
    //printf("\nState: %f, %f, %f\n", t.state.x, t.state.y, t.state.z);   // DEBUG

    // Inicialización del track
    pairing pairings;
    pairings.common_id = nextFreeIDVisualTrackList();
    pairings.targets.push_back(t.id);

    // Se añade el track
    addTrack(pairings, t.state);

    // Se reordena la trackList por ids (probablemente ha sido desornada al añadir el último track)
    sortTrackList();

    return pairings.common_id;
}

// Añade un track usando como id común la del propio GPS
// Se supone que cada GPS tiene una id distinta y por tanto no existirán dos tracks
// con el mismo id
// Reordenará la lista al añadir el nuevo track
void DistanceAsociationEIF::addGPSTrack(int common_id, geometry_msgs::Point pos){
    if(common_id >= visual_tracks_common_id_offset){
        printf("\n\nCARE, new track ID exceeds the maximum number of gps allowed");
    }

    // Se prepara el estado que será guardado en el track como inicialización
    //printf("\nState: %f, %f, %f\n", pos.x, pos.y, pos.z);

    // Inicialización del track
    target_local_id t;
    t.target_local_id = common_id;
    t.device_id = GPS_VIRTUAL_DEVICE_ID;

    pairing pairings;
    pairings.common_id = common_id;
    pairings.targets.push_back(t);        

    // Se añade el track
    addTrack(pairings, pos);

    // Se reordena la trackList por ids (probablemente ha sido desornada al añadir el último track)
    sortTrackList();
}

// Esta versión de la función permite añadir también un target visual en los emparejamientos del nuevo track
void DistanceAsociationEIF::addGPSTrack(int common_id, geometry_msgs::Point pos, target_local_id visual_target_id){
    if(common_id >= visual_tracks_common_id_offset){
        printf("\n\nCARE, new track ID exceeds the maximum number of gps allowed");
    }

    // Se prepara el estado que será guardado en el track como inicialización
    //printf("\nState: %f, %f, %f\n", pos.x, pos.y, pos.z);   // DEBUG

    // Inicialización del track
    target_local_id t;
    t.target_local_id = common_id;
    t.device_id = GPS_VIRTUAL_DEVICE_ID;

    pairing pairings;
    pairings.common_id = common_id;
    pairings.targets.push_back(t);
    pairings.targets.push_back(visual_target_id);        

    // Se añade el track
    addTrack(pairings, pos);

    // Se reordena la trackList por ids (probablemente ha sido desornada al añadir el último track)
    sortTrackList();
}

// Borra un track de la track list dado su índice
// También borrar los correspondientes elementos en la listas de datos del EIF
// Devuelve -1 si no se ha podido borrar el track o 0 en caso de éxito
int DistanceAsociationEIF::eraseTrack(unsigned int index){
    if(index < trackList.size()){
        trackList.erase(trackList.begin() + index);

        // Erase also EIF data
        sigmat1.erase(sigmat1.begin() + index);
        psigmat.erase(psigmat.begin() + index);
        sigmat.erase(sigmat.begin() + index);

        ept1.erase(ept1.begin() + index);
        pept.erase(pept.begin() + index);
        ept.erase(ept.begin() + index);

        mut1.erase(mut1.begin() + index);
        pmut.erase(pmut.begin() + index);
        covariance1.erase(covariance1.begin() + index);
    }
}

// Busca en la lista de tracks si existe un target de un drone en un emparejamiento
// en caso afirmativo borra el emparejamiento, en otro caso no hace nada. 
// Devuelve el número de targets borrados
int DistanceAsociationEIF::eraseTargetFromTrackVector(std::vector <Track> &tl, target_local_id target){
    std::vector <unsigned int> indexList;
    unsigned int erasedTargets = 0;
    // Se recorre la lista de targets buscando el target por borrar
    // Si un track se ha quedado sin emparamientos se añade a la lista de tracks por borrar
    for(int i = 0; i < tl.size(); i++){
        erasedTargets += eraseTargetFromPairing(tl[i].pairings, target);
        if(tl[i].pairings.targets.size() == 0){
            indexList.push_back(i);
        }
    }

    // Se borran los track que han quedado vacíos
    for(int i = 0; i < indexList.size(); i++){
        eraseTrack(indexList[i] - i);
    }

    return erasedTargets;
}

// Busca en una lista de emparejamientos si existe un target de un drone en un emparejamiento
// en caso afirmativo devuelve el número de targets borrados y borra el emparejamiento, 
// en otro caso no hace nada y devuelve -1
int DistanceAsociationEIF::eraseTargetFromPairing(pairing &p, target_local_id target){
    // Se va a recorrer la lista de emparejamientos buscando los índices de 
    // los emparejamientos que contengan al target pasado como argumento
    std::vector <unsigned int> indexList;
    for(int i = 0; i < p.targets.size(); i++){
        if(p.targets[i].device_id == target.device_id && p.targets[i].target_local_id == target.target_local_id){
            indexList.push_back(i);
        }
    }

    // Se borran todos los target encontrados
    if(indexList.size() > 0){
        for(int i = 0; i < indexList.size(); i++){
            p.targets.erase(p.targets.begin() + indexList[i] - i);  // Cada vez que se borra un elemento los indices bajan en una unidas
        }
    }

    return indexList.size();
}

// Busca en un la lista de tracks el siguiente ID libre
int DistanceAsociationEIF::nextFreeIDTrackList(){
    unsigned int i = 0; // Primera ID válida de tracks puramente visuales = visual_tracks_common_id_offset
    int nextFreeID = -1;
    bool freeIDfinded;
    while(nextFreeID == -1){
        freeIDfinded = true;
        for(int j = 0; j < trackList.size(); j++){
            if(trackList[j].pairings.common_id == i){
                freeIDfinded = false;   // id ya en uso
            }
        }
        if(freeIDfinded == true){
            nextFreeID = i;
        }
        i++;
    }
    
    return nextFreeID;
}

// Busca en un la lista de tracks el siguiente ID libre de los tracks visuales
int DistanceAsociationEIF::nextFreeIDVisualTrackList(){
    unsigned int i = visual_tracks_common_id_offset; // Primera ID válida de tracks puramente visuales = visual_tracks_common_id_offset
    int nextFreeID = -1;
    bool freeIDfinded;
    while(nextFreeID == -1){
        freeIDfinded = true;
        for(int j = 0; j < trackList.size(); j++){
            if(trackList[j].pairings.common_id == i){
                freeIDfinded = false;   // id ya en uso
            }
        }
        if(freeIDfinded == true){
            nextFreeID = i;
        }
        i++;
    }
    
    return nextFreeID;
}

// Calcula la distancia entre dos targets3D de dos drones distintos en una de las tablas guardadas
// de la lista de tablas de targets3D
// Deben existir al menos dos drones en la tabla
// Devuelve 0 si se han encontrado los targtes3D indicados, en caso contrario devuelve -1
int DistanceAsociationEIF::findTargets3Ddistance(unsigned int table_index, target_local_id target1, target_local_id target2, double &d){
    // Deben existir al menos dos drones en la tabla (y la tabla debe existir)
    if(table_index >= targets3Dtables.size() || table_index < 0 || targets3Dtables[table_index].size() <= 1){   // La tabla indicada debe existir
        //printf("NO EXISTE LA TABLA O HAY MENOS DE DOS DRONES\n"); // DEBUG
        return -1;  // La tabla indicada no existe        
    }else{  // La tabla existe
        // Se deben encontrar (si existen) los índices en la tabla de los drones con drone1_id y drone2_id        
        int index_drone1 = findDeviceTableIndex(table_index,target1.device_id);
        int index_drone2 = findDeviceTableIndex(table_index,target2.device_id);

        // En caso de no existir, se devuelve -1
        // en caso de haber sido encontrados los índices, se continúa con la búsqueda
        if(index_drone1 == -1 || index_drone2 == -1){
            //printf("INDEX DE DRONE %d o %d no encontrada, index: %d, %d\n", drone1_id, drone2_id,index_drone1,index_drone2); // DEBUG
            return -1;            
        }else{
            //printf("drone1_id %d -> index %d || drone2_id %d -> index %d\n", drone1_id,index_drone1,drone2_id,index_drone2); // DEBUG
            // Se buscan los índices de ambos targets en la lista de los drones
            // En caso de no existir, se devuelve menos 1
            // Si ambos valores son encontrados, se calcula la distancia entre dichos targets y se devuelve 0
            int index_target1 = findIndexUint16(target1.target_local_id, targets3Dtables[table_index][index_drone1].targetsID);
            int index_target2 = findIndexUint16(target2.target_local_id, targets3Dtables[table_index][index_drone2].targetsID);
            if(index_target1 == -1 || index_target2 == -1){
                return -1;
            }else{
                d = euclideanDistance(targets3Dtables[table_index][index_drone1].targets3D[index_target1], targets3Dtables[table_index][index_drone2].targets3D[index_target2]);
                return 0;
            }
        }        
    }
}

// Calcula la distancia entre un target y un track en un instante tiempo t
// Si no hay estimación de ese track en ese instante o medida de dicho target en ese instante se devuelve -1
// Devuelve 0 si el track y el arget han sido encontrados, y por tanto la distancia calculada
int DistanceAsociationEIF::findTrackTargets3Ddistance(unsigned int table_index, unsigned int track_index, target_local_id target, double &d){
    // Primero se comprueba si existe existe la tabla
    if(table_index >= targets3Dtables.size() || table_index < 0){        
        return -1;  // La tabla no existe se sale y devuelve -1
    }else{  // La tabla existe
        // Se comprueba si existe el track indicado
        if(track_index >= trackList.size() || track_index < 0){ // El track no existe
            return -1;
        }else{  // EL track existe
            // Se comprueba que el track tiene una estimación para el instante de tiempo atrás track_index
            // en su ventana de tiempo de estados
            // (no se comprueba si es mayor que 0 porque ya fue comprobado arriba en el primer if)
            if(table_index >= trackList[track_index].stateTW.size()){    // No hay estimación
                return -1;
            }else{  // La estimación existe
                // Se busca el target del drone indicado en la tabla del instante anterior table_index
                int index_drone = findDeviceTableIndex(table_index,target.device_id);
                // En caso de no existir, se devuelve -1
                // en caso de haber sido encontrados el índice, se continúa con la búsqueda
                if(index_drone == -1){
                    return -1;
                }else{
                    // Se busca el índice del target en la lista del drone
                    // En caso de no existir, se devuelve menos 1
                    // Si el valor es encontrado se calcula la distancia entre dicho targets y la estimación correspondiente del track
                    int index_target = findIndexUint16(target.target_local_id, targets3Dtables[table_index][index_drone].targetsID);
                    if(index_target == -1){ // target no encontrado
                        return -1;
                    }else{  // target encontrado
                        d = euclideanDistance(targets3Dtables[table_index][index_drone].targets3D[index_target], trackList[track_index].stateTW[table_index]);
                        return 0;
                    }
                }
            }
        }
        
    }
}

// Actualiza los emparejamientos llamando los dos métodos
// de asociación en el orden correcto
// RECORDAR: llamar a updateTrackListStatePredictionModel antes de llamar esta función
void DistanceAsociationEIF::updateTrackListPairings(){
    // Se borra la lista de targets marcados para ser
    // asociados mediante la ventana de tiempo
    // Se borra vector a vector
    if(markedTargets.size() > 0){
        for(int i = 0; i < markedTargets.size(); i++){
            markedTargets[i].clear();
        }
        // Se borra el vector de vectores
        markedTargets.clear();
    }
    // Se prepara la nueva lista de targets marcados para la nueva tabla recibida
    // Por defecto los targets se envian a la asociación por ventana de tiempo
    for(int i = 0; i < targets3Dtables[0].size(); i++){
        std::vector <bool> auxVector;
        if(targets3Dtables[0][i].targets3D.size() > 0){   // Sólo se crea un vector de marcados si existen targets en dicho device            
            auxVector.assign(targets3Dtables[0][i].targets3D.size(),true);
            
        }// En caso de no contener targets se añade un vector vacío
        markedTargets.push_back(auxVector); // Push del vector (ya sea vacío con elementos)
    }

    // Se asocia mendiante la cercanía del estado de los tracks
    updateTrackListPairingsByState(trackList,markedTargets);

    // DEBUG
    //drawMarkedTargets(markedTargets);
    // DEBUG

    // Los targets marcados se asocian mediante la ventana de tiempo
    updateTrackListPairingsTimeWindow(markedTargets);
}

void DistanceAsociationEIF::initialize(){
    asociation_window = TIME_WINDOW_LENGHT_DEFAULT;
    asociation_th = ASOCIATION_TH_DEFAULT;
    track_asociation_th = TRACK_ASOCIATION_TH_DEFAULT;
    max_time_without_pairing_refresh = MAX_TIME_WITHOUT_PAIRING_REFRESH;
    visual_tracks_common_id_offset = VISUAL_TRACKS_COMMON_ID_OFFSET_DEFAULT;
    lastGPSdata.source_id = GPS_VIRTUAL_DEVICE_ID;
    G = Eigen::MatrixXd::Identity(ORDER,ORDER);
    R = Eigen::MatrixXd::Identity(ORDER,ORDER);
    Qvisual = Eigen::MatrixXd::Identity(2,2);
    Qgps = Eigen::MatrixXd::Identity(3,3);
    Hgps = Eigen::MatrixXd::Zero(3,6);
    // GPS observation model (and Jacobian)
    Hgps << 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
}

// Setea los valores iniciales del filtro EIF que se la dará al crear el track
void DistanceAsociationEIF::setInitialState(Eigen::MatrixXd sigmat1_m, Eigen::MatrixXd ept1_m){
    sigmat1_initial = sigmat1_m;
}

// Busca (si existe) un target con la id pasada como argumento en la lista de tracks
// En caso aformativo devuelve el índice del track en la lista
int DistanceAsociationEIF::findTrack(unsigned int track_id){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && index == -1){
        if(trackList[i].pairings.common_id == track_id){
            index = i;
        }
        i++;
    }
    return index;
}

// Busca (si existe) un track de GPS con la id pasada como argumento en la lista de tracks
// En caso afirmativo devuelve el índice del track en la lista
int DistanceAsociationEIF::findGPSTrack(unsigned int track_id){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && trackList[i].pairings.common_id < visual_tracks_common_id_offset && index == -1){
        if(trackList[i].pairings.common_id == track_id){
            index = i;
        }
        i++;
    }
    return index;
}

// Busca (si existe) el índice del vector de un drone en la tabla de targets3D dada su id (la id del drone)
// Devuelve -1 si no encuentra el drone, o el índice en caso de encontrarlo
int DistanceAsociationEIF::findDeviceTableIndex(unsigned int table_index, unsigned int drone_id){
    if(table_index >= targets3Dtables.size() || table_index < 0){   // La tabla indicada debe existir
        //printf("NO EXISTE LA TABLA (findDeviceTableIndex)\n"); // DEBUG
        return -1;  // La tabla indicada no existe
    }else{  // La tabla existe
        unsigned int i = 0;
        int index = -1;
        while(i < targets3Dtables[table_index].size() && index == -1){
            if(targets3Dtables[table_index][i].source_id == drone_id){
                index = i;
            }
            i++;
        }
        return index;
    }
}

// Busca un valor en un vector de enteros de 16 bits sin signo (vectores de IDs)
// Devuelve -1 si no encuentra el valor
int DistanceAsociationEIF::findIndexUint16(unsigned short int d, std::vector<unsigned short int> v){
    unsigned int i = 0;
    int index = -1;
    while(i < v.size() && index == -1){
        if(v[i] == d){
            index = i;
        }
        i++;
    }
    return index;
}

// Busca el índice del primer track visual en la lista de tracks
// En caso de no haber tracks visuales se devuelve -1
int DistanceAsociationEIF::findFirstVisualTrackIndex(){
    unsigned int i = 0;
    int index = -1;
    while(i < trackList.size() && index == -1){
        if(trackList[i].pairings.common_id >= visual_tracks_common_id_offset){
            index = i;
        }
        i++;
    }
    return index;
}

int DistanceAsociationEIF::GPSTracksCount(){
    int index = findFirstVisualTrackIndex();
    if(index == -1){
        return trackList.size();
    }else{
        return index;
    }
}

// Calcula la distancia Euclídea entre dos puntos
double DistanceAsociationEIF::euclideanDistance(geometry_msgs::Point p1, geometry_msgs::Point p2){
    return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z));
}

// Calcula la contribución dadas las medidas visuales de un target
void DistanceAsociationEIF::calculateVisualContribution(const Eigen::Ref<const Eigen::MatrixXd>& z,
const Eigen::Ref<const Eigen::Matrix4d>& T, const Eigen::Ref<const Eigen::MatrixXd>& pmut,
const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib,
Eigen::Ref<Eigen::MatrixXd> omegat_contrib){
	// Se actualiza la H y h con la nueva media estimada (Se usa la última posición leída del drone)
	double Hden, Hnum1, Hnum2;
	Hden = T(2,0)*pmut(0,0) + T(2,1)*pmut(1,0) + T(2,2)*pmut(2,0) + T(2,3);
	Hnum1 = T(0,0)*pmut(0,0) + T(0,1)*pmut(1,0) + T(0,2)*pmut(2,0) + T(0,3);
	Hnum2 = T(1,0)*pmut(0,0) + T(1,1)*pmut(1,0) + T(1,2)*pmut(2,0) + T(1,3);

	Eigen::MatrixXd h(2,1);
	h(0,0) = Hnum1/Hden;
	h(1,0) = Hnum2/Hden;

	Eigen::MatrixXd H = Eigen::MatrixXd::Zero(2,ORDER);	// Se pretende inicializar a 0

	H(0,0) = (T(0,0)*Hden - Hnum1*T(2,0))/(Hden*Hden);
	H(1,0) = (T(1,0)*Hden - Hnum2*T(2,0))/(Hden*Hden);

	H(0,1) = (T(0,1)*Hden - Hnum1*T(2,1))/(Hden*Hden);
	H(1,1) = (T(1,1)*Hden - Hnum2*T(2,1))/(Hden*Hden);

	H(0,2) = (T(0,2)*Hden - Hnum1*T(2,2))/(Hden*Hden);
	H(1,2) = (T(1,2)*Hden - Hnum2*T(2,2))/(Hden*Hden);
	// El resto de elementos de H son 0

	/*Eigen::MatrixXd zh = Eigen::MatrixXd::Zero(2,ORDER);
	zh = z - h;
	printf("\n z-h: %.10f, %.10f\n", zh(0), zh(1));*/
	// Se calcula la contribución a la actualización
	omegat_contrib = H.transpose()*Q.inverse()*H;
	ept_contrib = H.transpose()*Q.inverse()*(z - h + H*pmut);
}

// Calcula la contribución dada por una medida
void DistanceAsociationEIF::calculateContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, 
const Eigen::Ref<const Eigen::MatrixXd>& h, const Eigen::Ref<const Eigen::MatrixXd>& H, 
const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, 
Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib){
	omegat_contrib = H.transpose()*Q.inverse()*H;
	ept_contrib = H.transpose()*Q.inverse()*(z - h + H*pmut);
}

// Devuelve el número de elementos de la trackList
int DistanceAsociationEIF::getTrackListSize(){
    return trackList.size();
}

// Devuelve un track de la lista de tracks
Track DistanceAsociationEIF::getTrack(unsigned int index){
    return trackList[index];
}

// Fija el modelo de predicción
void DistanceAsociationEIF::setPredictionModel(Eigen::MatrixXd m){
    G = m;
}

// Fija el ruido del modelo de predicción
void DistanceAsociationEIF::setPredictionModelTrust(Eigen::MatrixXd m){
    R = m;
}

// Fija el ruido de las medidas
void DistanceAsociationEIF::setVisualMeasureTrust(Eigen::MatrixXd m){
    Qvisual = m;
}

// Fija el ruido de las medidas
void DistanceAsociationEIF::setGPSMeasureTrust(Eigen::MatrixXd m){
    Qgps = m;
}

// Pinta un vector de tracks (trackList)
void DistanceAsociationEIF::print_trackList(){
    printf("\n\nTrackList (%u tracks)\n", (unsigned int)trackList.size());
    for(unsigned int i = 0; i < trackList.size(); i++){
        printf("\n___TRACK %d___ (index: %d)",trackList[i].pairings.common_id,i);
        printf("\n   State -> %.3f,%.3f,%.3f",trackList[i].state.x, trackList[i].state.y, trackList[i].state.z);
        printf("\n   Time Window: ");
        for(unsigned int j = 0; j < trackList[i].stateTW.size(); j++){
            printf(" (%.3f,%.3f,%.3f) -", trackList[i].stateTW[j].x,trackList[i].stateTW[j].y,trackList[i].stateTW[j].z);
        }
        printf("\n   Life time -> %u",trackList[i].life_time);
        printf("\n   Time_since_last_pairing time -> %u",trackList[i].time_since_last_pairing);
        printf("\n   Pairings (device,local_id):");
        for(unsigned int j = 0; j < trackList[i].pairings.targets.size(); j++){
            printf("\n   %d,%d",trackList[i].pairings.targets[j].device_id,trackList[i].pairings.targets[j].target_local_id);
        }
    }
    printf("\n");
}

// Pinta una targets3Dtable
void DistanceAsociationEIF::draw_targets3Dtable(unsigned int index){
    if(index < targets3Dtables.size()){
        //printf("\n");
        for(int i = 0; i < targets3Dtables[index].size(); i++){            
            std::cout << "Array procedente de " << targets3Dtables[index][i].source_id << std::endl;        
            if(targets3Dtables[index][i].targets3D.size() > 0){
                for(int j = 0; j < targets3Dtables[index][i].targets3D.size(); j++){
                    std::cout << "target_" << targets3Dtables[index][i].targetsID[j] << ": ";
                    printf("%.3f, %.3f, %.3f\n", targets3Dtables[index][i].targets3D[j].x, targets3Dtables[index][i].targets3D[j].y, targets3Dtables[index][i].targets3D[j].z);
                }
            }
        }
    }
}
// Pinta todas las targets3Dtable
void DistanceAsociationEIF::draw_targets3Dtables(){
    if(targets3Dtables.size() > 0){
        for(int i = 0; i < targets3Dtables.size(); i++){
            printf("\nTabla %d\n", i);
            draw_targets3Dtable(i);
        }
    }
}

void DistanceAsociationEIF::set_asociation_window(int value){
    asociation_window = value;
}
int DistanceAsociationEIF::get_asociation_window(){
    return asociation_window;
}

void DistanceAsociationEIF::set_asociation_th(double value){
    asociation_th = value;
}
double DistanceAsociationEIF::get_asociation_th(){
    return asociation_th;
}

void DistanceAsociationEIF::set_track_asociation_th(double value){
    track_asociation_th = value;
}
double DistanceAsociationEIF::get_track_asociation_th(){
    return track_asociation_th;
}

void DistanceAsociationEIF::set_max_track_time_without_pairing_refresh(unsigned int value){
    max_time_without_pairing_refresh = value;
}
unsigned int DistanceAsociationEIF::get_max_track_time_without_pairing_refresh(){
    return max_time_without_pairing_refresh;
}

void DistanceAsociationEIF::set_visual_tracks_common_id_offset(unsigned int value){
    visual_tracks_common_id_offset = value;
}
unsigned int DistanceAsociationEIF::get_visual_tracks_common_id_offset(){
    return visual_tracks_common_id_offset;
}

void DistanceAsociationEIF::printPairingCoupleVector(std::vector <pairingCouple> v){
    if(v.size() > 0){
        for(int i = 0; i < v.size(); i++){
            printf("id1: %d, id2: %d -> %f\n", v[i].target1.id.target_local_id, v[i].target2.id.target_local_id, v[i].d);
        }
    }
        printf("\n");
}

void DistanceAsociationEIF::drawMarkedTargets(std::vector <std::vector <bool>> m){
    printf("\n\nmarkedTargets (%u devices)\n", (unsigned int)m.size());
    if(m.size() > 0){
        /*printf("vectors size -> ");
        for(unsigned int i = 0; i < m.size(); i++){
            printf("%u ", (unsigned int)m[i].size());
        }*/
        for(unsigned int i = 0; i < m.size(); i++){
            printf("\ndevice %d -> ", i);
            for(unsigned int j = 0; j < m[i].size(); j++){            
                if(m[i][j])
                    printf("1 ");
                else
                    printf("0 ");
            }
        }
    }
    printf("\n");
}

void DistanceAsociationEIF::set_fixed_sigmaGPS(bool value){
    fixed_sigmaGPS = value;
}
bool DistanceAsociationEIF::get_fixed_sigmaGPS(){
    return fixed_sigmaGPS;
}
