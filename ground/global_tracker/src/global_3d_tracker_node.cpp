#include "ros/ros.h"
#include <geometry_msgs/PoseStamped.h>
#include <string>
#include <DistanceAsociationEIF.h>
#include "global_tracker/MeasurementInfo.h"
#include <visualanalysis_msgs/TargetPositionROI2DArray.h>
#include <signal.h>

// Namespaces
using namespace ros;

/***** DEFINITIONS *****/
// Filter timings
#define DEFAULTALGORITMRATE 1
#define MAXCONTRIBUTIONWAITTIMEDEFAULT 0.02

// Camera
#define FOCAL_LENGTH_DEFAULT 0.0014451673
#define PIXEL_SIZE_DEFAULT 0.00000155
#define FX_DEFAULT 932.366017
#define FY_DEFAULT 932.366017
#define CX_DEFAULT 960.5
#define CY_DEFAULT 540.5

// Offset of gimbal with respect to drone frame
// #define GIMBAL_OFFSET_X_DEFAULT -0.051
// #define GIMBAL_OFFSET_Y_DEFAULT 0.0
// #define GIMBAL_OFFSET_Z_DEFAULT -0.162

// Information filter
#define ORDER 6
#define SIGMAMODEL 10
#define SIGMACAM 0.01	// Desviación típica de las medidas visuales
#define SIGMAGPS 1	// Desviación típica de las medidas gps

// TARGET
#define GPS_ALTITUDE_DEFAULT 0
/************************/

/*** STRUCTURES ***/
/******************/

/***** DECLARATION (Y DEFINITION) OF FUNCTIONS *****/
void tracking2DCB(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& pose2D);

void targetsGPSCB(const multidrone_msgs::TargetStateArray::ConstPtr& gpsinfo);

void RX_EIF_msg(const global_tracker::EIFasoc_P2P::ConstPtr &eifmsg);

void tracking3D_calculate(const ros::TimerEvent& event);

void tracking3D_calculate_end(const ros::TimerEvent& event);

Eigen::Matrix4d PoseStamped2Hmatrix(geometry_msgs::PoseStamped poseStamp);

Eigen::Quaterniond QuatFromTwoVectors(Eigen::Vector3d u, Eigen::Vector3d v);
/***************************************************/

/***** GLOBAL VARIABLES *****/
int id;	// Own ID
int group_id;

//Subscripción ros
Subscriber sub_tracking2D;
Subscriber sub_targets_gps;
Subscriber sub_internal_topic;

// Publicación ros
Publisher pub_tracking3D_pose;
Publisher pub_internal_topic;

//Publisher pubTargetPos, pubTargetGPS;	// MATLAB

// Camera parameters
double fx, fy, cx, cy, focal_length, pixel_size;

// Offset of gimbal with respect to drone frame
std::vector <double> gimbalOffset;
std::vector <double> visualOffset;

// Transformada de la cámara del drone
Eigen::Matrix4d globalToCamera;
Eigen::Matrix4d RZ180;

/*** Variables del Filtro de Información Distribuído ***/
unsigned int nseq;

double Tp;	// Tiempo entre iteraciones
//int orden;
double sigmaModel; 	// Desviación típica del modelo
double sigmaCam, sigmaGPS; 	// Desviación típica del modelo de observación para ambos tipos de medidas
bool fixed_sigmaGPS;				// Indica si se usa la Qgps fija o la Qgps_k que viene con la medida gps
Eigen::MatrixXd I(ORDER,ORDER);	// Matriz identidad
Eigen::MatrixXd R(ORDER,ORDER);	// Matriz de varianza del modelo de predicción
Eigen::MatrixXd G(ORDER,ORDER);	// Jacobiano del modelo
Eigen::MatrixXd Qvisual(2,2);	// Matriz de varianza del modelo de observación para medidas visuales
Eigen::MatrixXd Qgps(3,3);	// Matriz de varianza del modelo de observación para medidas gps

Eigen::VectorXd initialState(ORDER,1);	// Estado inicial

Eigen::MatrixXd sigmat0(ORDER,ORDER);	// Sigma inicial y de reinicio

Eigen::MatrixXd sigmat1(ORDER,ORDER);
Eigen::MatrixXd ept1(ORDER,1);

global_tracker::EIFasoc_P2P enviapMean;
global_tracker::EIFasoc_P2P enviaContribution;
global_tracker::Contribution contribution;
std::vector <global_tracker::Contribution> contrib_vector;
global_tracker::MeasurementInfo measure;
std::vector <global_tracker::MeasurementInfo> targetMeasurements;

Eigen::Quaterniond oriresQ;
Eigen::Vector3d rotationZero;
Eigen::Vector3d orires;

// Publicación de resultados
multidrone_msgs::TargetStateArray tracking3Dresult;
std::vector<multidrone_msgs::TargetState> targetEstimationList;
multidrone_msgs::TargetState auxTargetState;
Track auxTrack;

int lastnContributions;	// DEBUG

Eigen::Matrix4d drone_pose;
Eigen::Matrix4d cam_ori;
Eigen::Quaterniond cam_oriQ;

Eigen::Matrix4d poseCam;
Eigen::MatrixXd aux_z(2,1);	// Última medida
std::vector <Eigen::MatrixXd> z;	// Última medida para cada target

// Proyección
//global_tracker::Targets3DArray_P2P targets3Dmsg;     // Mensaje de asociación, contiene la lista de targets y la lista de sus ids
//global_tracker::Targets3DArray_P2P targets3D_request;// Mensaje de request de mensajes de asociación de otros drones
std::vector <geometry_msgs::Point> targets3Darray;       // Lista de targets3D
std::vector <unsigned short int> IDtargets3Darray;       // Lista de IDs de los targets3D antes de la asociación  
Eigen::Vector3d ps, po; // Puntos que forman la recta dónde se encuentra un target
Eigen::Vector3d v; // Vector de dirección de la recta dónde se encuentra el target
Eigen::Vector4d ps_cam; // Punto auxiliar en el que se almacena la posición del punto origen de la recta dónde se encuenra
                        // el target, en el sistema de referencia de la cámara
//Eigen::Matrix4d Tcam;   // Matriz homogénea con la pose de la cam
//Eigen::Quaterniond quat;    // Quaternion auxiliar
geometry_msgs::Point proj_res;  // resultado de la proyección
double GPS_altitude;					// Altura del dentro del target

// Control de ejecución del algoritmo de tracking
ros::Timer timer;
double tracking3D_rate;
double maxContributionWaitTime;
ros::Timer waitContributionTimer;

// ASOCIACIÓN
int asociation_window;
double asociation_th, track_asociation_th;
DistanceAsociationEIF asoc;
std::vector <global_tracker::EIFasoc_P2P> EIFasocTable;    // Vector de mensajes con listas de targets3D

int max_track_time_without_measures;	// Número de iteraciones sin medidas antes de ser borrado un track

// Node Handler ROS pointer
ros::NodeHandle* pnh;	// Let access the ROS node handler out of main function
/*******************************************************/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "onboard_3d_tracker_node");

	// Inicialización de variables
	ros::NodeHandle nh("");
	pnh = &nh;

	ros::NodeHandle nhPrivate("~");

	// Matrices de transformación
	Eigen::Matrix4d RY90;	
	RY90 <<  0.0, 0.0, 1.0, 0.0,
					 0.0, 1.0, 0.0, 0.0,
					-1.0, 0.0, 0.0, 0.0,
					 0.0, 0.0, 0.0, 1.0;

	Eigen::Matrix4d RZ90n;
	RZ90n <<  0.0, 1.0, 0.0, 0.0,
					 -1.0, 0.0, 0.0, 0.0,
					  0.0, 0.0, 1.0, 0.0,
					  0.0, 0.0, 0.0, 1.0;

	globalToCamera = RY90*RZ90n;

	// Inicialización variables Filtro de Información
	nseq = 0;

	/*** READ PARAMETERS ***/
	// Parámetros launch
	std::string internal_topic;
	
 	if(!nhPrivate.getParam("drone_id", id)){
   		id = 0;
 	}

 	if(!nhPrivate.getParam("group_id", group_id)){
   		group_id = 0;	// El 0 indica que puede trabajar con cualquier grupo
 	}

 	if(!nhPrivate.getParam("sigmaModel", sigmaModel)){
   		sigmaModel = SIGMAMODEL;
 	}

 	if(!nhPrivate.getParam("sigmaCam", sigmaCam)){
   		sigmaCam = SIGMACAM;
 	}

	if(!nhPrivate.getParam("sigmaGPS", sigmaGPS)){
   		sigmaGPS = SIGMAGPS;
 	}

	if(!nhPrivate.getParam("fixed_sigmaGPS", fixed_sigmaGPS)){
   		fixed_sigmaGPS = false;
 	}

	if(!nhPrivate.getParam("/focal_length", focal_length)){
   		focal_length = FOCAL_LENGTH_DEFAULT;
 	}
	
	if(!nhPrivate.getParam("/pixel_size", pixel_size)){
   		pixel_size = PIXEL_SIZE_DEFAULT;
 	}
	
	std::vector<double> camera_matrix;
	if(!nhPrivate.getParam("/camera_matrix/data", camera_matrix)){
		fx = FX_DEFAULT;
		fy = FY_DEFAULT;
		cx = CX_DEFAULT;
		cy = CY_DEFAULT;
 	}else{
		fx = camera_matrix[0];
		fy = camera_matrix[4];
		cx = camera_matrix[2];
		cy = camera_matrix[5];
	}

	// Altura dle GPS respecto al suelo
	if(!nhPrivate.getParam("GPS_altitude", GPS_altitude)){
   		GPS_altitude = GPS_ALTITUDE_DEFAULT;
 	}
	
	// Visual mark relative position to the GPS position of the target
	if(!nhPrivate.getParam("visual_offset_toGPS", visualOffset)){
   		visualOffset.assign(3,0.0);
 	}

	if(!nhPrivate.getParam("gimbalOffset", gimbalOffset)){
   		gimbalOffset.assign(3,0.0);
 	}

 	if(!nhPrivate.getParam("internal_topic", internal_topic)){	// Comunications between Tracking3D nodes of different drones
   		internal_topic = "/comModule";
 	}

 	if(!nhPrivate.getParam("tracking3D_rate", tracking3D_rate)){
   		tracking3D_rate = DEFAULTALGORITMRATE;
 	}

 	if(!nhPrivate.getParam("maxContributionWaitTime", maxContributionWaitTime)){
   		maxContributionWaitTime = MAXCONTRIBUTIONWAITTIMEDEFAULT;
 	}

	if(!nhPrivate.getParam("asociation_window", asociation_window)){
   		asociation_window = 20;
 	}

    if(!nhPrivate.getParam("asociation_th", asociation_th)){
   		asociation_th = 0.3;
 	}

	if(!nhPrivate.getParam("track_asociation_th", track_asociation_th)){
   		track_asociation_th = 0.3;
 	}

	if(!nhPrivate.getParam("max_track_time_without_measures", max_track_time_without_measures)){
   		max_track_time_without_measures = 30;
 	}
	/***********************/

	// Se desactivan las medidas hasta que no llegue una válida
	// z(0) = -1;
	// z(1) = -1;
	I = Eigen::MatrixXd::Identity(ORDER,ORDER);
	R = I*sigmaModel*sigmaModel;
	R(0,3) = sigmaModel*sigmaModel;
	R(1,4) = sigmaModel*sigmaModel;
	R(2,5) = sigmaModel*sigmaModel;
	R(3,0) = sigmaModel*sigmaModel;
	R(4,1) = sigmaModel*sigmaModel;
	R(5,2) = sigmaModel*sigmaModel;
	Qvisual = Eigen::MatrixXd::Identity(2,2)*sigmaCam*sigmaCam;
	Qgps = Eigen::MatrixXd::Identity(3,3)*sigmaGPS*sigmaGPS;
	Tp = 1.0/tracking3D_rate;
	initialState << 0, 0, 0, 0, 0, 0;

	cam_ori(3,0) = 0;
	cam_ori(3,1) = 0;
	cam_ori(3,2) = 0;
	cam_ori(3,3) = 1.0;

	ps_cam(3) = 1;  // El cuarto elemento del vector homogéneo es siempre 1

	// sigma inicial
	sigmat0 = I*sigmaModel*sigmaModel;
	sigmat0 = sigmat0.inverse();

	sigmat1 = sigmat0;
	ept1 = sigmat1*initialState;
	
	tracking3Dresult.header.frame_id = "/map";
	rotationZero << 1.0, 0.0, 0.0;

	asoc.set_asociation_window(asociation_window);
	asoc.set_asociation_th(asociation_th);
	asoc.set_track_asociation_th(asociation_th);
	// Modelo de predicción (coincide con su jacobiano en este caso)
	G << 1, 0, 0, Tp, 0, 0,
		0, 1, 0, 0, Tp, 0,
		0, 0, 1, 0, 0, Tp,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 1;
	asoc.setPredictionModel(G);
	asoc.setPredictionModelTrust(R);
	asoc.setVisualMeasureTrust(Qvisual);
	asoc.setGPSMeasureTrust(Qgps);
	asoc.setInitialState(sigmat1,ept1);
	asoc.set_fixed_sigmaGPS(fixed_sigmaGPS);
	asoc.set_max_track_time_without_pairing_refresh(max_track_time_without_measures);

	//std::cout << sigmat1 << std::endl;

 	//printf("\n\nStarting distributedEIF_%d\n\n", id);	// DEBUG

 	// Create the ROS susbcription and publishers
 	// Hay que suscribirse al topic de tracking 2D del propio drone
	sub_tracking2D = nh.subscribe("visual_analysis/target_position_2d", 1, tracking2DCB, ros::TransportHints().tcpNoDelay());

	// Hay que suscribirse al topic que envía las medidas de GPS de los targets para integrarlas en el filtro
	sub_targets_gps = nh.subscribe("/targets_pose", 1, targetsGPSCB, ros::TransportHints().tcpNoDelay());

	// Hay que publicar en el topic de comunicaciones internas con otros nodos de Tracking3D
	pub_internal_topic = nh.advertise<global_tracker::EIFasoc_P2P> (internal_topic, 1);

	// Hay que suscribirse al topic de comunicaciones internas con otros nodos de de Tracking3D
	sub_internal_topic = nh.subscribe(internal_topic, 1, RX_EIF_msg, ros::TransportHints().tcpNoDelay());

	// Hay que publicar en el topic de tracking 3D del propio drone
	pub_tracking3D_pose = nh.advertise<multidrone_msgs::TargetStateArray> ("/target_3d_state", 1);

	// BORRAR DEBUG MATLAB
	/*pubTargetPos = nh.advertise<geometry_msgs::PoseStamped> ("auxPose",1);
	pubTargetGPS = nh.advertise<geometry_msgs::PoseStamped> ("auxGPS",1);*/
	// BORRAR DEBUG MATLAB

	// Periodic timer that execute the algoritm with period Tp
	timer = nh.createTimer(ros::Duration(Tp), tracking3D_calculate);

	ros::spin();

	return 0;
}

void tracking3D_calculate(const ros::TimerEvent& event){
  //printf("\nLlamada a tracking3D_calculate.\n");	// DEBUG
  // Esta función inicia el cálculo de la posición del objetivo mediante
  // un Filtro de Información extendido distribuido
	//printf("\n\n");
	//ROS_INFO("-----COMENZANDO NUEVO CALCULO----- nseq: %u", nseq); // DEBUG

	/*** Se publica el último resultado guardado ***/
	int trackListSize = asoc.getTrackListSize();
	if(trackListSize > 0){	// Only publish if there are at least one target
		targetEstimationList.clear();
		for(unsigned int i = 0; i < trackListSize; i++){
			// Se carga el track i y se rellena el targetState con los datos correspondientes
			auxTrack = asoc.getTrack(i);
			auxTargetState.target_id = auxTrack.pairings.common_id;
			auxTargetState.pose.pose.position.x = auxTrack.state.x;
			auxTargetState.pose.pose.position.y = auxTrack.state.y;
			auxTargetState.pose.pose.position.z = auxTrack.state.z;

			// Se calcula la orientación como quaternion
			// La orientación se calcula a partir del vector de velocidad
			orires(0) = asoc.mut1[i](3);
			orires(1) = asoc.mut1[i](4);
			orires(2) = asoc.mut1[i](5);
			oriresQ = QuatFromTwoVectors(rotationZero, orires.normalized());

			auxTargetState.pose.pose.orientation.x = oriresQ.x();
			auxTargetState.pose.pose.orientation.y = oriresQ.y();
			auxTargetState.pose.pose.orientation.z = oriresQ.z();
			auxTargetState.pose.pose.orientation.w = oriresQ.w();

			targetEstimationList.push_back(auxTargetState);	// Se añade el target a la lista
		}

		// Final message
		tracking3Dresult.header.stamp = ros::Time::now();
		tracking3Dresult.targets = targetEstimationList;

		// Se publica
		pub_tracking3D_pose.publish(tracking3Dresult);
	}
	/***********************************************/

	asoc.initilizeEIFiteration();	// Se inicializan las matrices del EIF para cada track (pone a 0 sigmat y ept)
	
	// Se ha recibido la información de la cámara preparada para ser procesada por el filtro estadístico
	// En este CB se realiza la predicción y se manda la media predecida al resto de nodos, para que 
	// calcular su contribución a la actualización y enviarla de vuela, llegando a este nodo de nuevo
	// por el CB RX_nodeContributionCB  

	// Predicción
	asoc.updateTrackListStatePredictionModel();

	//asoc.print_trackList();	// DEBUG

	// Se prepara el paquete de datos para ser enviado
	enviapMean.head = 0;
	enviapMean.group_id = group_id;  
	enviapMean.destination_id = 0;	// El 0 indica broadcast. Todos los demás nodos deben recibir la media predicha (del mismo grupo)
	enviapMean.source_id = id;
	enviapMean.nseq = nseq;
	enviapMean.type = 0; 	// Tipo media predicha
	// Se añaden al mensaje las medias predichas calculadas
	contrib_vector.clear();
	Track trackinfo;
	for(unsigned int i = 0; i < asoc.getTrackListSize(); i++){
		contribution.state_vector[0] = asoc.pmut[i](0);
		contribution.state_vector[1] = asoc.pmut[i](1);
		contribution.state_vector[2] = asoc.pmut[i](2);
		contribution.state_vector[3] = asoc.pmut[i](3);
		contribution.state_vector[4] = asoc.pmut[i](4);
		contribution.state_vector[5] = asoc.pmut[i](5);
		contrib_vector.push_back(contribution);
	}
	enviapMean.contrib = contrib_vector;

	// Se publica la estimación para que el resto de drones devuelvan su contribución
	pub_internal_topic.publish(enviapMean);

	// Tras la publicación se lanza un timer en modo oneshoot que ejecutará el final del cálculo del algoritmo
	// Esta será el máximo tiempo que tendrán para llegar el resto de contribuciones
	waitContributionTimer = pnh->createTimer(ros::Duration(maxContributionWaitTime), tracking3D_calculate_end, true);

	// Suma la predicción a la estimacion final del EIF para cada track
	asoc.addPrediction();

	/*** CONTRIBUCIÓN PROPIA ***/
	// Aunque no se haya recibido tracking2D, se pushea en la última tabla de targets la lista propia para aegurar que la tabla
	// no está vacía en ningún caso, aunque no tenga targets (artificio de programación)
	
	// La contribución no puede ser calculada sin haber realizado la asociación, por tanto se
	// recopilarán los datos de necesarios para el cálculo de la contribución y se guardan en una tabla
	global_tracker::EIFasoc_P2P m;
	m.head = 0;
	m.group_id = group_id;  
	m.destination_id = 0;	// El 0 indica broadcast. Todos los demás nodos deben recibir la media predicha (del mismo grupo)
	m.source_id = id;
	m.nseq = nseq;
	m.type = 1; 	// Tipo datos de contribución
	// También se guardan la lista de targets y sus ids
	m.targets3D = targets3Darray;
	m.targetsID = IDtargets3Darray;

	// Es necesaria la pose de la cámara
	Eigen::Matrix4d Tpc = poseCam;
	Tpc = Tpc*globalToCamera;
	// La matriz utilizada por el algoritmo es la inversa
	Eigen::Matrix4d T = Tpc.inverse();
	for(unsigned int i = 0; i < targets3Darray.size(); i++){		
		// Se calcula recopilan los datos de medida del target i y se comienza a generar la tabla de targets con la información del EIF
		measure.z.x = z[i](0);
		measure.z.y = z[i](1);

		// Se guarda (por filas) la matriz de transformación
		for(int h = 0; h < 4; h++){
			for(int j = 0; j < 4; j++){
				measure.T[h*4 + j] = T(h,j);
			}
		}

		targetMeasurements.push_back(measure);		
	}	// Fin de contribución propia

	m.measurements = targetMeasurements;
	EIFasocTable.push_back(m);	// Se añade a la tabla
	targetMeasurements.clear();

	nseq++;
}

void tracking3D_calculate_end(const ros::TimerEvent& event){
	/*** A PARTIR DE AQUÍ NO SE USARÁN LOS DATOS DE CONTRIBUCION QUE LLEGUEN EN ESTA ITERACIÓN ***/
	// En este punto se han guardado los valores calculados por el algoritmo, el resto de contribuciones 
	// que lleguen hasta que se comience un nuevo cálculo no será utilizado (aunque sean sumadas las contribuciones, 
	// las variables son puestas a 0 al comenzar un nuevo cáculo)

	//ROS_INFO("NCONTRIBUTIONS: %d, nseq: %u", nContributions, nseq);	// DEBUG

	/*** CIERRE DE LA ITERACIÓN DEL EIF ***/
	// Se añade la tabla con datos de contribución y targets 3D a las tablas de asoaciación
	// (últimos datos de contribución y ventana de tiempo)
	asoc.push_EIFasocTableWithGPSdata(EIFasocTable);	

	// DEBUG
	//asoc.draw_targets3Dtables();      // Se pintan todas las tablas de la ventana de asociación completa
	//printf("\n");
	// DEBUG

	// Con esta nueva tabla de targets3D se actualiza la tabla de emparejamientos
	asoc.updateTrackListPairings();

	//asoc.print_trackList();	// DEBUG

	// Una vez actualizada la tabla de emparejamientos se pueden hacer los cálculos de contribuciones
	// y realizar la estimación del EIF
	asoc.addContributions();

	// Se guardan el vector y matriz de información para la siguiente iteración
	asoc.saveEstimation();

	// Se calcula el resultado en la forma dual (se recupera la estimación del vector de estados)
	// y la matriz de covarianza y se actualiza con ello el estado de cada track
	asoc.updateTrackListStates();

	// BORRAR DEBUG MATLAB
	/*geometry_msgs::PoseStamped auxGPSpos;	
	if(asoc.lastGPSdata.targets3D.size() > 0){
		auxGPSpos.header.stamp = ros::Time::now();
		auxGPSpos.pose.position.x = asoc.lastGPSdata.targets3D[0].x;
		auxGPSpos.pose.position.y = asoc.lastGPSdata.targets3D[0].y;
		auxGPSpos.pose.position.z = asoc.lastGPSdata.targets3D[0].z;
		pubTargetGPS.publish(auxGPSpos);
	}

	geometry_msgs::PoseStamped auxPose;
	if(targetEstimationList.size() > 0){
		auxTrack = asoc.getTrack(0);
		auxPose.header.stamp = auxGPSpos.header.stamp;
		auxPose.pose.position.x = auxTrack.state.x;
		auxPose.pose.position.y = auxTrack.state.y;
		auxPose.pose.position.z = auxTrack.state.z;
		pubTargetPos.publish(auxPose);
	}*/
	// BORRAR DEBUG MATLAB

	// Borrado de vectores y otras variables para ser rellenadas en la siguiente iteración
	EIFasocTable.clear();	// Se borra la tabla para crear una nueva en la siguiente iteración
	asoc.clearGPSdataVector();	// DEBUG BORRAR (Si se quita de aquí, descomentarlo de DistanceAsociationEIF)

	//asoc.print_trackList();	// DEBUG
	/**************************************/
}

// Esta función se llama cuando es recibido un mensaje EIF de otro nodo
void RX_EIF_msg(const global_tracker::EIFasoc_P2P::ConstPtr &eifmsg){
	//ROS_INFO("Soy %d, recibido paquete del eif. group_id: %d, destination_id: %d, source_id: %d", id, eifmsg->group_id, eifmsg->destination_id, eifmsg->source_id);
	// Según si es un mensaje de media predicha o de contribución se trata de distinta forma
	if (eifmsg->type == 0){	// media predicha
		//ROS_INFO("Soy %d, es un paquete de media predicha.", id);	// DEBUG
		// El nodo no se responde a sí mismo
		if(!(eifmsg->source_id == id)){
			// Cuando se recibe una media predicha debe enviarse los datos de contribución propia al origen
			// sólo se envía si el nodo es el destinatario (o el mensaje es broadcast) y el grupo es el mismo
			if((eifmsg->group_id == group_id || eifmsg->group_id == 0) && (eifmsg->destination_id == id || eifmsg->destination_id == 0)){
				global_tracker::EIFasoc_P2P m;
				m.head = 0;
				m.group_id = group_id;  
				m.destination_id = eifmsg->source_id;	// El 0 indica broadcast. Todos los demás nodos deben recibir la media predicha (del mismo grupo)
				m.source_id = id;
				m.nseq = nseq;
				m.type = 1; 	// Tipo datos de contribución
				// También se guardan la lista de targets y sus ids
				m.targets3D = targets3Darray;
				m.targetsID = IDtargets3Darray;

				// El primer paso es leer la posición actual de la cámara (la última leída)
				Eigen::Matrix4d Tpc = poseCam;
				Tpc = Tpc*globalToCamera;
				// La matriz utilizada por el algoritmo es la inversa
				Eigen::Matrix4d T = Tpc.inverse();
				for(unsigned int i = 0; i < targets3Darray.size(); i++){
					// Se calcula recopilan los datos de medida del target i y se comienza a generar la tabla de targets con la información del EIF
					measure.z.x = z[i](0);
					measure.z.y = z[i](1);

					// Se guarda (por columnas) la matriz de transformación
					for(int h = 0; h < 4; h++){
						for(int j = 0; j < 4; j++){
							measure.T[h*4 + j] = T(h,j);
						}
					}

					targetMeasurements.push_back(measure);					
				}
				m.measurements = targetMeasurements;
				// Se publica
				pub_internal_topic.publish(m);
				targetMeasurements.clear();
			}
		}
	}else if(eifmsg->type == 1){	// Información de contribución
		//ROS_INFO("Soy %d, es un paquete de contribucion de %d hacia %d.", id, eifmsg->source_id,eifmsg->destination_id);
		//ROS_INFO("mi nseq | n seq recibido: %d | %d", nseq, eifmsg->nseq);
		// Sólo se acepta el mensaje si este nodo es para quién va dirigido (y de nodos del mismo grupo)
		// Sólo se aceptan nodeContribution con el mismo número de secuencia que el actual
		// De esta forma se evita la suma de contribuciones de secuencias anteriores
		// independiente de retrasos en la llegada de los paquetes
		if((eifmsg->group_id == group_id || eifmsg->group_id == 0) && (id == eifmsg->destination_id /*&& eifmsg->nseq == nseq*/)){
		  //ROS_INFO("Soy %d, recibida nodeContribution de %d", id, eifmsg->source_id);

			// Se guarda el mensaje con los datos de contribución en la tabla
			EIFasocTable.push_back(*eifmsg);	// Se añade a la tabla			
		}
	}
}

void targetsGPSCB(const multidrone_msgs::TargetStateArray::ConstPtr& gpsinfo){
	//ROS_INFO("Recibido gpsinfo, soy %d", id);	// DEBUG

	//asoc.manageGPSdata(*gpsinfo);

	// La posición del target va a ser la del visual
	// Si la posición del GPS respecto al la marca visual es conocida
	// debe ser ajustada la posición dada por el GPS para ser integrada con la del 
	// visual con precisión
	multidrone_msgs::TargetStateArray auxTargetState = *gpsinfo;
	for(unsigned int i = 0; i < auxTargetState.targets.size(); i++){
		auxTargetState.targets[i].pose.pose.position.x += visualOffset[0];
		auxTargetState.targets[i].pose.pose.position.y += visualOffset[1];
		auxTargetState.targets[i].pose.pose.position.z += visualOffset[2];
	}
	asoc.manageGPSdata(auxTargetState);
}

void tracking2DCB(const visualanalysis_msgs::TargetPositionROI2DArray::ConstPtr& tracking2D){
  //ROS_INFO("Recibido tracking2D, soy %d", id);	// DEBUG
  //ROS_INFO("Recibido tracking2D, soy %d. X: %d, Y: %d, Size: %d", id, pose2D->targets.at(0).x, pose2D->targets.at(0).y, pose2D->targets.at(0).size);	// DEBUG

  /*** Medidas EIF ***/
  // Se actualiza la variable que guarda las medidas para cuándo se realice la actualización
  // Y para el cálculo de contribuciones a otros nodos
  // Se guarda un vector con la medida para cada target
  z.clear();	// Lista de medidas anterior borrada
  for(unsigned int i = 0; i < tracking2D->targets.size(); i++){
		if(tracking2D->targets[i].x == -1){
			aux_z(0) = -1;
			aux_z(1) = -1;
		}else{
			//aux_z(0) = (tracking2D->targets[i].x - tracking2D->camera.principalPointX)*tracking2D->camera.pixelSizeX/tracking2D->camera.focalLength;
			//aux_z(1) = -(tracking2D->targets[i].y - tracking2D->camera.principalPointY)*tracking2D->camera.pixelSizeY/tracking2D->camera.focalLength;

			// MI SIMULADOR
			//aux_z(0) = (tracking2D->targets[i].x - cx)/fx;
			//aux_z(1) = -(tracking2D->targets[i].y - cy)/fy;
			// GAZEBO
			aux_z(0) = (tracking2D->targets[i].x - cx)/fx;
			aux_z(1) = (tracking2D->targets[i].y - cy)/fy;
		}
		z.push_back(aux_z);	// Se añade al vector de medidas para cada target
  }  
  /******************/ 

  /*** Is necesary also the camera pose ***/
  // It can be obtained with drone pose and gimbal orientation
  drone_pose = PoseStamped2Hmatrix(tracking2D->drone_pose);
  cam_oriQ.x() = tracking2D->gimbal.orientation.x;
  cam_oriQ.y() = tracking2D->gimbal.orientation.y;
  cam_oriQ.z() = tracking2D->gimbal.orientation.z;
  cam_oriQ.w() = tracking2D->gimbal.orientation.w;
  cam_ori.block(0,0,3,3) = cam_oriQ.normalized().toRotationMatrix();

  cam_ori(0,3) = gimbalOffset[0];
  cam_ori(1,3) = gimbalOffset[1];
  cam_ori(2,3) = gimbalOffset[2];

  poseCam = drone_pose*cam_ori;
	/****************************************/

  /*** Cálculo de lista de targets3D (mediante proyección) ***/
  targets3Darray.clear();  // Se borra la lista de targets anterior
  IDtargets3Darray.clear();  // Se borra la lista de IDs de targets anterior

  // Se prepara la información que debe ser enviada a otros drones para su propia asociación
	// y para realizar la asociación propia
	// esta información es directamente la posición 3D reconstruida mediante proyección de cada uno de los targets
	// dados por el tracking2D

	// Se calculan los dos puntos que forman la recta dónde se encuentra el target
	// Se calculan en el sistema de referencia global
	// Uno de ellos es el punto [0,0,0] en el sistema de referencia de la cámara
	// este es directamente la posición de la cámara en global
	// El Rayo dónde se encuentra el target se dirige hacia este punto
	po(0) = poseCam(0,3);
	po(1) = poseCam(1,3);
	po(2) = poseCam(2,3);
  for(int i = 0; i < tracking2D->targets.size(); i++){
		// Cálculo de proyección para el target i
		// Rellenar los vectores del mensaje (parte de asociación)

		// El punto origen que forma la recta junto al punto po es el punto del target en
		// la imagen (plano sensor)
		// Primero se calculan sus coordenadas en el sistema de referencia de la cámara

		ps_cam(0) = -focal_length;
		ps_cam(1) = (tracking2D->targets[i].x - cx)*pixel_size;
		ps_cam(2) = (tracking2D->targets[i].y - cy)*pixel_size;

		ps = (poseCam*ps_cam).head(3);   // Punto del plano sensor en coordenadas globales

		v = po - ps;    // Vector de dirección del rayo proyectado

		// DEBUG    SE VA A SUPONER K EL TARGET SIEMPRE ESTÁ A ALTURA FIJA EN EJES GLOBALES, HACER PROYECCIÓN REAL
		proj_res.z = GPS_altitude + visualOffset[2];
		proj_res.x = po(0) + ((proj_res.z-po(2))/v(2))*v(0);
		proj_res.y = po(1) + ((proj_res.z-po(2))/v(2))*v(1);

		//ROS_INFO("Target %d, Projection -> X: %f, Y: %f, Z: %f\n", i, proj_res.x, proj_res.y, proj_res.z);	// DEBUG
		// DEBUG

		// Se añade el resultado al vector de targets3D del mensaje
		targets3Darray.push_back(proj_res);
		// Se guarda también su ID (ID local al device pre-asociación)
		IDtargets3Darray.push_back(tracking2D->targets[i].id);
  }
  /***********************************************************/

	/*** Cada vez que se recibe un mensaje de tracking2D se publica el último resultado guardado ***/
	// auxTargetState.target_id = 0;
	// auxTargetState.pose.pose.position.x = res(0);
	// auxTargetState.pose.pose.position.y = res(1);
	// auxTargetState.pose.pose.position.z = res(2);
	// auxTargetState.pose.pose.orientation.x = oriresQ.x();
	// auxTargetState.pose.pose.orientation.y = oriresQ.y();
	// auxTargetState.pose.pose.orientation.z = oriresQ.z();
	// auxTargetState.pose.pose.orientation.w = oriresQ.w();
	// auxTargetState.velocity.twist.linear.x = res(3);
	// auxTargetState.velocity.twist.linear.y = res(4);
	// auxTargetState.velocity.twist.linear.z = res(5);
	// /*auxTargetState.velocity.twist.angular.x = -1;
	// auxTargetState.velocity.twist.angular.y = -1;
	// auxTargetState.velocity.twist.angular.z = -1;*/
	// auxTargetState.velocity.twist.angular.x = lastnContributions; // DEBUG(INFO ADICIONAL)

	// // La sigma se guarda en la matriz de covarianza de la pose
	// for(int j = 0; j < 36; j++){
	// 	auxTargetState.pose.covariance[j] = resSigma(j/ORDER, j - ORDER*(int)(j/ORDER));
	// }
	
	// targetEstimationList[0] = auxTargetState;
	
	/*targetEstimationList.clear();
	for(unsigned int i = 0; i < asoc.getTrackListSize(); i++){
		// Se carga el track i y se rellena el targetState con los datos correspondientes
		auxTrack = asoc.getTrack(i);
		auxTargetState.target_id = auxTrack.pairings.common_id;
		auxTargetState.pose.pose.position.x = auxTrack.state.x;
		auxTargetState.pose.pose.position.y = auxTrack.state.y;
		auxTargetState.pose.pose.position.z = auxTrack.state.z;

		targetEstimationList.push_back(auxTargetState);	// Se añade el target a la lista
	}

	// Final message
	tracking3Dresult.header.stamp = ros::Time::now();
	tracking3Dresult.targets = targetEstimationList;

	// Se publica
	pub_tracking3D_pose.publish(tracking3Dresult);*/
}

Eigen::Matrix4d PoseStamped2Hmatrix(geometry_msgs::PoseStamped pose){
	Eigen::Matrix4d T = Eigen::MatrixXd::Identity(4,4);
	// Se obtiene la posición
	Eigen::Vector3d P;
	P(0) = pose.pose.position.x;
	P(1) = pose.pose.position.y;
	P(2) = pose.pose.position.z;

	// Se obtiene la orientación
	Eigen::Quaterniond q;
	q.x() = pose.pose.orientation.x;
	q.y() = pose.pose.orientation.y;
	q.z() = pose.pose.orientation.z;
	q.w() = pose.pose.orientation.w;
	Eigen::Matrix3d Rt = q.normalized().toRotationMatrix();

	//std::cout << Rt << std::endl;
	//ROS_INFO("q: %f, %f, %f, %f", q.x(), q.y(), q.z(), q.w());	// DEBUG

	// Se actualiza la matriz 4x4 de la pose
	T.block(0,3,3,1) = P;
	T.block(0,0,3,3) = Rt;
	T(3,3) = 1.0;

	//ROS_INFO("P>: %f, %f, %f", P(0), P(1), P(2));	// DEBUG
	//std::cout << "\nTrans:\n" << Trans << std::endl;	// DEBUG

	return T;
}

Eigen::Quaterniond QuatFromTwoVectors(Eigen::Vector3d u, Eigen::Vector3d v){
	Eigen::Vector3d w = u.cross(v); 
	Eigen::Quaterniond q(u.dot(v), w(0), w(1), w(2)); 
	q.w() += q.norm(); 
	return q.normalized(); 
}