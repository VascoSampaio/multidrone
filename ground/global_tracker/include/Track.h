#include "ros/ros.h"
#include <vector>
#include <deque>
#include <geometry_msgs/Point.h>

/*** STRUCTURES ***/
typedef struct target_local_id{
    int device_id;
    int target_local_id;
} target_local_id;

typedef struct pairing{
    std::vector <target_local_id> targets;    // Listado de ids para cada drone del target de este emparejamiento [drone_id,target_id]
    int common_id;  // Id común asignada con la que serán tratados los targets del emparejamiento
} pairing;
/******************/

class Track
{
public:

pairing pairings;   // Emparejamientos
geometry_msgs::Point state; // Estado del track (posición) común
unsigned int time_since_last_pairing;   // Tiempo sin haber sido refrescado un emparejamiento con el track. Parámetro usado para borrar un track sin uso durante demasiado tiempo
unsigned int life_time;     // Tiempo de vida del track (iteraciones)
std::deque <geometry_msgs::Point> stateTW;

// Constructor y destructor
Track();
Track(pairing p, geometry_msgs::Point state0, unsigned int time_since_last_pairing, unsigned int life_time);
~Track();

// Añade un nuevo estado a la ventana de tiempo de estados de un track de la lista de tracks
// Cuando la ventana de tiempo de estados está llena se van borrando los estados antiguos
void pushStateTimeWindow(geometry_msgs::Point state, int asociation_window);

};