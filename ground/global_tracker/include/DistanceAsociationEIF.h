#include "ros/ros.h"
#include <iostream>
#include <Track.h>
#include "global_tracker/Targets3DArray_P2P.h"
#include "global_tracker/EIFasoc_P2P.h"
#include <multidrone_msgs/TargetStateArray.h>
#include <multidrone_msgs/TargetState.h>
#include "global_tracker/Contribution.h"
#include <math.h>
#include <Eigen/Dense>
#include <vector>
#include <deque>

/***** DEFINITIONS *****/
#define TIME_WINDOW_LENGHT_DEFAULT 10
#define ASOCIATION_TH_DEFAULT 0.2
#define TRACK_ASOCIATION_TH_DEFAULT 0.2
#define MAX_TIME_WITHOUT_PAIRING_REFRESH 30
#define VISUAL_TRACKS_COMMON_ID_OFFSET_DEFAULT 20
#define GPS_VIRTUAL_DEVICE_ID 0
#define ORDER 6
/***********************/

typedef Eigen::Matrix<double, -1, -1, Eigen::ColMajor> Cm;

/*** STRUCTURES ***/
typedef struct target_local_index{
    int device_index;
    int target_local_index;
} target_local_index;

typedef struct target_local{
    target_local_id id;
    geometry_msgs::Point state; // Estado del target (posición)
} target_local;

typedef struct pairingCouple{
    target_local target1;    // Id del elemento emparejado de la lista 1
    target_local target2;    // Id del elemento emparejado de la lista 2
    double d;   // Distancia del emparejamiento (cuanto menor, más posibilidad de que el emparejamiento sea correcto)   
} pairingCouple;

typedef struct pairingCoupleIndex{
    target_local_index target1;    // Id del elemento emparejado de la lista 1
    target_local_index target2;    // Id del elemento emparejado de la lista 2
    double d;   // Distancia del emparejamiento (cuanto menor, más posibilidad de que el emparejamiento sea correcto)   
} pairingCoupleIndex;
/******************/

class DistanceAsociationEIF
{

public:
    // Información del EIF
    std::vector<Eigen::MatrixXd> sigmat1;
    std::vector<Eigen::MatrixXd> psigmat;
    std::vector<Eigen::MatrixXd> sigmat;

    std::vector<Eigen::MatrixXd> ept1;
    std::vector<Eigen::MatrixXd> pept;
    std::vector<Eigen::MatrixXd> ept;

    std::vector<Eigen::MatrixXd> mut1;
    std::vector<Eigen::MatrixXd> pmut;
    std::vector<Eigen::MatrixXd> covariance1;

    // GPS
    global_tracker::EIFasoc_P2P lastGPSdata;

    // Constructor y destructor
    DistanceAsociationEIF();
    ~DistanceAsociationEIF();

    // Borrar las medidas GPS guardadas para que no sean añadidas e integradas en el filtro
    // en la proxima iteración
    void clearGPSdataVector();

    // Esta función prepara las medidas del GPS para ser integradas por el filtro
    void manageGPSdata(multidrone_msgs::TargetStateArray gpsdata);

    // Esta función sumará las contribuciones a cada track
    // Para ello recorrerá la lista de emparejamientos de cada track y buscará si han llegado 
    // medidas del correspondiente target del drone emparejado
    void addContributions();

    // Función que guarda el vector y matriz de información para la siguiente iteración
    // Es el último paso en la iteración dle filtro EIF
    void saveEstimation();

    // Actualiza el estado de los track con el último calculo realizado por el filtro EIF
    // Tambien guarda el resultado del filtro en la forma canónica del filtro de kalman
    // (mut1 y covariance1)
    // Además suma uno a los tiempos de vida del track (se suponque esta función es llamada
    // al final de cada iteración)
    // Si un target lleva demasiado tiempo vivo sin el refresco o añadido de un pairing
    // se elimina el track por deshuso
    void updateTrackListStates();

    // Suma la predicción a la estimacion final del EIF para cada track
    void addPrediction();

    // Inicializa las variables para la nueva iteración del EIF
    void initilizeEIFiteration();

    // Guarda la última tabla con datos de asoación y EIF
    // También llama a push_targets3Dtable para guardar la parte correspondiente
    // de los datos en la tabla de targets de la ventana de tiempo
    void push_EIFasocTable(std::vector <global_tracker::EIFasoc_P2P> newTable);
    // Esta función añade un device "virtual" con las últimas medidas de los gps
    // antes de llamar a la función anterior
    void push_EIFasocTableWithGPSdata(std::vector <global_tracker::EIFasoc_P2P> newTable);

    // Introduce una nueva tabla de targets 3D para ser asociada
    void push_targets3Dtable(std::vector <global_tracker::Targets3DArray_P2P> newTable);

    // Actualiza la lista de tracks con un modelo de predicción
    // Llamar en la fase de predicción del filtro bayesiano
    void updateTrackListStatePredictionModel();

    // Actualiza los emparejamientos llamando los dos métodos
    // de asociación en el orden correcto
    void updateTrackListPairings();

    // Actualiza el vector de emparejamientos con la tabla de targets3D
    // Todos los targets son añadidos al proceso
    void updateTrackListPairingsTimeWindow();

    // Actualiza el vector de emparejamientos con la tabla de targets3D
    // Se debe indicar que targets deben ser añadidos al proceso
    void updateTrackListPairingsTimeWindow(std::vector <std::vector <bool>> markedTargets);

    // Setea los valores iniciales del filtro EIF que se la dará al crear el track
    void setInitialState(Eigen::MatrixXd sigmat1_m, Eigen::MatrixXd ept1_m);

    // Pinta un vector de tracks (trackList)
    void print_trackList();

    // Fija el modelo de predicción
    void setPredictionModel(Eigen::MatrixXd m);
    void setPredictionModelTrust(Eigen::MatrixXd m);
    void setVisualMeasureTrust(Eigen::MatrixXd m);
    void setGPSMeasureTrust(Eigen::MatrixXd m);

    // Devuelve el número de elementos de la trackList
    int getTrackListSize();

    // Devuelve un track de la lista de tracks
    Track getTrack(unsigned int index);

    // Pinta una targets3Dtable
    void draw_targets3Dtable(unsigned int index);
    void draw_targets3Dtables();

    void set_asociation_window(int value);
    int get_asociation_window();

    void set_asociation_th(double value);
    double get_asociation_th();

    void set_track_asociation_th(double value);
    double get_track_asociation_th();

    void set_fixed_sigmaGPS(bool value);
    bool get_fixed_sigmaGPS();

    void set_max_track_time_without_pairing_refresh(unsigned int value);
    unsigned int get_max_track_time_without_pairing_refresh();

    void set_visual_tracks_common_id_offset(unsigned int value);
    unsigned int get_visual_tracks_common_id_offset();

    // Pinta los targets marcados para ser asociados mediante la lista ventana de tiempo
    void drawMarkedTargets(std::vector <std::vector <bool>> m);

private:
    std::vector <Track> trackList;  // La lista debe estar ordenada por ids de los tracks
                                    // así siempre estarán priemor lso tracks gps y luego los visuales
    int asociation_window;
    double asociation_th;       // asociation_threshold
    double track_asociation_th;       // asociation_threshold using EIF updated tracks
    unsigned int max_time_without_pairing_refresh;  // Máximo tiempo que puede estar un track vivo sin haber sido que algún pairing haya sido añadido o refrescado
    std::deque <std::vector <global_tracker::Targets3DArray_P2P>> targets3Dtables; // Vector de tablas de targets3D por emparejar
    std::vector <pairing> pairings; // Vector de emparejamientos
    std::vector <std::vector <bool>> markedTargets;
    Eigen::MatrixXd G;  // Modelo de prediccón
    Eigen::MatrixXd R;  // Ruido del modelo de predicción
    Eigen::MatrixXd Qvisual;  // Ruido de las medidas visuales
    Eigen::MatrixXd Qgps;  // Ruido de las medidas gps
    bool fixed_sigmaGPS;	// Indica si se usa la Qgps fija o la Qgps_k que viene con la medida gps
    Eigen::MatrixXd Hgps;  // Jacobiano del modelo de observación para las medidas gps

    unsigned int visual_tracks_common_id_offset;    // A partir de que id de asignan las id de los track puramente visuales (los tracks con GPS tendrán ids por debajo de este valor)

    // Información del EIF
    Eigen::MatrixXd sigmat1_initial;
    Eigen::MatrixXd ept1_initial;

    std::vector <global_tracker::EIFasoc_P2P> EIFasocTable; // Guarda la tabla de asociación con datos del EIF de la última iteración

    // GPS
    //global_tracker::EIFasoc_P2P lastGPSdata;

    // Ordena la trackList por la id de los tracks
    void sortTrackList();

    // Cambia la id de un track y reordena la trackList
    void changeTrackID(unsigned int track_index, unsigned int newID);

    // Añade dos targets, el primero de ellos tipo track gps, a la trackList
    void addGPStargetPair2TrackList(target_local GPStarget, target_local visualTarget);

    // Borra un track de la track list dada su índice
    // También borrar los correspondientes elementos en la listas de datos del EIF
    // Devuelve -1 si no se ha podido borrar el track o 0 en caso de éxito
    int eraseTrack(unsigned int index);

    // Calcula la contribución dadas las medidas visuales de un target
    void calculateVisualContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, const Eigen::Ref<const Eigen::Matrix4d>& T, const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib);

    // Calcula la contribución dada por una medida
    void calculateContribution(const Eigen::Ref<const Eigen::MatrixXd>& z, const Eigen::Ref<const Eigen::MatrixXd>& h, const Eigen::Ref<const Eigen::MatrixXd>& H, const Eigen::Ref<const Eigen::MatrixXd>& pmut, const Eigen::Ref<const Eigen::MatrixXd>& Q, Eigen::Ref<Eigen::MatrixXd> ept_contrib, Eigen::Ref<Eigen::MatrixXd> omegat_contrib);

    // Actualiza los emparejamientos de la track list usando la distancia de cada target
    // de la nueva tabla de medidas con los estados de cada track
    // Llamar en la fase de actualización tras añadir la nueva tabla de medidas
    // con la función push_targets3Dtable
    void updateTrackListPairingsByState(std::vector <Track> &tl, std::vector <std::vector <bool>> &markedTargets);

    // Actualiza la lista de tracks con un modelo de predicción
    // Llamar en la fase de predicción del filtro bayesiano
    void updateTrackListStatePredictionModel(std::vector <Track> &tl);

    // Añade un emparejamiento propuesto a la lista de tracks
    // Se da por hecho que el target no ha sido asociado de forma confiable
    // por los track predichos por el EIF y por tanto los targets del emparejamiento
    // no aparecen en la lista de Tracks, y por tanto se debe crear un track. La única 
    // excepción es que en esta misma iteración el target haya sido emparejado en otra 
    // comparación de targets de drones y entonces ya si haya un track con alguno de los 
    // targets a añadir del emparejamiento. En este caso se añade el target que falta al
    // track, no se crea uno nuevo
    int addPairingCouple2TrackList(pairingCouple pair);

    // Busca un target en la trackList, en caso de encontrarlo devuelve el índice 
    // del track dónde se encuentra. En caso de no encontrarlo devuelve -1
    int findTargetLocalTrackList(std::vector <Track> &tl, target_local_id target);

    // Busca un target en una lista de emparejamientos, en caso de encontrarlo devuelve el índice 
    // del target en la lista. En caso de no encontrarlo devuelve -1
    int findTargetLocalPairingList(pairing p, target_local_id target);

    // Busca el índice del primer track visual en la lista de tracks
    // En caso de no haber tracks visuales se devuelve el offset de 
    // posición de los tracks visuales
    // Esta función también puede ser usada para calcular el número de tracks de gps en la lista
    int findFirstVisualTrackIndex();
    int GPSTracksCount();

    // Añade un track a la lista de tracks con un par emparejados
    // Se devuelve el id común asignado a los emparejamientos del track
    // Se elige el primer id libre en la lista
    // El track se crea con un primer pairingCouple
    // Se supone que no existe ningún otro track con el par de targets añadidos
    int addVisualTrack(pairingCouple pair);
    int addVisualTrack(target_local t);
    // Añade un track a la lista de tracks con los emparejamientos pasados como argumento con el estado pasado como argumento
    void addTrack(pairing pairings, geometry_msgs::Point trackPos);

    // Añade un track usando como id común la del propio GPS
    // Se supone que cada GPS tiene una id distinta y por tanto no existirán dos tracks
    // con el mismo id
    // Reordenará la lista al añadir el nuevo track
    void addGPSTrack(int common_id, geometry_msgs::Point pos);
    // Esta versión de la función permite añadir también un target visual en los emparejamientos del nuevo track
    void addGPSTrack(int common_id, geometry_msgs::Point pos, target_local_id visual_target_id);

    // Busca en un la lista de tracks el siguiente ID libre
    int nextFreeIDTrackList();

    // Busca en un la lista de tracks el siguiente ID libre de los tracks visuales
    int nextFreeIDVisualTrackList();

    // Busca en la lista de emparejamientos si existe un target de un drone en un emparejamiento
    // en caso afirmativo devuelve el número de targets borrados y borra el emparejamiento, 
    // en otro caso no hace nada y devuelve -1
    int eraseTargetFromPairing(pairing &p, target_local_id target);

    // Busca en la lista de tracks si existe un target de un drone en un emparejamiento
    // en caso afirmativo devuelve el número de 0 y borra el/los emparejamiento, 
    // en otro caso no hace nada y devuelve -1
    int eraseTargetFromTrackVector(std::vector <Track> &tl, target_local_id target);

    // Busca (si existe) un track con la id pasada como argumento en la lista de tracks
    // En caso afirmativo devuelve el índice del track en la lista
    int findTrack(unsigned int track_id);

    // Busca (si existe) un track de GPS con la id pasada como argumento en la lista de tracks
    // En caso afirmativo devuelve el índice del track en la lista
    // EN otro caso devuelve -1
    int findGPSTrack(unsigned int track_id);

    // Calcula la distancia entre dos targets3D de dos drones distintos en una de las tablas guardadas
    // de la lista de tablas de targets3D
    // Deben existir al menos dos drones en la tabla
    // Devuelve 0 si se han encontrado los targtes3D indicados, en caso contrario devuelve -1
    int findTargets3Ddistance(unsigned int table_index, target_local_id target1, target_local_id target2, double &d);

    // Calcula la distancia entre un target y un track en un instante tiempo t
    // Si no hay track o medida de dicho target en ese instante se devuelve -1
    // Devuelve 0 si el track y el target han sido encontrados, y por tanto la distancia calculada
    int findTrackTargets3Ddistance(unsigned int table_index, unsigned int track_index, target_local_id target, double &d);

    // Busca (si existe) el índice del vector de un drone en la tabla de targets3D dada su id (la id del drone)
    // Devuelve -1 si no encuentra el drone, o el índice en caso de encontrarlo
    int findDeviceTableIndex(unsigned int table_index, unsigned int drone_id);

    // Busca un valor en un vector de enteros de 16 bits sin signo (vectores de IDs)
    // Devuelve -1 si no encuentra el valor
    int findIndexUint16(unsigned short int d, std::vector<unsigned short int> v);
    
    // Calcula la distancia Euclídea entre dos puntos
    double euclideanDistance(geometry_msgs::Point p1, geometry_msgs::Point p2);

    void initialize();

    void printPairingCoupleVector(std::vector <pairingCouple> v);

};
