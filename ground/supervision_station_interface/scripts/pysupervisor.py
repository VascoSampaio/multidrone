#!/usr/bin/env python
from multidrone_msgs.srv import PostSemanticAnnotations, PostSemanticAnnotationsRequest, PostSemanticAnnotationsResponse, SupervisorAlarm, SupervisorAlarmRequest, SupervisorAlarmResponse,  SafetyCheck, SafetyCheckRequest, SafetyCheckResponse
import rospkg
import rospy
import time
from os import system
import signal
import sys
from os import listdir
from os.path import isfile, join
from Tkinter import *
from tkMessageBox import *


# Finish the execution directly when Ctrl+C is pressed (signal.SIGINT received), without escalating to SIGTERM.
def signal_handler(sig, frame):
    print('Ctrl+C pressed, signal.SIGINT received.')
    sys.exit(0)


def main_menu():
    
    print "\nMain menu of the pysupervisor. Please choose the option to send:"
    print "1. KML to the Semantic Map Manager"
    print "2. Alarm to drones 1 and 2"

    selected = raw_input(" >> ")
    system("clear")
    if selected == "1":
        KML_menu()
    elif selected == "2":
        alarm_menu()

    else:
        system("clear")
        print "Not a valid option."


# 1. KML to the Semantic Map Manager:
def KML_menu():
    #show the files in the directory:
    onlyfiles = [f for f in listdir(kml_path) if isfile(join(kml_path, f))]
    print "KML menu. The next KMLs are available:"
    cont = 0
    orded_list = sorted(onlyfiles)
    for i in range(len(orded_list)):
        num = i+int(1)
        print("%d. %s" %(num, orded_list[i]) )
        cont = cont+1
    print("Press any other key to quit.")
    selected = raw_input(" >> ")
    try:
        if not( selected.isalpha() ) and int(selected)>=1 and int(selected) <= len(orded_list): # selected.isalpha() return true if selected is a leter
            system("clear")
            print("%s selected" %orded_list[int(selected)-1] )
            kml = open( kml_path + orded_list[int(selected)-1], 'r').read()
            try:
                kml_request = PostSemanticAnnotationsRequest()
                kml_request.supervisor_or_dashboard = False
                kml_request.clear = False
                kml_request.semantic_map = kml
                kml_service.call(kml_request)
            except rospy.ServiceException, e:
                print "\nService call failed: %s"%e
            main_menu()
        else:
            system("clear")
            print "Not a valid option, returning to main menu."
            main_menu()
    except:
        system("clear")
        print "Not a valid option, returning to main menu."
        main_menu()


# 2. Alarm to drones 1 and 2:
def alarm_menu():
    try:
        system("clear")
        alarm_request = SupervisorAlarmRequest()
        alarm_request.drone_id = 1
        print alarm.call(alarm_request)
        alarm_request.drone_id = 2
        print alarm.call(alarm_request)
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


# Callback of the safety check service:
def safety_check_callback(req):

    # Open a question dialog that can return "yes" or "no" depending on the button pressed.
    main = Tk()
    main.withdraw()                                                         # Hide the main window (second window needed internally for Tkinter)
    result = askquestion(title="Safety Check", message="Is this plan OK?")  # Dialog window with two buttons, "yes" and "no".
    main.destroy()                                                          # Destroy the main window for possible next calls of this callback.

    check_response = SafetyCheckResponse()
    if result=='yes':
        check_response.result = SafetyCheckResponse.SAFETY_OK
    else:
        check_response.result = SafetyCheckResponse.SAFETY_ERROR
    
    # Return the response of the service callback
    return check_response


if __name__ == "__main__":
    rospy.init_node('pysupervisor', anonymous=True)

    kml_path = rospy.get_param('~kml_path')

    kml_service = rospy.ServiceProxy('post_semantic_annotations', PostSemanticAnnotations)
    alarm = rospy.ServiceProxy('execution/alarm', SupervisorAlarm)

    # In Python there is no need for spin, without saying anything by default a spin thread is created, rospy.spin() only blocks the code in the spin loop.
    safety_check = rospy.Service('supervisor/safety_check', SafetyCheck, safety_check_callback)

    signal.signal(signal.SIGINT, signal_handler)    # Associate signal SIGINT (Ctrl+C pressed) to handler (function "signal_handler")

    system("clear")
    print "Welcome to the pysupervisor,"

    while not rospy.is_shutdown():
        main_menu()
        time.sleep(0.1)
