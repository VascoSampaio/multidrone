# MULTIDRONE FULL - Ground #

This folder contains all the code of the ground station. It may not contain subfolders but ROS packages.

## List of packages ##

* dashboard_interface (RAI/USE)
* global_tracker (USE/IST)
* ground_visual_analysis (AUTH)
* multidrone_planning (USE)
* rtkbasestation (IST)
* supervision_station_interface (TS)

### Dependencies ###

* [Python 2.7](https://www.python.org/download/releases/2.7/): `sudo apt-get install python`
* [Matplotlib](https://matplotlib.org/): `sudo pip install matplotlib`
